#!/bin/bash
echo -e "\nEnabling development modules"
drush @rfid.loc en dna_develop admin_menu adminimal_admin_menu -y

drush @rfid.loc vset devel_krumo_skin 'orange'

drush @rfid.loc upwd 1 --password="admin"
