#!/bin/sh
# Script to build RFID Attendance project.

if [ ! -d 'src' ]; then
  echo "src folder not found."
  echo "Run this script (./scripts/build.sh) from the repository root. You should see the scripts and src folder."
  exit 0;
fi

REPO_PATH=`pwd`
PROFILE_NAME='attendance'
TIMESTAMP=`date +"%Y%m%d%H%M%S"`
BUILD_PATH="$REPO_PATH/drupal-$TIMESTAMP"
PROFILE_VERSION=$(grep -e 'version' "$REPO_PATH/src/profile/attendance.info" | awk '{print $NF}')

echo '================='
echo ' RFID Attendance '
echo '================='

echo "\nThis command can be used to build the distribution.\n"
echo "  [r] Build in release mode (latest stable release)"
echo "  [d] Build in development mode (latest dev code with .git working-copy)\n"
echo "**NOTE**: Site version is $PROFILE_VERSION. Manually update attendance.info version number before building the release.\n"
echo "Selection: \c"
read SELECTION

if [ $SELECTION = "r" ]; then
  DRUSH_OPTS='--no-cache --no-gitinfofile'

elif [ $SELECTION = "d" ]; then
  DRUSH_OPTS='--working-copy --no-gitinfofile --no-cache'

else
 echo "Invalid selection."
 exit 0
fi

# Alter the final destination of the build (Drupal root) based on whether we
# are building in dev or release mode.
if [ $SELECTION = "r" ]; then
  BUILD_DEST_PATH="$REPO_PATH/release"
else
  BUILD_DEST_PATH="$REPO_PATH/drupal"
fi

echo "Repo path: $REPO_PATH"
echo "Temp build path: $BUILD_PATH"
echo "Profile name: $PROFILE_NAME"

# Do the build
set -e
echo "Building Drupal Core..."

if [ $SELECTION = "r" ]; then
  drush make $DRUSH_OPTS "$REPO_PATH/src/profile/drupal-org-core.make.yml" $BUILD_PATH
else
  drush make $DRUSH_OPTS "$REPO_PATH/src/profile/drupal-org-core.development.make.yml" $BUILD_PATH
fi

cd $BUILD_PATH
if [ $SELECTION = "r" ]; then
  echo "Building contrib/custom in release mode..."
  drush make $DRUSH_OPTS --no-core "../src/profile/$PROFILE_NAME.make.yml" -y

elif [ $SELECTION = "d" ]; then
  echo "Building contrib/custom in development mode (latest dev code with .git working-copy)..."
  drush make $DRUSH_OPTS --no-core "../src/profile/$PROFILE_NAME.make.yml" -y
fi
set +e

# Remove current drupal dir
echo 'Rebuilding drupal directory...'
sudo rm -rf $BUILD_DEST_PATH

if [ -d $BUILD_DEST_PATH ]; then
  echo "Unable to wipe the drupal directory to rebuild. Please check permissions."
  rm -rf $BUILD_PATH
  exit 0
fi

# Copy temporary build folder to drupal folder.
mv $BUILD_PATH $BUILD_DEST_PATH

# @todo figure out why make file would not put pouch.find in proper destination.
cd $BUILD_DEST_PATH
cp sites/all/libraries/pouchdb.find/pouchdb.find.min.js sites/all/libraries/pouchdb
rm -rf sites/all/libraries/pouchdb.find
cp sites/all/libraries/pouchdb.upsert/dist/pouchdb.upsert.min.js sites/all/libraries/pouchdb
rm -rf sites/all/libraries/pouchdb.upsert
cp ../src/libraries/pouchdb/pouchdb.crypto-pouch.min.js sites/all/libraries/pouchdb
rm -rf sites/all/libraries/pouchdb.crypto-pouch
cp sites/all/libraries/select2-flat-theme/select2-flat-theme.min.css sites/all/libraries/select2/dist/css
rm -rf sites/all/libraries/select2-flat-theme

# If favicons exist then move those to the Drupal root.
if [ -d "$REPO_PATH/src/assets/favicons" ]; then
  echo "Copying assets."
  cd $REPO_PATH

    if [ $SELECTION = "r" ]; then
      cp src/assets/favicons/* release
    else
      cp src/assets/favicons/* drupal
    fi
else
  echo "*** Unable to copy favicon assets. ***"
fi

# If in development mode create symlinks to the appropriate /src/... folder for
# the custom profile, module, and theme. Otherwise we copy the /src/... folders
# to the appropriate location under the Drupal root.
# Release mode.
if [ $SELECTION = "r" ]; then
  # Copy profile, modules, and themes. Create the necessary directories.
  cd $REPO_PATH
  cp -R src/profile "release/profiles/$PROFILE_NAME"
  cp -R src/modules release/sites/all/modules/custom
  cp -R src/themes release/sites/all/themes/custom
  cp src/sites/default/settings.php release/sites/default/settings.php
  cp src/sites/default/drushrc.php release/sites/default/drushrc.php

  # Drupal core contains a .gitignore file that we do not want in the release
  # folder.
  rm release/.gitignore

  # Copy any libraries that are not able to be included in the attendance.make.yml
  # file.
  find "$REPO_PATH/src/libraries" -mindepth 1 -maxdepth 1 -type d -exec cp -R "{}" "$REPO_PATH/release/sites/all/libraries/" \;

# Development mode.
elif [ $SELECTION = "d" ]; then
  echo 'Setting up symlinks...'
  # Build Symlinks
  cd "$REPO_PATH/drupal/profiles"
  ln -s ../../src/profile $PROFILE_NAME
  cd "$REPO_PATH/drupal/sites/all/modules"
  ln -s ../../../../src/modules custom
  cd "$REPO_PATH/drupal/sites/all/themes"
  ln -s ../../../../src/themes custom

  # Copy any libraries that are not able to be included in the attendance.make.yml
  # file.
  find "$REPO_PATH/src/libraries" -mindepth 1 -maxdepth 1 -type d -exec cp -R "{}" "$REPO_PATH/drupal/sites/all/libraries/" \;

  # Set up storage folder.
  IS_NEW=0
  cd $REPO_PATH
  if [ ! -d 'storage' ]; then
    echo 'Setting up storage folder...'
    IS_NEW=1
    mkdir storage
    mkdir storage/files
    mkdir storage/files_private
    cp src/sites/default/example.settings.local.inc storage/settings.local.inc
  fi

  cd "$REPO_PATH/drupal/sites/default"
  ln -s ../../../storage/files/ files
  ln -s ../../../storage/files_private/ files_private
  ln -s ../../../src/sites/default/settings.php settings.php
  ln -s ../../../src/sites/default/drushrc.php drushrc.php
  ln -s ../../../storage/settings.local.inc settings.local.inc

  # If we're updating an existing environment then attempt to run the various
  # drush commands to update, etc. We only run this in development mode to
  # avoid affecting a local dev environment that simply wants to build the
  # release folder.
  if [ $IS_NEW != 1 ]; then
    cd "$REPO_PATH/drupal"

    # Registry rebuild -- make sure it exists before running.
    if `drush @rfid.loc help registry-rebuild > /dev/null 2>&1` ; then
      echo 'Rebuilding Registry'
      drush @rfid.loc rr
    else
      echo 'Skipping registry rebuild because https://drupal.org/project/registry_rebuild is not installed.'
    fi

    echo 'Clearing caches...'
    if drush @rfid.loc cc all | grep -q "No Drupal"; then
      echo "No Drupal site found."
    else
      echo 'Running updates...'
      drush @rfid.loc updb -y;
      echo 'Reverting all features...'
      drush @rfid.loc fra -y;
      drush @rfid.loc cc all;
    fi

    # echo 'Setting file/folder permissions for development...'
    cd $REPO_PATH
    # sudo chown -RL $USER:www-data . && sudo chmod -R 755 .
  fi
fi

# Update file/folder permissions.
# Make sure git will read the folder/file permissions.
# We run this regardless of build mode to avoid swapping permissions with git.
git config core.fileMode true

if [ -z "$WWW" ]; then
  WWW=www-data
fi

if [ $SELECTION = "r" ]; then
  cd $REPO_PATH
  sudo fix-permissions.sh --drupal_path=release
else
  cd $REPO_PATH
  sudo fix-permissions.sh --drupal_path=drupal
  # If in development make sure the server can write to the file system.
  sudo chmod -R 766 storage/
fi

cd $REPO_PATH
echo "$(date): $BUILD_DEST_PATH" >> .rfid.local-build.log

echo 'Build complete.'
