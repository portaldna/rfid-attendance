SELECT
  rp.id,
  rp.uid,
  rpid.field_rfid_person_id_value AS person_id,
  rpfn.field_rfid_person_first_name_value AS first_name,
  rpln.field_rfid_person_last_name_value AS last_name,
  rp.status,
  FROM_UNIXTIME(rp.created, '%Y-%m-%dT%TZ') AS created,
  FROM_UNIXTIME(rp.changed, '%Y-%m-%dT%TZ') AS changed
FROM eck_rfid_person rp
LEFT JOIN field_data_field_rfid_person_id rpid ON rpid.entity_id = rp.id
LEFT JOIN field_data_field_rfid_person_first_name rpfn ON rpfn.entity_id = rp.id
LEFT JOIN field_data_field_rfid_person_last_name rpln ON rpln.entity_id = rp.id
