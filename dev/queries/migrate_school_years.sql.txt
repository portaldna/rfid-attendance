SELECT
  rsy.id,
  rsy.uid,
  rsyl.field_school_year_label_value AS label,
  FROM_UNIXTIME(rsyse.field_school_year_start_end_value, '%Y-%m-%dT%TZ') AS valid_from,
  FROM_UNIXTIME(rsyse.field_school_year_start_end_value2, '%Y-%m-%dT%TZ') AS valid_to,
  rsy.status,
  FROM_UNIXTIME(rsy.created, '%Y-%m-%dT%TZ') AS created,
  FROM_UNIXTIME(rsy.changed, '%Y-%m-%dT%TZ') AS changed
FROM eck_rfid_school_year rsy
LEFT JOIN field_data_field_school_year_label rsyl ON rsyl.entity_id = rsy.id
LEFT JOIN field_data_field_school_year_start_end rsyse ON rsyse.entity_id = rsy.id
