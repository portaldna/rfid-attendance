SELECT
  rt.id,
  rt.uid,
  rtid.field_rfid_tag_id_value AS tag_id,
  IFNULL(rtto.field_rfid_tag_assigned_to_target_id, 0) AS assign_to_uid,
  rt.status,
  FROM_UNIXTIME(rt.created, '%Y-%m-%dT%TZ') AS created,
  FROM_UNIXTIME(rt.changed, '%Y-%m-%dT%TZ') AS changed
FROM eck_rfid_tag rt
LEFT JOIN field_data_field_rfid_tag_id rtid ON rtid.entity_id = rt.id
LEFT JOIN field_data_field_rfid_tag_assigned_to rtto ON rtto.entity_id = rt.id
