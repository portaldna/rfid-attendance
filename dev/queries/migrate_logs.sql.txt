SELECT
  rl.id,
  rl.uid,
  rlr.field_rfid_log_reader_target_id AS reader_id,
  rlt.field_rfid_log_tag_target_id AS tag_id,
  rlp.field_rfid_log_person_target_id AS person_id,
  FROM_UNIXTIME(rl.created, '%Y-%m-%dT%TZ') AS created,
  FROM_UNIXTIME(rl.changed, '%Y-%m-%dT%TZ') AS changed
FROM eck_rfid_log rl
LEFT JOIN field_data_field_rfid_log_reader rlr ON rlr.entity_id = rl.id
LEFT JOIN field_data_field_rfid_log_tag rlt ON rlt.entity_id = rl.id
LEFT JOIN field_data_field_rfid_log_person rlp ON rlp.entity_id = rl.id
