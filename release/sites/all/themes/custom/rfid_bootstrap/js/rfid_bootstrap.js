/**
 * @file
 * Javascript file for RFID Bootstrap theme.
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.behaviors.rfid_bootstrap_init = {
    attach: function (context, settings) {

      // @see https://github.com/FezVrasta/bootstrap-material-design/tree/v3
      // @see https://cdn.rawgit.com/FezVrasta/bootstrap-material-design/gh-pages-v3/index.html
      $.material.init();

      $('.dna-select2-init-processed')
        .once('rfid-select2-init2')
        .each(function (i) {
          $(this)
            .closest('.form-group')
            .find('.select2-selection--single[role="combobox"]')
            .addClass('form-control');
        });

      $('[dna-select2-id]')
        .once('rfid-select2-init')
        .on('dna-select2-init', function (e) {
          $(this)
            .closest('.form-group')
            .find('.select2-selection--single[role="combobox"]')
            .addClass('form-control');
        })
        .on('select2:opening', function (e) {
          $(this)
            .closest('.form-group')
            .addClass('is-focused');
        })
        .on('select2:open', function (e) {
          $(this)
            .closest('.form-group')
            .find('.select2-search__field')
            .addClass('form-control');
        })
        .on('select2:closing', function (e) {
          $(this)
            .closest('.form-group')
            .removeClass('is-focused');
        })

      // @todo replaced this with select2.
      // @see https://github.com/FezVrasta/dropdown.js
      // $(".form-select").dropdown({ "autoinit" : ".form-select" });

    }
  };

})(jQuery, Drupal, this, this.document);
