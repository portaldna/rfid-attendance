<?php

/**
 * @file
 * Main file for RFID Attendance Bootstrap sub-theme.
 */

/**
 * Override or insert variables into the maintenance page template.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("maintenance_page" in this case).
 */
function rfid_bootstrap_preprocess_maintenance_page(array &$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  rfid_bootstrap_preprocess_html($variables, $hook);
  rfid_bootstrap_preprocess_page($variables, $hook);
}

/**
 * Override or insert variables into the html templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 * @param string $hook
 *   The name of the template being rendered ("html" in this case).
 */
function rfid_bootstrap_preprocess_html(array &$variables, $hook) {
  $links = [
    [
      'rel' => 'apple-touch-icon',
      'sizes' => '180x180',
      'href' => '/apple-touch-icon',
    ],
    [
      'rel' => 'icon',
      'type' => 'image/png',
      'sizes' => '32x32',
      'href' => '/favicon-32x32.png',
    ],
    [
      'rel' => 'icon',
      'type' => 'image/png',
      'sizes' => '16x16',
      'href' => '/favicon-16x16.png',
    ],
    [
      'rel' => 'manifest',
      'href' => '/manifest.json',
    ],
    [
      'rel' => 'mask-icon',
      'href' => '/safari-pinned-tab.svg',
      'color' => '#1e73b3',
    ],
  ];

  foreach ($links as $attributes) {
    drupal_add_html_head_link($attributes);
  }

  $metas = [
    [
      'name' => 'msapplication-TileColor',
      'content' => '#1e73b3',
    ],
    [
      'name' => 'theme-color',
      'content' => '#1e73b3',
    ],
  ];

  foreach ($metas as $attributes) {
    $meta = [
      '#tag' => 'meta',
      '#attributes' => $attributes,
    ];
    drupal_add_html_head($meta, $attributes['name']);
  }
}

/**
 * Override or insert variables into the page templates.
 *
 * @param array $variables
 *   Variables to pass to the theme template.
 */
function rfid_bootstrap_preprocess_page(array &$variables) {
  if (!empty($variables['primary_nav'])) {
    foreach (element_children($variables['primary_nav']) as $element_id) {
      $variables['primary_nav'][$element_id]['#below'] = [];
    }
  }
}

/**
 * Implements template_preprocess_entity().
 */
function rfid_bootstrap_preprocess_entity(&$variables) {
  $variables['classes_array'][] = drupal_clean_css_identifier($variables['entity_type'] . '--' . $variables['elements']['#bundle'] . '--' . $variables['view_mode']);
}

/**
 * Implements hook_preprocess_bootstrap_modal().
 */
function rfid_bootstrap_preprocess_bootstrap_modal(&$variables) {
  $variables['modal_id'] = $variables['attributes']['id'];
}

/**
 * Implements theme_form_element().
 */
function rfid_bootstrap_form_element(array &$variables) {
  if (in_array($variables['element']['#type'], ['textfield', 'textarea'])) {
    $variables['element']['#wrapper_attributes']['class'][] = 'label-floating';
  }
  if (!empty($variables['element']['#required'])) {
    $variables['element']['#wrapper_attributes']['class'][] = 'required';
  }
  return bootstrap_form_element($variables);
}

/**
 * Implements theme_status_messages().
 */
function rfid_bootstrap_status_messages(array $variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = [
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
    'info' => t('Informative message'),
  ];

  // Map Drupal message types to their corresponding Bootstrap classes.
  // @see http://twitter.github.com/bootstrap/components.html#alerts
  $status_class = [
    'status' => 'success',
    'error' => 'danger',
    'warning' => 'warning',
    // Not supported, but in theory a module could send any type of message.
    // @see drupal_set_message()
    // @see theme_status_messages()
    'info' => 'info',
  ];

  // Retrieve messages.
  $message_list = drupal_get_messages($display);

  // Allow the disable_messages module to filter the messages, if enabled.
  if (module_exists('disable_messages') && variable_get('disable_messages_enable', '1')) {
    $message_list = disable_messages_apply_filters($message_list);
  }

  foreach ($message_list as $type => $messages) {
    $class = (isset($status_class[$type])) ? ' alert-' . $status_class[$type] : '';

    foreach ($messages as $message) {
      $output .= "<div class=\"alert alert-block alert-dismissible$class messages $type\">\n";
      $output .= "  <a class=\"close\" data-dismiss=\"alert\" href=\"#\">&times;</a>\n";

      if (!empty($status_heading[$type])) {
        $output .= '<h4 class="element-invisible">' . filter_xss_admin($status_heading[$type]) . "</h4>\n";
      }

      $output .= filter_xss_admin($message);
      $output .= "</div>\n";
    }
  }

  return $output;
}
