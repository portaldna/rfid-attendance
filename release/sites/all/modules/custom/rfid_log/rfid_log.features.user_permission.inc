<?php

/**
 * @file
 * rfid_log.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rfid_log_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'dna_datatables view rfid_log listing'.
  $permissions['dna_datatables view rfid_log listing'] = array(
    'name' => 'dna_datatables view rfid_log listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'dna_datatables',
  );

  // Exported permission: 'eck add rfid_log bundles'.
  $permissions['eck add rfid_log bundles'] = array(
    'name' => 'eck add rfid_log bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck add rfid_log rfid_log entities'.
  $permissions['eck add rfid_log rfid_log entities'] = array(
    'name' => 'eck add rfid_log rfid_log entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_log bundles'.
  $permissions['eck administer rfid_log bundles'] = array(
    'name' => 'eck administer rfid_log bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_log rfid_log entities'.
  $permissions['eck administer rfid_log rfid_log entities'] = array(
    'name' => 'eck administer rfid_log rfid_log entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_log bundles'.
  $permissions['eck delete rfid_log bundles'] = array(
    'name' => 'eck delete rfid_log bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_log rfid_log entities'.
  $permissions['eck delete rfid_log rfid_log entities'] = array(
    'name' => 'eck delete rfid_log rfid_log entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_log rfid_log entities own'.
  $permissions['eck delete rfid_log rfid_log entities own'] = array(
    'name' => 'eck delete rfid_log rfid_log entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_log bundles'.
  $permissions['eck edit rfid_log bundles'] = array(
    'name' => 'eck edit rfid_log bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_log rfid_log entities'.
  $permissions['eck edit rfid_log rfid_log entities'] = array(
    'name' => 'eck edit rfid_log rfid_log entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_log rfid_log entities own'.
  $permissions['eck edit rfid_log rfid_log entities own'] = array(
    'name' => 'eck edit rfid_log rfid_log entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_log bundles'.
  $permissions['eck list rfid_log bundles'] = array(
    'name' => 'eck list rfid_log bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_log rfid_log entities'.
  $permissions['eck list rfid_log rfid_log entities'] = array(
    'name' => 'eck list rfid_log rfid_log entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_log bundles'.
  $permissions['eck view rfid_log bundles'] = array(
    'name' => 'eck view rfid_log bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_log rfid_log entities'.
  $permissions['eck view rfid_log rfid_log entities'] = array(
    'name' => 'eck view rfid_log rfid_log entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_log rfid_log entities own'.
  $permissions['eck view rfid_log rfid_log entities own'] = array(
    'name' => 'eck view rfid_log rfid_log entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'manage rfid_log properties'.
  $permissions['manage rfid_log properties'] = array(
    'name' => 'manage rfid_log properties',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  return $permissions;
}
