/**
 * @file
 * Javascript file for RFID Log.
 */

(function ($, Drupal, window, document) {

  'use strict';

  DNADatatables.filters.rfid_log_not_visible = function (item, filterInfo, delta) {
    var status = parseInt(Object.getByPath(item, 'attributes.status', 1));
    var peopleIDs = DNADatatables.filterCacher('rfid_log_not_visible', !delta, function () {
      var personIDs = [];

      $('#dna-datatables-page-listing--rfid_log')
        .DataTable()
        .rows({filter: 'applied'})
        .data()
        .toArray()
        .forEach(function (log) {
          var id = Object.getByPath(log, 'relationships.person.data.id', false);

          if (id && !personIDs.includes(id)) {
            personIDs.push(id);
          }
        });

      return personIDs;
    });

    return status && !peopleIDs.includes(item.id);
  };

  Drupal.theme.dna_datatables_colRender__rfid_log__person_id = function (tableID, column, data, type, rowData, meta) {
    if (data.data === null) {
      return rowData.attributes.person;
    }

    return Drupal.theme('actionLink', data.data, 'edit', type, Object.getByPath(data.data, 'attributes.id', ''));
  };

  Drupal.theme.dna_datatables_colRender__rfid_person__id = function (tableID, column, data, type, rowData, meta) {
    return data + ' (' + Drupal.theme('actionLink', rowData, 'edit', type, rowData.meta.label) + ')';
  };

  Drupal.behaviors.rfid_log_init = {
    attach: function (context, settings) {

      $('#dna-datatables-page-listing--rfid_log')
        .once('rfid-log-missing-init')
        .on('search.dt', function () {
          $('#rfid-log-missing').DataTable().draw();
        });

    }
  };

  // @todo add to dna modules.
  function rfid_log_customSearchInit($table) {
    $.fn.dataTable.ext.search.push(
      function (settings, rowSearchData, rowIndex, rowRawData, delta) {
        if (rowRawData.type == 'rfid_person') {
          var swiped = $('#rfid-log-page__datatable')
            .DataTable()
            .rows({filter: 'applied'})
            .data()
            .toArray()
            .map(function (log) {
              return log.attributes.person.id;
            });

          if ($.inArray(rowRawData.id, swiped) >= 0) {
            return false;
          }
        }

        return true;
      }
    );

    $table.DataTable().search('').draw();

    $('#rfid-log-filter--date-range')
      .once('rfid-log-filter-init')
      .on('change', function () {
        $table.DataTable().draw();
      });
  };

})(jQuery, Drupal, this, this.document);
