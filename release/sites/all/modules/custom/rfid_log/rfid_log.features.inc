<?php

/**
 * @file
 * rfid_log.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function rfid_log_eck_bundle_info() {
  $items = array(
    'rfid_log_rfid_log' => array(
      'machine_name' => 'rfid_log_rfid_log',
      'entity_type' => 'rfid_log',
      'name' => 'rfid_log',
      'label' => 'Attendance Log',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function rfid_log_eck_entity_type_info() {
  $items = array(
    'rfid_log' => array(
      'name' => 'rfid_log',
      'label' => 'Attendance Log',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'uuid' => array(
          'label' => 'UUID',
          'type' => 'text',
          'behavior' => 'uuid',
        ),
      ),
    ),
  );
  return $items;
}
