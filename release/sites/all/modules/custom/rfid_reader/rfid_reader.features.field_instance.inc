<?php
/**
 * @file
 * rfid_reader.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rfid_reader_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'rfid_reader-rfid_reader-field_rfid_reader_description'.
  $field_instances['rfid_reader-rfid_reader-field_rfid_reader_description'] = array(
    'bundle' => 'rfid_reader',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Optionally include additional information about the device. E.g. location of device.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'rfid_reader',
    'field_name' => 'field_rfid_reader_description',
    'label' => 'Description',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'rfid_reader-rfid_reader-field_rfid_reader_id'.
  $field_instances['rfid_reader-rfid_reader-field_rfid_reader_id'] = array(
    'bundle' => 'rfid_reader',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Provide a unique identifier for this device.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'rfid_reader',
    'field_name' => 'field_rfid_reader_id',
    'label' => 'Reader ID',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Description');
  t('Optionally include additional information about the device. E.g. location of device.');
  t('Provide a unique identifier for this device.');
  t('Reader ID');

  return $field_instances;
}
