<?php
/**
 * @file
 * rfid_reader.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rfid_reader_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'dna_datatables view rfid_reader listing'.
  $permissions['dna_datatables view rfid_reader listing'] = array(
    'name' => 'dna_datatables view rfid_reader listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'dna_datatables',
  );

  // Exported permission: 'eck add rfid_reader bundles'.
  $permissions['eck add rfid_reader bundles'] = array(
    'name' => 'eck add rfid_reader bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck add rfid_reader rfid_reader entities'.
  $permissions['eck add rfid_reader rfid_reader entities'] = array(
    'name' => 'eck add rfid_reader rfid_reader entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_reader bundles'.
  $permissions['eck administer rfid_reader bundles'] = array(
    'name' => 'eck administer rfid_reader bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_reader rfid_reader entities'.
  $permissions['eck administer rfid_reader rfid_reader entities'] = array(
    'name' => 'eck administer rfid_reader rfid_reader entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_reader bundles'.
  $permissions['eck delete rfid_reader bundles'] = array(
    'name' => 'eck delete rfid_reader bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_reader rfid_reader entities'.
  $permissions['eck delete rfid_reader rfid_reader entities'] = array(
    'name' => 'eck delete rfid_reader rfid_reader entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_reader rfid_reader entities own'.
  $permissions['eck delete rfid_reader rfid_reader entities own'] = array(
    'name' => 'eck delete rfid_reader rfid_reader entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_reader bundles'.
  $permissions['eck edit rfid_reader bundles'] = array(
    'name' => 'eck edit rfid_reader bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_reader rfid_reader entities'.
  $permissions['eck edit rfid_reader rfid_reader entities'] = array(
    'name' => 'eck edit rfid_reader rfid_reader entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_reader rfid_reader entities own'.
  $permissions['eck edit rfid_reader rfid_reader entities own'] = array(
    'name' => 'eck edit rfid_reader rfid_reader entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_reader bundles'.
  $permissions['eck list rfid_reader bundles'] = array(
    'name' => 'eck list rfid_reader bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_reader rfid_reader entities'.
  $permissions['eck list rfid_reader rfid_reader entities'] = array(
    'name' => 'eck list rfid_reader rfid_reader entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_reader bundles'.
  $permissions['eck view rfid_reader bundles'] = array(
    'name' => 'eck view rfid_reader bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_reader rfid_reader entities'.
  $permissions['eck view rfid_reader rfid_reader entities'] = array(
    'name' => 'eck view rfid_reader rfid_reader entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_reader rfid_reader entities own'.
  $permissions['eck view rfid_reader rfid_reader entities own'] = array(
    'name' => 'eck view rfid_reader rfid_reader entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'manage rfid_reader properties'.
  $permissions['manage rfid_reader properties'] = array(
    'name' => 'manage rfid_reader properties',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  return $permissions;
}
