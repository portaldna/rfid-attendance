<?php
/**
 * @file
 * rfid_reader.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function rfid_reader_eck_bundle_info() {
  $items = array(
    'rfid_reader_rfid_reader' => array(
      'machine_name' => 'rfid_reader_rfid_reader',
      'entity_type' => 'rfid_reader',
      'name' => 'rfid_reader',
      'label' => 'RFID Reader',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function rfid_reader_eck_entity_type_info() {
  $items = array(
    'rfid_reader' => array(
      'name' => 'rfid_reader',
      'label' => 'RFID Reader',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'status' => array(
          'label' => 'Status',
          'type' => 'integer',
          'behavior' => '',
        ),
        'uuid' => array(
          'label' => 'UUID',
          'type' => 'text',
          'behavior' => 'uuid',
        ),
      ),
    ),
  );
  return $items;
}
