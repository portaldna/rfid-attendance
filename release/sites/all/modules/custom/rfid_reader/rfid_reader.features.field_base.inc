<?php
/**
 * @file
 * rfid_reader.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function rfid_reader_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_rfid_reader_description'.
  $field_bases['field_rfid_reader_description'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_rfid_reader_description',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(),
    'translatable' => 0,
    'type' => 'text_long',
  );

  // Exported field_base: 'field_rfid_reader_id'.
  $field_bases['field_rfid_reader_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_rfid_reader_id',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
