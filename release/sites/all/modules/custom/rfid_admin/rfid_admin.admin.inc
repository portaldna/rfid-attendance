<?php

/**
 * @file
 * Admin page callbacks for RFID Admin.
 */

/**
 * Extra Fields Config Form.
 *
 * @todo WIP
 */
function dna_admin_configure_extra_fields_form($form, &$form_state) {
  // @todo use something other than the 'js' settings.
  $js_settings = rfid_core_get_rfid_js_settings();
  $extra_fields = variable_get('rfid_extra_field_labels', []);
  $entity_fields = [];

  foreach ($js_settings as $entity_type => $settings) {
    foreach ($settings['columns'] as $delta => $column_info) {
      if (strpos($column_info['name'], 'extra') === 0) {
        $entity_fields[$entity_type][$column_info['name']] = $column_info['title'];
      }
    }
  }

  foreach ($entity_fields as $entity_type => $attrs) {
    foreach ($attrs as $attr => $attr_label) {
      $form['rfid_extra_field_labels']['#tree'] = TRUE;
      $form['rfid_extra_field_labels'][$entity_type][$attr] = [
        '#title' => t('@type @attr Label', [
          '@type' => $js_settings[$entity_type]['entityLabel'],
          '@attr' => $attr_label,
        ]),
        '#type' => 'textfield',
        '#default_value' => !empty($extra_fields[$entity_type][$attr]) ? $extra_fields[$entity_type][$attr] : '',
      ];
    }
  }

  return system_settings_form($form);
}
