<?php

/**
 * @file
 * Page callbacks for RFID Core module.
 */

/**
 * Page callback: Dialog content.
 *
 * @todo move to dna modules.
 */
function rfid_core_dialog_content_ajax_load() {
  $path = current_path();
  $path = str_replace('rfid-dialog/', '', $path);
  $item = menu_get_item($path);
  $page_callback = $item['page_callback'];
  $page_args = $item['page_arguments'];
  $output = call_user_func_array($page_callback, $page_args);

  $commands = [];
  $commands[] = ajax_command_html('#rfid-core-modal__content', render($output));
  $commands[] = ajax_command_invoke('#rfid-core-modal', 'modal', ['show']);
  return ['#type' => 'ajax', '#commands' => $commands];
}
