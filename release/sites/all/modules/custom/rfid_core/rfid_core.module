<?php

/**
 * @file
 * Code for the RFID Core feature.
 */

/**
 * Implements hook_menu().
 */
function rfid_core_menu() {
  $items = [];

  // @todo move to dna modules.
  $items['rfid-dialog'] = [
    'title' => '',
    'page callback' => 'rfid_core_dialog_content_ajax_load',
    'access callback' => 'rfid_core_dialog_content_access',
    'delivery callback' => 'ajax_deliver',
    'file' => 'rfid_core.pages.inc',
    'type' => MENU_CALLBACK,
  ];

  return $items;
}

/**
 * Access callback: Dialog Content.
 *
 * @todo move to dna modules.
 */
function rfid_core_dialog_content_access() {
  $path = current_path();

  if (strpos($path, 'rfid-dialog') !== 0) {
    return FALSE;
  }

  $path = str_replace('rfid-dialog/', '', $path);
  $item = menu_get_item($path);
  $access_callback = $item['access_callback'];
  $access_args = unserialize($item['access_arguments']);
  return call_user_func_array($access_callback, $access_args);
}

/**
 * Implements hook_dna_pouchdb_database_info().
 */
function rfid_core_dna_pouchdb_database_info() {
  return [
    'rfid_attendance' => [],
  ];
}

/**
 * Implements hook_dna_datatables_datatable_default_config_alter().
 */
function rfid_core_dna_datatables_datatable_default_config_alter(&$config) {
  $config['language']['paginate']['first'] = '<span class="glyphicon glyphicon-step-backward" aria-hidden="true"></span>';
  $config['language']['paginate']['previous'] = '<span class="glyphicon glyphicon-menu-left" aria-hidden="true"></span>';
  $config['language']['paginate']['next'] = '<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>';
  $config['language']['paginate']['last'] = '<span class="glyphicon glyphicon-step-forward" aria-hidden="true"></span>';
}

/**
 * Implements hook_dna_datatables_datatable_default_config_buttons_alter().
 */
function rfid_core_dna_datatables_datatable_default_config_button_alter(&$button, $context) {
  if (!empty($button['name'])) {
    if ($button['name'] == 'export') {
      $button['text'] = '<span class="glyphicon glyphicon-export" aria-hidden="true"></span>';
    }

    elseif ($button['name'] == 'config') {
      $button['text'] = '<span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>';
    }

    elseif ($button['name'] == 'colvis') {
      $button['text'] = '<span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>';
    }

    elseif ($button['name'] == 'filters') {
      $button['text'] = '<span class="glyphicon glyphicon-filter" aria-hidden="true"></span>';
    }

    elseif (strpos($button['name'], 'add__rfid') === 0) {
      $button['attr']['href'] = url("rfid-dialog{$button['attr']['href']}");
      $button['text'] = '<span class="glyphicon glyphicon-plus" aria-hidden="true"></span>';
      unset($button['extend']);
      $class_name = 'use-ajax';
      $button['className'] = !empty($button['className']) ? $button['className'] . " {$class_name}" : $class_name;
    }
  }

  if (empty($context['parent'])) {
    $class_name = 'btn';

    if (!empty($button['name']) && strpos($button['name'], 'add__rfid') === 0) {
      $class_name .= ' btn-primary';
    }

    $button['className'] = !empty($button['className']) ? $button['className'] . " {$class_name}" : $class_name;
  }
}

/**
 * Implements hook_page_build().
 */
function rfid_core_page_build(&$page) {
  global $theme;

  if ($theme == 'rfid_bootstrap') {
    $page['page_bottom']['rfid_core']['modal'] = [
      '#theme' => 'bootstrap_modal',
      '#attributes' => [
        'id' => 'rfid-core-modal',
      ],
    ];
    $page['page_bottom']['rfid_core']['#attached']['library'][] = ['system', 'drupal.ajax'];
    $page['page_bottom']['rfid_core']['#attached']['js'][] = [
      'data' => drupal_get_path('module', 'rfid_core') . '/js/rfid_core.js',
      'scope' => 'footer',
    ];
  }
}

/**
 * Implements hook_form_alter().
 */
function rfid_core_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'eck__entity__form_') === 0) {
    $entity_type = $form['#entity_type'];

    if (strpos($entity_type, 'rfid_') !== 0) {
      return;
    }

    $component_class = $form['#component_class'];

    $current_path = current_path();
    $in_modal = (strpos($current_path, 'rfid-dialog') === 0);
    $entity = !empty($form_state['values']['entity']) ? $form_state['values']['entity'] : $form_state[$entity_type];

    $form['in_modal'] = [
      '#type' => 'value',
      '#value' => $in_modal,
    ];

    if ($in_modal) {
      $form['header'] = [
        '#prefix' => '<div class="modal-header">',
        '#suffix' => '</div>',
        '#weight' => -100,
        'close' => [
          '#markup' => '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>',
        ],
        'title' => [
          '#prefix' => '<h4 class="modal-title">',
          '#suffix' => '</h4>',
          '#markup' => drupal_get_title(),
        ],
      ];
      $form['modal_content_open'] = [
        '#markup' => '<div class="modal-body">',
        '#weight' => -99,
      ];
      $form['modal_content_close'] = [
        '#markup' => '</div>',
        '#weight' => 99,
      ];
      $form['actions']['#attributes']['class'][] = 'modal-footer';
      $form['actions']['dismiss'] = [
        '#prefix' => '<button type="button" class="btn btn-sm" data-dismiss="modal" aria-label="Close">',
        '#suffix' => '</button>',
        '#markup' => t('Cancel'),
      ];
      $form['actions']['submit']['#attributes']['class'][] = 'btn-sm';
    }

    $form['#validate'][] = 'rfid_core_rfid_entity_form_unique_id_validate';

    // @todo leave commented out until we understand the outcome of disabling an
    // entity.
    if (FALSE && isset($entity->status)) {
      $form['status'] = [
        '#title' => t('Enabled'),
        '#type' => 'checkbox',
        '#default_value' => (int) $entity->status,
        '#weight' => 95,
      ];
    }

    $form['actions']['#weight'] = 100;
    // @todo
    // $form['actions']['submit']['#attributes']['class'][] = 'btn-raised';
  }
}

/**
 * Validate handler: RFID Entity form unique id.
 */
function rfid_core_rfid_entity_form_unique_id_validate($form, &$form_state) {
  $entity = $form_state['values']['entity'];
  $entity_type = $form['#entity_type'];
  $id_field_name = "field_{$entity_type}_id";

  if (empty($form_state['values'][$id_field_name][LANGUAGE_NONE][0]['value'])) {
    return;
  }

  $id = $form_state['values'][$id_field_name][LANGUAGE_NONE][0]['value'];

  $sql = "
    SELECT
      entity_id
    FROM {field_data_{$id_field_name}}
    WHERE {$id_field_name}_value = :id
    AND entity_id != :entity_id
  ";
  $sql_args = [
    ':id' => $id,
    ':entity_id' => !empty($entity->id) ? $entity->id : 0,
  ];
  $dupes = db_query($sql, $sql_args)->fetchAll();

  if ($dupes) {
    form_set_error($id_field_name, t('This ID already exists. Please provide a unique ID.'));
  }
}

/**
 * Implements hook_dna_ecker_entity_form_submit_ajax_commands_alter().
 */
function rfid_core_dna_ecker_entity_form_submit_ajax_commands_alter(&$commands, $form, $form_state) {
  if (form_get_errors()) {
    return;
  }

  if (!$form_state['values']['in_modal'] && !empty($form_state['redirect'])) {
    ctools_include('ajax');
    $commands['rfidRedirect'] = ctools_ajax_command_redirect($form_state['redirect']);
  }

  $commands['rfidHideModal'] = ajax_command_invoke('#rfid-core-modal', 'modal', ['hide']);
}

/**
 * Implements hook_dna_select2_global_settings_alter().
 */
function dna_core_dna_select2_global_settings_alter(&$settings) {
  if ($GLOBALS['theme'] == 'rfid_bootstrap') {
    $settings['theme'] = 'flat';
  }
}
