/**
 * @file
 * Javascript file for RFID Core.
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.theme.actionLink = function (item, linkId, dtType, label) {
    dtType = dtType || 'diplay';
    var linkInfo = Drupal.dna_jsonapi.getLinkInfo(item, linkId, label);

    if (dtType == 'display') {
      var actionLabel = linkId.charAt(0).toUpperCase() + linkId.slice(1);

      if (linkInfo.path.length) {
        return `<a href="/rfid-dialog${linkInfo.path}" class="use-ajax" data-toggle="tooltip" data-placement="top" title="${actionLabel} ${linkInfo.label}">${linkInfo.label}</a>`;
      }
    }

    return linkInfo.label;
  }

  Drupal.theme.dna_datatables_colRender__relationship = function (tableID, column, data, type, rowData, meta) {
    if (data.data === null) {
      return rowData.attributes[column];
    }

    if (Array.isArray(data.data)) {
      return data.data.reduce(function (output, item) {
        return output.concat((output.length) ? ', ' : '', Drupal.theme('actionLink', item, 'edit', type));
      }, '');
    }

    return Drupal.theme('actionLink', data.data, 'edit', type);
  };

  Drupal.behaviors.rfid_core_devHelper = {
    attach: function (context, settings) {

      $('div.krumo-root')
        .once('rfid-core-krumo-dev-init', function () {
          $('.alert-wrapper .alert').css('opacity', '1');
          $('.alert-wrapper').css({
            'position': 'relative',
            'max-width': 'unset',
            'pointer-events': 'auto'
          });
        });

    }
  };

})(jQuery, Drupal, this, this.document);
