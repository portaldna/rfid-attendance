<?php
/**
 * @file
 * rfid_person.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rfid_person_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'rfid_person-rfid_person-field_rfid_person_first_name'.
  $field_instances['rfid_person-rfid_person-field_rfid_person_first_name'] = array(
    'bundle' => 'rfid_person',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'rfid_person',
    'field_name' => 'field_rfid_person_first_name',
    'label' => 'First Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'rfid_person-rfid_person-field_rfid_person_id'.
  $field_instances['rfid_person-rfid_person-field_rfid_person_id'] = array(
    'bundle' => 'rfid_person',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Include a unique identifier for this person.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'rfid_person',
    'field_name' => 'field_rfid_person_id',
    'label' => 'Person ID',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Exported field_instance:
  // 'rfid_person-rfid_person-field_rfid_person_last_name'.
  $field_instances['rfid_person-rfid_person-field_rfid_person_last_name'] = array(
    'bundle' => 'rfid_person',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'rfid_person',
    'field_name' => 'field_rfid_person_last_name',
    'label' => 'Last Name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('First Name');
  t('Include a unique identifier for this person.');
  t('Last Name');
  t('Person ID');

  return $field_instances;
}
