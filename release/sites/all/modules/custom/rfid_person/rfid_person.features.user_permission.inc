<?php
/**
 * @file
 * rfid_person.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rfid_person_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'dna_datatables view rfid_person listing'.
  $permissions['dna_datatables view rfid_person listing'] = array(
    'name' => 'dna_datatables view rfid_person listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'dna_datatables',
  );

  // Exported permission: 'eck add rfid_person bundles'.
  $permissions['eck add rfid_person bundles'] = array(
    'name' => 'eck add rfid_person bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck add rfid_person rfid_person entities'.
  $permissions['eck add rfid_person rfid_person entities'] = array(
    'name' => 'eck add rfid_person rfid_person entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_person bundles'.
  $permissions['eck administer rfid_person bundles'] = array(
    'name' => 'eck administer rfid_person bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_person rfid_person entities'.
  $permissions['eck administer rfid_person rfid_person entities'] = array(
    'name' => 'eck administer rfid_person rfid_person entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_person bundles'.
  $permissions['eck delete rfid_person bundles'] = array(
    'name' => 'eck delete rfid_person bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_person rfid_person entities'.
  $permissions['eck delete rfid_person rfid_person entities'] = array(
    'name' => 'eck delete rfid_person rfid_person entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_person rfid_person entities own'.
  $permissions['eck delete rfid_person rfid_person entities own'] = array(
    'name' => 'eck delete rfid_person rfid_person entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_person bundles'.
  $permissions['eck edit rfid_person bundles'] = array(
    'name' => 'eck edit rfid_person bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_person rfid_person entities'.
  $permissions['eck edit rfid_person rfid_person entities'] = array(
    'name' => 'eck edit rfid_person rfid_person entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_person rfid_person entities own'.
  $permissions['eck edit rfid_person rfid_person entities own'] = array(
    'name' => 'eck edit rfid_person rfid_person entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_person bundles'.
  $permissions['eck list rfid_person bundles'] = array(
    'name' => 'eck list rfid_person bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_person rfid_person entities'.
  $permissions['eck list rfid_person rfid_person entities'] = array(
    'name' => 'eck list rfid_person rfid_person entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_person bundles'.
  $permissions['eck view rfid_person bundles'] = array(
    'name' => 'eck view rfid_person bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_person rfid_person entities'.
  $permissions['eck view rfid_person rfid_person entities'] = array(
    'name' => 'eck view rfid_person rfid_person entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_person rfid_person entities own'.
  $permissions['eck view rfid_person rfid_person entities own'] = array(
    'name' => 'eck view rfid_person rfid_person entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'manage rfid_person properties'.
  $permissions['manage rfid_person properties'] = array(
    'name' => 'manage rfid_person properties',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  return $permissions;
}
