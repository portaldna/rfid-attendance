<?php

/**
 * @file
 * Code for the RFID Person feature.
 */

include_once 'rfid_person.features.inc';

/**
 * Implements hook_dna_ecker_eck_entity_info().
 */
function rfid_person_dna_ecker_eck_entity_info()
{
  return [
    'rfid_person' => [
      'path' => 'people',
      'label_plural' => t('People'),
    ],
  ];
}

/**
 * Implements hook_dna_jsonapi_api_info().
 */
function rfid_person_dna_jsonapi_api_info()
{
  return [
    'rfid_person' => [
      'meta' => [
        'entityType' => 'rfid_person',
      ],
    ],
  ];
}

/**
 * Implements hook_dna_jsonapi_api_info_TYPE_alter().
 */
function rfid_person_dna_jsonapi_api_info_rfid_person_alter(&$info)
{
  $attr_tag = [
    'tag' => [
      'label' => t('RFID Tags'),
      'required' => 0,
      'cardinality' => -1,
      'type' => 'relationship',
      'weight' => 2,
    ],
  ];
  dna_utilities_array_insert($info['attributes'], 'last_name', $attr_tag);
  $info['relationships']['tag'] = 'rfid_tag';
}

/**
 * Implements hook_dna_jsonapi_entity_json_array_alter().
 */
function rfid_person_dna_jsonapi_entity_json_array_alter(&$json_array, $context)
{
  if ($context['entity_type'] == 'rfid_person') {
    if ($tag_entity_ids = rfid_person_get_person_tags($context['entity']->id)) {
      foreach ($tag_entity_ids as $tag_entity_id) {
        dna_jsonapi_json_array_add_relationship($json_array, 'tag', $tag_entity_id, 'rfid_tag', TRUE);
      }
    } else {
      dna_jsonapi_json_array_add_attribute($json_array, 'tag', t('Unassigned'));
    }
  }
}

/**
 * Implements hook_entity_insert().
 */
function rfid_person_entity_insert($entity, $entity_type)
{
  _rfid_person_tag_relationship_rebuild($entity, $entity_type);
}

/**
 * Implements hook_entity_update().
 */
function rfid_person_entity_update($entity, $entity_type)
{
  _rfid_person_tag_relationship_rebuild($entity, $entity_type);
}

/**
 * Implements hook_dna_datatables_table_info().
 */
function rfid_person_dna_datatables_table_info()
{
  return [
    'rfid_person' => [
      'jsonapi' => [
        'type' => 'rfid_person',
      ],
    ],
  ];
}

/**
 * Implements hook_dna_datatables_table_info_ID_alter().
 */
function rfid_person_dna_datatables_table_info_rfid_person_alter(&$info)
{
  $export_cols = [
    'id',
    'first_name',
    'last_name',
    'tag',
    'created',
    'changed',
    'status',
  ];

  foreach ($info['config']['columns'] as $col => &$col_info) {
    dna_datatables_set_column_visibility_available($col_info, in_array($col, $export_cols));
    dna_datatables_set_column_export($col_info, in_array($col, $export_cols));
  }

  $info['config']['columns']['tag']['visible'] = TRUE;
  $info['config']['columns']['tag']['searchable'] = TRUE;
  $info['config']['columns']['status']['visible'] = TRUE;
  $info['config']['columns']['status']['searchable'] = TRUE;
}

/**
 * Implements hook_dna_datatables_page_listing_info().
 */
function rfid_person_dna_datatables_page_listing_info()
{
  return [
    'rfid_person' => [
      'jsonapi' => [
        'type' => 'rfid_person',
      ],
      'menu' => [
        'type' => MENU_NORMAL_ITEM,
        'menu_name' => 'main-menu',
        'weight' => 3,
      ],
    ],
  ];
}

/**
 * Implements hook_dna_datatables_page_listing_build_rfid_person_alter().
 */
function rfid_person_dna_datatables_page_listing_build_rfid_person_alter(&$output)
{
  $output['#attached']['js'][] = [
    'data' => drupal_get_path('module', 'rfid_person') . '/js/rfid_person.js',
    'scope' => 'footer',
  ];
}

/**
 * Implements hook_eck_entity_ENTITY_TYPE_label().
 */
function rfid_person_eck_entity_rfid_person_label($entity, $entity_id)
{
  $first = $entity->field_rfid_person_first_name[LANGUAGE_NONE][0]['value'];
  $last = $entity->field_rfid_person_last_name[LANGUAGE_NONE][0]['value'];
  return "{$first} {$last}";
}

/**
 * Implements hook_form_alter().
 *
 * @todo allow multiple tags assigned to a person.
 */
function rfid_person_form_alter(&$form, &$form_state, $form_id)
{
  if (strpos($form_id, 'eck__entity__form_') === 0 && strpos($form_id, '_rfid_person_') !== FALSE) {
    if ($tags = entity_load('rfid_tag')) {
      $person = !empty($form_state['values']['entity']) ? $form_state['values']['entity'] : $form['#entity'];
      $options = [
        0 => t('Unassigned'),
      ];
      $default_value = [];
      $tags_in_use = [];

      foreach ($tags as $tag_entity_id => $tag) {
        $tag_wrapper = entity_metadata_wrapper('rfid_tag', $tag);
        $label = $tag_wrapper->label();

        if (!empty($tag->field_rfid_tag_assigned_to)) {
          $person_label = $tag_wrapper->field_rfid_tag_assigned_to->label();
          $label .= " (person: {$person_label})";

          if (!empty($person->id) && $tag->field_rfid_tag_assigned_to[LANGUAGE_NONE][0]['target_id'] == $person->id) {
            $default_value[] = $tag_entity_id;
          } else {
            $tags_in_use[$tag_entity_id] = ['value' => $tag_entity_id];
          }
        }

        $options[$tag_entity_id] = $label;
      }

      $form['rfid_tag'] = [
        '#title' => t('Tag'),
        '#type' => 'select',
        '#multiple' => TRUE,
        '#options' => $options,
        '#default_value' => $default_value,
        '#weight' => 10,
        '#attributes' => [
          'dna-select2-placeholder-value' => 0,
        ],
      ];

      $form['#submit'][] = 'rfid_person_form_assign_tag_submit';
    }

    $form['status'] = [
      '#title' => t('Active'),
      '#type' => 'checkbox',
      '#default_value' => $person->status,
      '#weight' => 20,
    ];
  }
}

/**
 * Submit handler: Person form assign tag.
 */
function rfid_person_form_assign_tag_submit($form, &$form_state)
{
  $person = $form_state['values']['entity'];
  $existing_tag_entity_ids = rfid_person_get_person_tags($person->id) ?: [];
  $new_tag_entity_ids = array_values($form_state['values']['rfid_tag']);

  $tags_to_remove = array_diff($existing_tag_entity_ids, $new_tag_entity_ids);
  $tags_to_add = array_diff($new_tag_entity_ids, $existing_tag_entity_ids);

  if ($tags_to_remove) {
    $tags = entity_load('rfid_tag', $tags_to_remove);

    foreach ($tags as $tag_entity_id => $tag) {
      $tag->field_rfid_tag_assigned_to = [];
      entity_save('rfid_tag', $tag);
    }
  }

  if ($tags_to_add) {
    $tags = entity_load('rfid_tag', array_values($tags_to_add));

    foreach ($tags as $tag_entity_id => $tag) {
      // No need to save a tag that is already assigned.
      if (!empty($tag->field_rfid_tag_assigned_to[LANGUAGE_NONE]) && $tag->field_rfid_tag_assigned_to[LANGUAGE_NONE][0]['target_id'] == $person->id) {
        continue;
      } else {
        if (!empty($tag->field_rfid_tag_assigned_to[LANGUAGE_NONE]) && $tag->field_rfid_tag_assigned_to[LANGUAGE_NONE][0]['target_id'] != $person->id) {
          drupal_set_message(t('Tag @tag has been re-assigned to @person.', [
            '@tag' => rfid_tag_eck_entity_rfid_tag_label($tag, $tag->id),
            '@person' => rfid_person_eck_entity_rfid_person_label($person, $person->id),
          ]), 'warning');
        }

        $tag->field_rfid_tag_assigned_to[LANGUAGE_NONE][0]['target_id'] = $person->id;
        entity_save('rfid_tag', $tag);
      }
    }
  }
}

/**
 * Get person tags.
 */
function rfid_person_get_person_tags($person_id = 0)
{
  $tags = &drupal_static(__FUNCTION__);

  if (!isset($tags)) {
    $sql = "
      SELECT
        field_rfid_tag_assigned_to_target_id AS uid,
        entity_id AS tag_entity_id
      FROM {field_data_field_rfid_tag_assigned_to}
    ";
    $results = db_query($sql)->fetchAll();

    foreach ($results as $result) {
      $tags[$result->uid][] = $result->tag_entity_id;
    }
  }

  if (!$person_id) {
    return $tags;
  }

  return !empty($tags[$person_id]) ? $tags[$person_id] : 0;
}

/**
 * Rebuild rfid_person json cache if tag is updated.
 */
function _rfid_person_tag_relationship_rebuild($tag, $entity_type)
{
  if ($entity_type != 'rfid_tag') {
    return;
  }

  $new_assigned_to = !empty($tag->field_rfid_tag_assigned_to[LANGUAGE_NONE]) ? $tag->field_rfid_tag_assigned_to[LANGUAGE_NONE][0]['target_id'] : 0;
  $old_assigned_to = (!empty($tag->original->field_rfid_tag_assigned_to[LANGUAGE_NONE])) ? $tag->original->field_rfid_tag_assigned_to[LANGUAGE_NONE][0]['target_id'] : 0;

  if ($new_assigned_to != $old_assigned_to) {
    dna_jsonapi_static_json($new_assigned_to, 'rfid_person');
    dna_jsonapi_static_json($old_assigned_to, 'rfid_person');

    // @todo work-around for regenerating caches without a change to the entity
    // change property.
    db_update('eck_rfid_person')
      ->fields(['changed' => REQUEST_TIME])
      ->condition('id', [$new_assigned_to, $old_assigned_to], 'IN')
      ->execute();
  }
}
