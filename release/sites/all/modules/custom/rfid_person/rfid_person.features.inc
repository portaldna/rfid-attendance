<?php
/**
 * @file
 * rfid_person.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function rfid_person_eck_bundle_info() {
  $items = array(
    'rfid_person_rfid_person' => array(
      'machine_name' => 'rfid_person_rfid_person',
      'entity_type' => 'rfid_person',
      'name' => 'rfid_person',
      'label' => 'Person',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function rfid_person_eck_entity_type_info() {
  $items = array(
    'rfid_person' => array(
      'name' => 'rfid_person',
      'label' => 'Person',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'status' => array(
          'label' => 'Status',
          'type' => 'integer',
          'behavior' => '',
        ),
        'uuid' => array(
          'label' => 'UUID',
          'type' => 'text',
          'behavior' => 'uuid',
        ),
      ),
    ),
  );
  return $items;
}
