/**
 * @file
 * Javascript file for RFID Tag.
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.theme.dna_datatables_colRender__rfid_person__id = function (tableID, column, data, type, rowData, meta) {
    return Drupal.theme('actionLink', rowData, 'edit', type, data);
  };

  Drupal.theme.dna_datatables_colRender__rfid_person__status = function (tableID, column, data, type, rowData, meta) {
    return parseInt(data) ? Drupal.t('Active') : Drupal.t('In-active');
  };

})(jQuery, Drupal, this, this.document);
