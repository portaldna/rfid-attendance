<?php
/**
 * @file
 * rfid_user.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rfid_user_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'rfid_core view rfid_log listing'.
  $permissions['rfid_core view rfid_log listing'] = array(
    'name' => 'rfid_core view rfid_log listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'rfid_core',
  );

  // Exported permission: 'rfid_core view rfid_person listing'.
  $permissions['rfid_core view rfid_person listing'] = array(
    'name' => 'rfid_core view rfid_person listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'rfid_core',
  );

  // Exported permission: 'rfid_core view rfid_reader listing'.
  $permissions['rfid_core view rfid_reader listing'] = array(
    'name' => 'rfid_core view rfid_reader listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'rfid_core',
  );

  // Exported permission: 'rfid_core view rfid_tag listing'.
  $permissions['rfid_core view rfid_tag listing'] = array(
    'name' => 'rfid_core view rfid_tag listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'rfid_core',
  );

  return $permissions;
}
