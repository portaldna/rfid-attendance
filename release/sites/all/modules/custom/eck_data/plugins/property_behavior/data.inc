<?php

/**
 * @file
 * The ECK data property behavior ctools plugin.
 */

$plugin = [
  'label' => 'Data',
  'entity_save' => 'eck_data_property_entity_insert',
];

/**
 * Entity Save.
 */
function eck_data_property_entity_insert($property, $vars) {
  $entity = $vars['entity'];

  if (empty($entity->data)) {
    $entity->data = [];
  }
}
