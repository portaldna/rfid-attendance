<?php

/**
 * @file
 * rfid_school_year.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function rfid_school_year_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function rfid_school_year_eck_bundle_info() {
  $items = array(
    'rfid_school_year_rfid_school_year' => array(
      'machine_name' => 'rfid_school_year_rfid_school_year',
      'entity_type' => 'rfid_school_year',
      'name' => 'rfid_school_year',
      'label' => 'School Year',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function rfid_school_year_eck_entity_type_info() {
  $items = array(
    'rfid_school_year' => array(
      'name' => 'rfid_school_year',
      'label' => 'School Year',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'uuid' => array(
          'label' => 'UUID',
          'type' => 'text',
          'behavior' => 'uuid',
        ),
        'status' => array(
          'label' => 'Status',
          'type' => 'integer',
          'behavior' => '',
        ),
      ),
    ),
  );
  return $items;
}
