<?php

/**
 * @file
 * rfid_school_year.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function rfid_school_year_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_school_year_label'.
  $field_bases['field_school_year_label'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_school_year_label',
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 255,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_school_year_start_end'.
  $field_bases['field_school_year_start_end'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_school_year_start_end',
    'indexes' => array(
      'value' => array(
        0 => 'value',
      ),
      'value2' => array(
        0 => 'value2',
      ),
    ),
    'locked' => 0,
    'module' => 'date',
    'settings' => array(
      'cache_count' => 4,
      'cache_enabled' => 0,
      'granularity' => array(
        'day' => 'day',
        'hour' => 0,
        'minute' => 0,
        'month' => 'month',
        'second' => 0,
        'year' => 'year',
      ),
      'timezone_db' => '',
      'todate' => 'required',
      'tz_handling' => 'none',
    ),
    'translatable' => 0,
    'type' => 'datestamp',
  );

  return $field_bases;
}
