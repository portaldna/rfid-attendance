/**
 * @file
 * Javascript file for RFID School Year module.
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.theme.dna_datatables_colRender__rfid_school_year__school_year_label = function (tableID, column, data, type, rowData, meta) {
    if (type != 'display') {
      return data;
    }

    return Drupal.theme('actionLink', rowData, 'edit', type, data);
  };

  Drupal.theme.dna_datatables_colRender__rfid_school_year__school_year_start_end = function (tableID, column, data, type, rowData, meta) {
    if (type != 'display') {
      return data.value;
    }

    return rowData.meta.label;
  };

})(jQuery, Drupal, this, this.document);
