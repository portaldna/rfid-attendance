<?php

/**
 * @file
 * rfid_school_year.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function rfid_school_year_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_rfid_school_year__rfid_school_year';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'redirect' => array(
          'weight' => '2',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_rfid_school_year__rfid_school_year'] = $strongarm;

  return $export;
}
