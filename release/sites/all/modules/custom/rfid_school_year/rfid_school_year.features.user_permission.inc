<?php

/**
 * @file
 * rfid_school_year.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rfid_school_year_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'eck add rfid_school_year bundles'.
  $permissions['eck add rfid_school_year bundles'] = array(
    'name' => 'eck add rfid_school_year bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck add rfid_school_year rfid_school_year entities'.
  $permissions['eck add rfid_school_year rfid_school_year entities'] = array(
    'name' => 'eck add rfid_school_year rfid_school_year entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_school_year bundles'.
  $permissions['eck administer rfid_school_year bundles'] = array(
    'name' => 'eck administer rfid_school_year bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_school_year rfid_school_year entities'.
  $permissions['eck administer rfid_school_year rfid_school_year entities'] = array(
    'name' => 'eck administer rfid_school_year rfid_school_year entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_school_year bundles'.
  $permissions['eck delete rfid_school_year bundles'] = array(
    'name' => 'eck delete rfid_school_year bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_school_year rfid_school_year entities'.
  $permissions['eck delete rfid_school_year rfid_school_year entities'] = array(
    'name' => 'eck delete rfid_school_year rfid_school_year entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_school_year rfid_school_year entities own'.
  $permissions['eck delete rfid_school_year rfid_school_year entities own'] = array(
    'name' => 'eck delete rfid_school_year rfid_school_year entities own',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_school_year bundles'.
  $permissions['eck edit rfid_school_year bundles'] = array(
    'name' => 'eck edit rfid_school_year bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_school_year rfid_school_year entities'.
  $permissions['eck edit rfid_school_year rfid_school_year entities'] = array(
    'name' => 'eck edit rfid_school_year rfid_school_year entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_school_year rfid_school_year entities own'.
  $permissions['eck edit rfid_school_year rfid_school_year entities own'] = array(
    'name' => 'eck edit rfid_school_year rfid_school_year entities own',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_school_year bundles'.
  $permissions['eck list rfid_school_year bundles'] = array(
    'name' => 'eck list rfid_school_year bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_school_year rfid_school_year entities'.
  $permissions['eck list rfid_school_year rfid_school_year entities'] = array(
    'name' => 'eck list rfid_school_year rfid_school_year entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_school_year bundles'.
  $permissions['eck view rfid_school_year bundles'] = array(
    'name' => 'eck view rfid_school_year bundles',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_school_year rfid_school_year entities'.
  $permissions['eck view rfid_school_year rfid_school_year entities'] = array(
    'name' => 'eck view rfid_school_year rfid_school_year entities',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_school_year rfid_school_year entities own'.
  $permissions['eck view rfid_school_year rfid_school_year entities own'] = array(
    'name' => 'eck view rfid_school_year rfid_school_year entities own',
    'roles' => array(),
    'module' => 'eck',
  );

  // Exported permission: 'manage rfid_school_year properties'.
  $permissions['manage rfid_school_year properties'] = array(
    'name' => 'manage rfid_school_year properties',
    'roles' => array(),
    'module' => 'eck',
  );

  return $permissions;
}
