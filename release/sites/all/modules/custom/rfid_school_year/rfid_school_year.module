<?php

/**
 * @file
 * Code for the School Year feature.
 */

include_once 'rfid_school_year.features.inc';

/**
 * Implements hook_dna_ecker_eck_entity_info().
 */
function rfid_school_year_dna_ecker_eck_entity_info() {
  return [
    'rfid_school_year' => [
      'path' => 'school-years',
      'label_plural' => t('School Years'),
    ],
  ];
}

/**
 * Implements hook_dna_jsonapi_api_info().
 */
function rfid_school_year_dna_jsonapi_api_info() {
  return [
    'rfid_school_year' => [
      'meta' => [
        'entityType' => 'rfid_school_year',
      ],
    ],
  ];
}

/**
 * Implements hook_eck_entity_ENTITY_TYPE_label().
 */
function rfid_school_year_eck_entity_rfid_school_year_label($entity, $entity_id) {
  if (!empty($entity->field_school_year_label[LANGUAGE_NONE][0]['value'])) {
    return $entity->field_school_year_label[LANGUAGE_NONE][0]['value'];
  }
  else {
    return _rfid_school_year_start_end_string($entity);
  }
}

/**
 * Implements hook_dna_datatables_table_info().
 */
function rfid_school_year_dna_datatables_table_info() {
  return [
    'rfid_school_year' => [
      'jsonapi' => [
        'type' => 'rfid_school_year',
      ],
    ],
  ];
}

/**
 * Implements hook_dna_datatables_table_info_ID_alter().
 */
function rfid_school_year_dna_datatables_table_info_rfid_school_year_alter(&$info) {
  $info['config']['columns']['school_year_start_end']['weight'] = 1;
  $info['config']['columns']['school_year_label']['weight'] = 0;
}

/**
 * Implements hook_dna_datatables_page_listing_info().
 */
function rfid_school_year_dna_datatables_page_listing_info() {
  return [
    'rfid_school_year' => [
      'jsonapi' => [
        'type' => 'rfid_school_year',
      ],
      'menu' => [
        'type' => MENU_NORMAL_ITEM,
        'menu_name' => 'main-menu',
        'weight' => 4,
      ],
    ],
  ];
}

/**
 * Implements hook_dna_datatables_page_listing_build_ID_alter().
 */
function rfid_school_year_dna_datatables_page_listing_build_rfid_school_year_alter(&$output) {
  $output['#attached']['js'][] = [
    'data' => drupal_get_path('module', 'rfid_school_year') . '/js/rfid_school_year.js',
    'scope' => 'footer',
  ];
}

/**
 * Helper: Start/end data string.
 */
function _rfid_school_year_start_end_string($school_year) {
  $start = $school_year->field_school_year_start_end[LANGUAGE_NONE][0]['value'];
  $end = $school_year->field_school_year_start_end[LANGUAGE_NONE][0]['value2'];
  return format_date($start, 'custom', 'm/d/Y') . ' - ' . format_date($end, 'custom', 'm/d/Y');
}
