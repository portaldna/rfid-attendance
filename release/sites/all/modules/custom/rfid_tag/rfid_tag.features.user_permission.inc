<?php
/**
 * @file
 * rfid_tag.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function rfid_tag_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'dna_datatables view rfid_tag listing'.
  $permissions['dna_datatables view rfid_tag listing'] = array(
    'name' => 'dna_datatables view rfid_tag listing',
    'roles' => array(
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'dna_datatables',
  );

  // Exported permission: 'eck add rfid_tag bundles'.
  $permissions['eck add rfid_tag bundles'] = array(
    'name' => 'eck add rfid_tag bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck add rfid_tag rfid_tag entities'.
  $permissions['eck add rfid_tag rfid_tag entities'] = array(
    'name' => 'eck add rfid_tag rfid_tag entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_tag bundles'.
  $permissions['eck administer rfid_tag bundles'] = array(
    'name' => 'eck administer rfid_tag bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck administer rfid_tag rfid_tag entities'.
  $permissions['eck administer rfid_tag rfid_tag entities'] = array(
    'name' => 'eck administer rfid_tag rfid_tag entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_tag bundles'.
  $permissions['eck delete rfid_tag bundles'] = array(
    'name' => 'eck delete rfid_tag bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_tag rfid_tag entities'.
  $permissions['eck delete rfid_tag rfid_tag entities'] = array(
    'name' => 'eck delete rfid_tag rfid_tag entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck delete rfid_tag rfid_tag entities own'.
  $permissions['eck delete rfid_tag rfid_tag entities own'] = array(
    'name' => 'eck delete rfid_tag rfid_tag entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_tag bundles'.
  $permissions['eck edit rfid_tag bundles'] = array(
    'name' => 'eck edit rfid_tag bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_tag rfid_tag entities'.
  $permissions['eck edit rfid_tag rfid_tag entities'] = array(
    'name' => 'eck edit rfid_tag rfid_tag entities',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck edit rfid_tag rfid_tag entities own'.
  $permissions['eck edit rfid_tag rfid_tag entities own'] = array(
    'name' => 'eck edit rfid_tag rfid_tag entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_tag bundles'.
  $permissions['eck list rfid_tag bundles'] = array(
    'name' => 'eck list rfid_tag bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck list rfid_tag rfid_tag entities'.
  $permissions['eck list rfid_tag rfid_tag entities'] = array(
    'name' => 'eck list rfid_tag rfid_tag entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_tag bundles'.
  $permissions['eck view rfid_tag bundles'] = array(
    'name' => 'eck view rfid_tag bundles',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_tag rfid_tag entities'.
  $permissions['eck view rfid_tag rfid_tag entities'] = array(
    'name' => 'eck view rfid_tag rfid_tag entities',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'eck view rfid_tag rfid_tag entities own'.
  $permissions['eck view rfid_tag rfid_tag entities own'] = array(
    'name' => 'eck view rfid_tag rfid_tag entities own',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  // Exported permission: 'manage rfid_tag properties'.
  $permissions['manage rfid_tag properties'] = array(
    'name' => 'manage rfid_tag properties',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'eck',
  );

  return $permissions;
}
