<?php
/**
 * @file
 * rfid_tag.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function rfid_tag_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'rfid_tag-rfid_tag-field_rfid_tag_assigned_to'.
  $field_instances['rfid_tag-rfid_tag-field_rfid_tag_assigned_to'] = array(
    'bundle' => 'rfid_tag',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => 0,
          'link' => 1,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'rfid_tag',
    'field_name' => 'field_rfid_tag_assigned_to',
    'label' => 'Assigned To',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'rfid_tag-rfid_tag-field_rfid_tag_id'.
  $field_instances['rfid_tag-rfid_tag-field_rfid_tag_id'] = array(
    'bundle' => 'rfid_tag',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Provide a unique identifier for this tag.',
    'display' => array(
      'default' => array(
        'label' => 'inline',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'rfid_tag',
    'field_name' => 'field_rfid_tag_id',
    'label' => 'Tag ID',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 0,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Assigned To');
  t('Provide a unique identifier for this tag.');
  t('Tag ID');

  return $field_instances;
}
