/**
 * @file
 * Javascript file for RFID Tag.
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.theme.dna_datatables_colRender__rfid_tag__id = function (tableID, column, data, type, rowData, meta) {
    return Drupal.theme('actionLink', rowData, 'edit', type);
  };

})(jQuery, Drupal, this, this.document);
