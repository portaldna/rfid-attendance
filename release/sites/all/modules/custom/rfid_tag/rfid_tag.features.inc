<?php
/**
 * @file
 * rfid_tag.features.inc
 */

/**
 * Implements hook_eck_bundle_info().
 */
function rfid_tag_eck_bundle_info() {
  $items = array(
    'rfid_tag_rfid_tag' => array(
      'machine_name' => 'rfid_tag_rfid_tag',
      'entity_type' => 'rfid_tag',
      'name' => 'rfid_tag',
      'label' => 'RFID Tag',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function rfid_tag_eck_entity_type_info() {
  $items = array(
    'rfid_tag' => array(
      'name' => 'rfid_tag',
      'label' => 'RFID Tag',
      'properties' => array(
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
        'changed' => array(
          'label' => 'Changed',
          'type' => 'integer',
          'behavior' => 'changed',
        ),
        'status' => array(
          'label' => 'Status',
          'type' => 'integer',
          'behavior' => '',
        ),
        'uuid' => array(
          'label' => 'UUID',
          'type' => 'text',
          'behavior' => 'uuid',
        ),
      ),
    ),
  );
  return $items;
}
