<?php

/**
 * @file
 * Code for the RFID Tag feature.
 */

include_once 'rfid_tag.features.inc';

/**
 * Implements hook_dna_ecker_eck_entity_info().
 */
function rfid_tag_dna_ecker_eck_entity_info()
{
  return [
    'rfid_tag' => [
      'path' => 'rfid-tags',
      'label_plural' => t('RFID Tags'),
    ],
  ];
}

/**
 * Implements hook_dna_jsonapi_api_info().
 */
function rfid_tag_dna_jsonapi_api_info()
{
  return [
    'rfid_tag' => [
      'meta' => [
        'entityType' => 'rfid_tag',
      ],
    ],
  ];
}

/**
 * Implements hook_dna_jsonapi_entity_json_array_alter().
 */
function rfid_tag_dna_jsonapi_entity_json_array_alter(&$json_array, $context)
{
  if ($context['entity_type'] == 'rfid_tag') {
    if (empty($json_array['relationships']['assigned_to']) || is_null($json_array['relationships']['assigned_to']['data'])) {
      dna_jsonapi_json_array_add_attribute($json_array, 'assigned_to', t('Unassigned'));
    }
  }
}

/**
 * Implements hook_dna_datatables_table_info().
 */
function rfid_tag_dna_datatables_table_info()
{
  return [
    'rfid_tag' => [
      'jsonapi' => [
        'type' => 'rfid_tag',
      ],
    ],
  ];
}

/**
 * Implements hook_dna_datatables_table_info_rfid_tag_alter().
 */
function rfid_tag_dna_datatables_table_info_rfid_tag_alter(&$info)
{
  $export_cols = [
    'id',
    'assigned_to',
    'created',
    'changed',
    'status',
  ];

  foreach ($info['config']['columns'] as $col => &$col_info) {
    dna_datatables_set_column_visibility_available($col_info, in_array($col, $export_cols));
    dna_datatables_set_column_export($col_info, in_array($col, $export_cols));
  }
}

/**
 * Implements hook_dna_datatables_page_listing_info().
 */
function rfid_tag_dna_datatables_page_listing_info()
{
  return [
    'rfid_tag' => [
      'jsonapi' => [
        'type' => 'rfid_tag',
      ],
      'menu' => [
        'type' => MENU_NORMAL_ITEM,
        'menu_name' => 'main-menu',
        'weight' => 1,
      ],
    ],
  ];
}

/**
 * Implements hook_dna_datatables_page_listing_build_rfid_tag_alter().
 */
function rfid_tag_dna_datatables_page_listing_build_rfid_tag_alter(&$output)
{
  $output['#attached']['js'][] = [
    'data' => drupal_get_path('module', 'rfid_tag') . '/js/rfid_tag.js',
    'scope' => 'footer',
  ];
}

/**
 * Implements hook_eck_entity_ENTITY_TYPE_label().
 */
function rfid_tag_eck_entity_rfid_tag_label($entity, $entity_id)
{
  return $entity->field_rfid_tag_id[LANGUAGE_NONE][0]['value'];
}

/**
 * Implements hook_form_alter().
 */
function rfid_tag_form_alter(&$form, &$form_state, $form_id)
{
  if (strpos($form_id, 'eck__entity__form_') === 0 && strpos($form_id, '_rfid_tag_') !== FALSE) {
    $assigned_tags = rfid_tag_get_tag_assignments();
    $options = &$form['field_rfid_tag_assigned_to'][LANGUAGE_NONE]['#options'];
    asort($options);
    $options['_none'] = t('Unassigned');

    $form['field_rfid_tag_assigned_to'][LANGUAGE_NONE]['#attributes']['placeholder'] = t('Assign a person');

    if ($assigned_tags) {
      $assigned_person_tags = [];

      foreach ($assigned_tags as $tag_entity_id => $tag_assignment) {
        $assigned_person_tags[$tag_assignment->person_entity_id][$tag_assignment->tag_id] = $tag_assignment->tag_id;
      }

      foreach ($assigned_person_tags as $person_entity_id => $tag_ids) {
        if (!isset($options[$person_entity_id])) {
          continue;
        }
        $options[$person_entity_id] .= ' (tags: ' . implode(', ', $tag_ids) . ')';
      }
    }
  }
}

/**
 * Get tag assignments.
 */
function rfid_tag_get_tag_assignments($reset = FALSE)
{
  $tags = &drupal_static(__FUNCTION__);

  if (!isset($tags) || $reset) {
    $sql = "
      SELECT
        ta.entity_id AS tag_entity_id,
        ta.field_rfid_tag_assigned_to_target_id AS person_entity_id,
        ti.field_rfid_tag_id_value AS tag_id
      FROM {field_data_field_rfid_tag_assigned_to} ta
      INNER JOIN {field_data_field_rfid_tag_id} ti ON ti.entity_id = ta.entity_id
    ";
    $tags = db_query($sql)->fetchAllAssoc('tag_entity_id');
  }

  return $tags;
}
