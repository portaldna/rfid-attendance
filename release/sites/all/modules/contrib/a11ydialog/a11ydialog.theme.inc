<?php

/**
 * @file
 * Theme functions for the A11y Dialog module.
 */

/**
 * Theme function: A11y Dialog.
 */
function theme_a11ydialog($variables) {
  $dialog_id = $variables['id'];
  $dialog_class = drupal_clean_css_identifier($dialog_id);
  $dialog_title = $variables['title'];
  $dialog_content = is_array($variables['content']) ? drupal_render($variables['content']) : $variables['content'];
  $a11ydialog_class = 'a11ydialog';

  // Dialog container related notes:
  // - It is not the actual dialog window, just the container with which the
  //   script interacts.
  // - It has to have the `aria-hidden="true"` attribute.
  $container_attributes = !empty($variables['container_attributes']) ? $variables['container_attributes'] : [];
  $container_attributes['id'] = $dialog_id;
  $container_attributes['aria-hidden'] = 'true';
  $container_attributes['class'][] = $a11ydialog_class;
  $container_attributes['class'][] = $a11ydialog_class . '--' . $dialog_class;

  $output = '<div' . drupal_attributes($container_attributes) . '>';

  // Overlay related notes:
  // - It has to have the `tabindex="-1"` attribute.
  // - It doesn’t have to have the `data-a11y-dialog-hide` attribute, however
  //   this is recommended. It hides the dialog when clicking outside of it.
  $overlay_hide = (isset($variables['hide_on_overlay']) && $variables['hide_on_overlay']) ? 'data-a11y-dialog-hide ' : '';
  $output .= '<div tabindex="-1" ' . $overlay_hide . 'class="' . $a11ydialog_class . '__overlay"></div>';

  // Dialog window content related notes:
  // - It is the actual visual dialog element.
  // - It has to have the `role="dialog"` attribute.
  // - It doesn’t have to have the `aria-labelledby` attribute however this is
  //   recommended. It should match the `id` of the dialog title.
  // - It doesn’t have to have a direct child with the `role="document"`,
  //   however this is recommended.
  $dialog_title_id = $dialog_id . '__title';
  $dialog_attributes = [
    'role' => 'dialog',
    'aria-labelledby' => $dialog_title_id,
    'class' => [
      $a11ydialog_class . '__dialog',
    ],
  ];
  $output .= '<div' . drupal_attributes($dialog_attributes) . '>';

  $document_attributes = [
    'role' => 'document',
    'class' => [
      $a11ydialog_class . '__document',
      'clearfix',
    ],
  ];
  $output .= '<div role="document" class="' . $a11ydialog_class . '__document clearfix">';
  // Dialog title related notes:
  // - It does not have to have the `tabindex="0"` attribute however it is
  //   recommended so the dialog doesn’t jump directly to a field, displaying
  //   keyboard on mobiles.
  $dialog_title_attributes = [
    'id' => $dialog_title_id,
    'tabindex' => 0,
    'class' => [
      $a11ydialog_class . '__title',
    ],
  ];
  // While we require a title to allow screen readers to read the dialog title,
  // we may not want to display them.
  if ($variables['hide_title']) {
    $dialog_title_attributes['class'][] = 'a11ydialog__title--invisible';
    $dialog_title_attributes['class'][] = 'element-invisible';
  }
  $output .= '<h1' . drupal_attributes($dialog_title_attributes) . '>' . $dialog_title . '</h1>';

  // Here lives the main content of the dialog.
  $output .= '<div class="a11ydialog__content clearfix">' . $dialog_content . '</div>';

  // Closing button related notes:
  // - It does have to have the `type="button"` attribute.
  // - It does have to have the `data-a11y-dialog-hide` attribute.
  // - It does have to have an aria-label attribute if you use an icon as
  //   content.
  $output .= '<button type="button" data-a11y-dialog-hide aria-label="Close this dialog window" class="' . $a11ydialog_class . '__close-button">';
  $output .= '&times;';
  $output .= '</button>';
  $output .= '</div>';
  $output .= '</div>';
  $output .= '</div>';

  return $output;
}

/**
 * Theme function: A11y Dialog Wrapper.
 */
function theme_a11ydialog_wrapper($variables) {
  $element = $variables['element'];

  if (empty($element['#a11ydialog_variables'])) {
    return $variables['element']['#children'];
  }

  $a11ydialog_variables = $element['#a11ydialog_variables'];
  $a11ydialog_variables['content'] = $variables['element']['#children'];

  return theme('a11ydialog', $a11ydialog_variables);
}
