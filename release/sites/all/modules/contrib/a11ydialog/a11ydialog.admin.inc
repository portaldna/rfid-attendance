<?php
/**
 * @file
 * Admin page callbacks for the A11y Dialog module.
 */

/**
 * Configuration form.
 */
function a11ydialog_configuration_form($form, &$form_state) {
  $form['a11ydialog_include'] = [
    '#type' => 'radios',
    '#title' => t('Choose when to automatically load the a11y-dialog library.'),
    '#options' => [
      A11YDIALOG_INCLUDE_EVERYWHERE => t('Include on every page'),
      A11YDIALOG_INCLUDE_NONE => t('Do not include'),
      A11YDIALOG_INCLUDE_NO_ADMIN => t('Include only on front end pages'),
      A11YDIALOG_INCLUDE_ADMIN => t('Include only on admin pages'),
    ],
    '#default_value' => variable_get('a11ydialog_include', A11YDIALOG_INCLUDE_DEFAULT),
  ];

  $form['a11ydialog_main_selector'] = [
    '#type' => 'textfield',
    '#title' => t('Content wrapper HTML ID'),
    '#description' => t('A11y Dialog looks for an HTML ID selector that wraps the main body of content. See !link for more information', [
      '!link' => l(t('A11y Dialog documentation'), "https://github.com/edenspiekermann/a11y-dialog/blob/2.5.7/README.md#javascript", ['external' => TRUE]),
    ]),
    '#field_prefix' => '#',
    '#default_value' => variable_get('a11ydialog_main_selector', 'main'),
  ];

  $form['a11ydialog_include_hide_css'] = [
    '#type' => 'checkbox',
    '#title' => t('Include css that initially hides dialogs'),
    '#description' => t('The A11y Dialog Library does not do anything to hide closed dialogs. This simply includes the minimum css required to initially hide closed dialogs. See !link.', [
      '!link' => l(t('A11y Dialog documentation'), "https://github.com/edenspiekermann/a11y-dialog/blob/2.5.7/README.md#css", ['external' => TRUE]),
    ]),
    '#default_value' => variable_get('a11ydialog_include_hide_css', TRUE),
  ];

  $form['a11ydialog_include_basic_css'] = [
    '#type' => 'checkbox',
    '#title' => t('Include css for basic styling'),
    '#description' => t('The A11y Dialog Library does not include any css. Check this option to add some basic styling to the modals.'),
    '#default_value' => variable_get('a11ydialog_include_basic_css', TRUE),
  ];

  $form['a11ydialog_create_on_query'] = [
    '#type' => 'checkbox',
    '#title' => t('Create new dialogs from query string'),
    '#description' => t('Dialogs may be triggered from the query string. (e.g. mysite.com?a11ydialog=my-dialog-id)<br/>Leave this box unchecked and only dialogs that are already created may be triggered. Check this box to automatically convert the target into a dialog and open.'),
    '#default_value' => variable_get('a11ydialog_create_on_query', FALSE),
  ];

  return system_settings_form($form);
}

/**
 * Admin callback: Demo page.
 */
function a11ydialog_demos_page() {
  $output = [];

  $output['intro'] = [
    '#prefix' => '<div style="margin: 20px 0;">',
    '#suffix' => '</div>',
    '#markup' => t('This page demonstrates several use-cases for the A11y Dialog module / library. All scripts / css are included on this demo page regardless of the configuration. In the page tabs there is a list of all active themes to show what the dialogs will look like in each theme.'),
  ];

  $output['demos'] = [
    '#sorted' => TRUE,
  ];

  // Block Demo.
  $output['demos']['block'] = [
    '#type' => 'fieldset',
    '#title' => t('Block Demo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'intro' => [
      '#markup' => t('This module dynamically creates a custom A11y Dialogs region. Any block can be added to this region using the Block Admin UI, Context module, etc. When these blocks are rendered, they are wrapped with the necessary markup to turn them into dialogs.')
      . '<br/><br/>'
      . t('The dialog title does attempt to set itself based on the block title and then adds an .element-invisible class. This is necessary to allow screen readers to read the dialog title without making each block contain the necessary aria attributes. <strong>It does potentially cause screen reader issues if the block title is blank.</strong> The dialog title can be updated using data attributes on the triggering element. See the data attributes demo below.')
      . '<br/><br/>'
      . t('To trigger a modal simply add the data-a11y-dialog-show="dialog_html_id" attribute to an element. Ideally this should be a button element, however other elements work as well.'),
    ],
    'code' => [
      '#prefix' => '<pre style="margin: 20px 0; white-space: pre-wrap;">',
      '#suffix' => '</pre>',
      '#markup' => t('@code', [
        '@code' => '<button type="button" data-a11y-dialog-show="a11ydialog_demo_block">Open Dialog</button>',
      ]),
    ],
    'button' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog_demo_block',
      ],
      '#value' => t('Open Dialog'),
    ],
  ];

  // Data Attributes Demo.
  $data_content = t('This is the same dialog from part 1 but the content has been changed using the data-a11ydialog-content attribute.');

  $output['demos']['data_attributes'] = [
    '#type' => 'fieldset',
    '#title' => t('Data Attributers Demo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'intro' => [
      '#markup' => t('There may be times a simple dialog is desired on a page but creating a block and configuring it to only show on that page is overkill. When the script is initializing it will look for all dialogs referenced by the data-a11y-dialog-show attribute. If one does not exist then a blank one is created.')
      . '<br/><br/>'
      . t('When a user clicks on a trigger that contains the data-a11ydialog-title and / or data-a11ydialog-content attributes, the dialog title and / or content will be replaced.')
      . '<pre style="margin: 20px 0; white-space: pre-wrap;">'
      . t('@code', [
        '@code' => '<button type="button" data-a11y-dialog-show="a11ydialog-demo-data-attr" data-a11ydialog-title="Data Attributes Demo Dialog - No Content">Open Dialog - Part 1</button>'])
      . '<br/><br/>'
      . t('@code', [
        '@code' => '<button type="button" data-a11y-dialog-show="a11ydialog-demo-data-attr" data-a11ydialog-content="' . $data_content . '">Open Dialog - Part 2</button>'])
      . '</pre>',
    ],
    'button1' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-data-attr',
        'data-a11ydialog-title' => t('Data Attributes Demo Dialog - No Content'),
      ],
      '#value' => t('Open Dialog - Part 1'),
    ],
    'button2' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-data-attr',
        'data-a11ydialog-content' => $data_content,
      ],
      '#value' => t('Open Dialog - Part 2'),
    ],
  ];

  // AJAX Demo.
  $output['demos']['ajax'] = [
    '#type' => 'fieldset',
    '#title' => t('AJAX Demo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'intro' => [
      '#markup' => t('Dialog content can also be loaded using the data-a11ydialog-get-url attribute. That attribute should contain the GET url.')
      . '<br/><br/>'
      . t('Note that by default this only changes the "content" area of the dialog and not the title. The dialog title can be updated using the data-a11ydialog-title attribute as noted above. Alternatively a data-a11ydialog-content-target attribute can be added to change what the ajax response is loaded. The options for that include "document" (title + content) and "content" (default option).')
      . '<pre style="margin: 20px 0; white-space: pre-wrap;">'
      . t('@code', [
        '@code' => '<button type="button" data-a11y-dialog-show="a11ydialog-demo-ajax" data-a11ydialog-title="AJAX Demo Dialog - No Content">Open Dialog - Part 1</button>'])
      . '<br/><br/>'
      . t('@code', [
        '@code' => '<button type="button" data-a11y-dialog-show="a11ydialog-demo-ajax" data-a11ydialog-get-url="/sites/all/modules/contrib/a11ydialog/demos/a11ydialog.ajax-demo.html">Open Dialog - Load</button>'])
      . '<br/><br/>'
      . t('@code', [
        '@code' => '<button type="button" data-a11y-dialog-show="a11ydialog-demo-ajax" data-a11ydialog-get-once-url="/sites/all/modules/contrib/a11ydialog/demos/a11ydialog.ajax-once-demo.html">Open Dialog - Load Once</button>'])
      . '</pre>',
    ],
    'button1' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-ajax',
        'data-a11ydialog-title' => t('AJAX Demo Dialog - No Content'),
      ],
      '#value' => t('Open Dialog - Part 1'),
    ],
    'button2' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-ajax',
        'data-a11ydialog-get-url' => '/sites/all/modules/contrib/a11ydialog/demos/a11ydialog.ajax-demo.html',
      ],
      '#value' => t('Open Dialog - Load'),
    ],
    'button3' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-ajax',
        'data-a11ydialog-get-once-url' => '/sites/all/modules/contrib/a11ydialog/demos/a11ydialog.ajax-once-demo.html',
      ],
      '#value' => t('Open Dialog - Load Once'),
    ],
  ];

  // Theme function (a11ydialog).
  $output['demos']['theme_a11ydialog'] = [
    '#type' => 'fieldset',
    '#title' => t('Theme Function Demo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'intro' => [
      '#markup' => t('Creating a dialog in code is relatively simple. A theme function is included with this module to generate the necessary markup.')
      . '<pre style="margin: 20px 0; white-space: pre-wrap;">'
      . t('@code', [
        '@code' => "
'mydialog' => array(
  '#theme' => 'a11ydialog',
  '#id' => 'a11ydialog-demo-basic',
  '#title' => t('Theme Dialog Demo'),
  '#content' => t('This dialog uses the a11ydialog theme function to render a dialog as part of a render array.'),
  '#container_attributes' => array('data-blah' => 'blah-data'),
),
",
      ])
      . '</pre>',
    ],
    'dialog' => [
      '#theme' => 'a11ydialog',
      '#id' => 'a11ydialog-demo-basic',
      '#title' => t('Theme Dialog Demo'),
      '#content' => t('This dialog uses the a11ydialog theme function to render a dialog as part of a render array.'),
      '#container_attributes' => ['data-blah' => 'blah-data'],
    ],
    'button' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-basic',
      ],
      '#value' => t('Open Dialog'),
    ],
  ];

  // @todo document or include script on how to include a form submit button in
  // a form element dialog.
  $output['demos']['form_element'] = [
    '#type' => 'fieldset',
    '#title' => t('Form Element Demo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'intro' => [
      '#markup' => t('Maybe we want to render a form element in a modal. This can be accomplished by using the a11ydialog_wrapper theme function. Just remember that a trigger needs to be added. You can add values to each of the three text fields and submit the form to see that the submit handler picks them up.')
      . '<br/><br/>'
      . t('Note: Dialogs "should" be a child of the body tag as all siblings get the aria-hidde="true" attribute. However moving a form element outside of the form tag means it will not get submitted. So when the dialog is shown the form element / dialog is moved and when hidden it is restored. This also means the dialog should not have a submitting button.  Whole forms can be in the dialog and contain a submitting button.  Should you need to include a submitting button with a form element dialog then custom javascript will be required.')
      . '<br/><br/>'
      . t("Should you want to keep the dialog in-place where the server rendered it, then add data-a11ydialog-noswap to the container attributes."),
    ],
    'code' => [
      '#prefix' => '<pre style="margin: 20px 0; white-space: pre-wrap;">',
      '#suffix' => '</pre>',
      '#markup' => t('@code', [
        '@code' => "
\$form['text1'] = array(
  '#type' => 'textfield',
  '#title' => t('Normal Textfield'),
);
\$form['text2'] = array(
  '#type' => 'textfield',
  '#title' => t('Theme Wrapper Textfield'),
  '#theme_wrappers' => array('form_element', 'a11ydialog_wrapper'),
  '#a11ydialog_variables' => array(
    'id' => 'a11ydialog-demo-form-element',
    'title' => t('Form Element Dialog Demo'),
  ),
);
\$form['text3'] = array(
  '#type' => 'textfield',
  '#title' => t('Another Normal Textfield'),
);
\$form['text4'] = array(
  '#prefix' => '<div id=\"a11ydialog-demo-inline\" data-a11ydialog-noswap>',
  '#suffix' => '</div>',
  '#type' => 'textfield',
  '#title' => t('Textfield Inline Initialized'),
);
",
      ]),
    ],
    'button' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-form-element',
      ],
      '#value' => t('Open Textfield Dialog'),
    ],
    'button2' => [
      '#type' => 'button',
      '#attributes' => [
        'data-a11y-dialog-show' => 'a11ydialog-demo-inline',
        'data-a11ydialog-title' => t('Inline Dialog Demo'),
      ],
      '#value' => t('Open Inline Textfield'),
    ],
    'form' => drupal_get_form('a11ydialog_demo_form'),
  ];

  $output['demos']['query'] = [
    '#type' => 'fieldset',
    '#title' => t('Querystring Demo'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    'intro' => [
      '#markup' => t('Dialogs may be triggered from the querystring. Simply put a11ydialog=my-dialog-id into the querystring and the dialog will open. This allows you to link directly to a page and trigger a dialog. It also lets you trigger a dialog from a link <a href="?a11ydialog=a11ydialog_demo_block">Open Block Dialog</a> without the data-a11y-dialog-show attribute.'),
    ],
  ];

  return $output;
}

/**
 * Form: Demo form.
 */
function a11ydialog_demo_form($form, &$form_state) {
  $form['text1'] = [
    '#type' => 'textfield',
    '#title' => t('Normal Textfield'),
  ];
  $form['text2'] = [
    '#type' => 'textfield',
    '#title' => t('Theme Wrapper Textfield'),
    '#theme_wrappers' => ['form_element', 'a11ydialog_wrapper'],
    '#a11ydialog_variables' => [
      'id' => 'a11ydialog-demo-form-element',
      'title' => t('Form Element Dialog Demo'),
    ],
  ];
  $form['text3'] = [
    '#type' => 'textfield',
    '#title' => t('Another Normal Textfield'),
  ];
  $form['text4'] = [
    '#prefix' => '<div id="a11ydialog-demo-inline" data-a11ydialog-noswap>',
    '#suffix' => '</div>',
    '#type' => 'textfield',
    '#title' => t('Textfield Inline Initialized'),
  ];

  $form['actions'] = ['#type' => 'actions'];
  $form['actions']['submit'] = [
    '#type' => 'submit',
    '#value' => t('Submit'),
  ];

  return $form;
}

/**
 * Submit handler: Demo form.
 */
function a11ydialog_demo_form_submit($form, &$form_state) {
  foreach (['text1', 'text2', 'text3'] as $element_id) {
    drupal_set_message(t('!title was set to @value', [
      '!title' => $form[$element_id]['#title'],
      '@value' => $form_state['values'][$element_id],
    ]));
  }
}
