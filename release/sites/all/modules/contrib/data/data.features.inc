<?php

/**
 * Implementation of hook_features_export_options. [component_hook]
 *
 * This hook will alert features of which specific items of this component may
 * be exported. For instances, in this case, we want to make available all the
 * existing items.  If there are no items to be exported, this component will
 * not be made available in the features export page.
 *
 * @return array
 *   A keyed array of items, suitable for use with a FormAPI select or
 *   checkboxes element.
 */
function data_table_schema_features_export_options() {
  $options = array();
  $tables = data_get_all_tables();

  foreach ($tables as $table) {
    $options[$table->get('name')] = $table->get('name');
  }

  return $options;
}

/**
 * Implementation of hook_features_export [component hook]
 *
 * This is a component hook, rather then a module hook, therefore this is the
 * callback from hook_features_api which relates to the specific component we
 * are looking to export. When a specific instance of the component we are
 * looking to export is selected, this will include the necessariy item, plus
 * any dependencies into our export array.
 *
 * @param array $data
 * this is the machine name for the component in question
 * @param array &$export
 * array of all components to be exported
 * @param string $module_name
 * The name of the feature module to be generated.
 * @return array
 * The pipe array of further processors that should be called
 */
function data_table_schema_features_export($data, &$export, $module_name) {
  $export['dependencies']['data'] = 'data';

  // The following is the simplest implementation of a straight object export
  // with no further export processors called.
  foreach ($data as $component) {
    $export['features']['data_table_schema'][$component] = $component;
  }
  return array();
}

/**
 * Implementation of hook_features_export_render. [component hook]
 *
 * This hook will be invoked in order to export
 * Component hook. The hook should be implemented using the name ot the
 * component, not the module, eg. [component]_features_export() rather than
 * [module]_features_export().
 *
 * Render one or more component objects to code.
 *
 * @param string $module_name
 *   The name of the feature module to be exported.
 * @param array $data
 *   An array of machine name identifiers for the objects to be rendered.
 * @param array $export
 *   The full export array of the current feature being exported. This is only
 *   passed when hook_features_export_render() is invoked for an actual feature
 *   update or recreate, not during state checks or other operations.
 * @return array
 *   An associative array of rendered PHP code where the key is the name of the
 *   hook that should wrap the PHP code. The hook should not include the name
 *   of the module, e.g. the key for `hook_example` should simply be `example`.
 */
function data_table_schema_features_export_render($module_name, $data, $export = NULL) {
  $code = array();
  $code[] = '  $data_table_schema = array();';
  $code[] = '';
  foreach ($data as $sys_name) {
    // Format it to the export.
    $code[] = data_export($sys_name, '  ') . '  $data_table_schema[] = $data_table;';
  }
  $code[] = '  return $data_table_schema;';
  $code = implode("\n", $code);

  return array('data_table_schema_features_default_tables' => $code);
}

/**
 * Implementation of hook_features_rebuild(). [component_hook]
 */
function data_table_schema_features_rebuild($module) {
  $items = module_invoke($module, 'data_table_schema_features_default_tables');

  // Loop over the items we need to recreate.
  foreach ($items as $item) {
    // Basic function to take the array and store in the database.
    $saved = data_create_table($item->name, $item->table_schema, $item->title);
  }
}

/**
 * Implementation of hook_features_revert(). [component_hook]
 */
function data_table_schema_features_revert($module) {
  data_table_schema_features_rebuild($module);
}
