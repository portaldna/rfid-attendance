<?php

/**
 * @file
 * Main module file for the flatpickr module.
 */

/**
 * Implements hook_element_info_alter().
 */
function flatpickr_element_info_alter(&$info) {
  if (libraries_info('flatpickr')) {
    if (isset($info['date_text'])) {
      $info['date_text']['#process'][] = 'flatpickr_element_date_text_process';
    }

    if (isset($info['textfield'])) {
      $info['textfield']['#process'][] = 'flatpickr_element_textfield_process';
    }
  }
}

/**
 * Form API #process callback.
 */
function flatpickr_element_date_text_process($element, &$form_state) {
  if (array_key_exists('#flatpickr', $element) && !$element['#flatpickr']) {
    return $element;
  }

  // PHP has some additional date format codes that flatpickr does not use. To
  // avoid needing a module/library like moment.js we attempt to convert the php
  // format codes to flatpickr format codes.
  $element['#date_format_original'] = $element['#date_format'];
  $element['#date_format'] = flatpickr_date_format_flatpickr_compatible($element['#date_format']);

  $element['date']['#date_increment'] = $element['#date_increment'];
  $element['date']['#date_year_range'] = $element['#date_year_range'];
  $element['date']['#flatpickr_date_format'] = $element['#date_format'];

  // If any flatpickr options were included on the element, pass them through.
  $element['date']['#flatpickr_options'] = !empty($element['#flatpickr_options']) ? $element['#flatpickr_options'] : [];

  return $element;
}

/**
 * Form API #process callback.
 */
function flatpickr_element_textfield_process($element, &$form_state) {
  if (empty($element['#flatpickr_date_format'])) {
    return $element;
  }

  $element['#attributes']['class'][] = 'flatpickr';
  $element['#flatpickr_options']['dateFormat'] = flatpickr_date_format_php_to_flatpickr($element['#flatpickr_date_format']);

  $granularity = date_format_order($element['#flatpickr_date_format']);

  $element['#flatpickr_options']['minuteIncrement'] = !empty($element['#date_increment']) ? $element['#date_increment'] : 1;
  $element['#flatpickr_options']['noCalendar'] = !date_has_date($granularity);
  $element['#flatpickr_options']['enableTime'] = date_has_time($granularity);
  $element['#flatpickr_options']['enableSeconds'] = in_array('second', $granularity);

  // Determine any min/max date restrictions.
  if (!empty($element['#date_year_range'])) {
    list($year_range['minDate'], $year_range['maxDate']) = explode(':', $element['#date_year_range']);

    foreach ($year_range as $setting => $value) {
      if (!empty($value) && ctype_digit($value)) {
        $value = (int) $value - (int) date('Y');
      }

      $element['#flatpickr_options'][$setting] = format_date(strtotime("midnight {$value} year", time()), 'custom', $element['#flatpickr_date_format']);
    }
  }

  $element['#flatpickr_options']['altInput'] = TRUE;
  $element['#flatpickr_options']['altFormat'] = $element['#flatpickr_options']['dateFormat'];

  $element['#flatpickr_options']['defaultDate'] = !empty($element['#default_value']) ? $element['#default_value'] : $element['#value'];

  // @todo turn into configuration.
  if ($plugins_enabled = variable_get('flatpickr_plugins', [])) {
    foreach ($plugins_enabled as $plugin_name => $plugin_config) {
      $element['#flatpickr_options']['plugins'][$plugin_name] = TRUE;
    }
  }

  $element['#description'] = t('Format: @date', [
    '@date' => date_format_date(date_example_date(), 'custom', $element['#flatpickr_date_format']),
  ]);

  _flatpickr_attach_field_js_settings($element);
  $element['#attached']['libraries_load'][] = ['flatpickr'];

  return $element;
}

/**
 * Implements hook_field_widget_info_alter().
 */
function flatpickr_field_widget_info_alter(&$info) {
  if (isset($info['date_text'])) {
    $info['date_text']['settings'] += [
      'flatpickr_enabled' => TRUE,
    ];
  }
}

/**
 * Implements hook_field_info_alter().
 */
function flatpickr_field_info_alter(&$info) {
  if (isset($info['timefield'])) {
    $info['timefield']['instance_settings'] += [
      'flatpickr' => [
        'flatpickr_enabled' => TRUE,
        // @todo make this configurable.
        'increment' => 1,
      ],
    ];
  }
}

/**
 * Implements hook_date_field_widget_settings_form_alter().
 */
function flatpickr_date_field_widget_settings_form_alter(&$form, $context) {
  if ($context['instance']['widget']['type'] === 'date_text') {
    $settings =& $context['instance']['widget']['settings'];

    $states = [
      'visible' => [
        'input[name="instance[widget][settings][flatpickr_enabled]"]' => ['checked' => TRUE],
      ],
    ];

    $form['flatpickr'] = [
      '#type' => 'fieldset',
      '#tree' => FALSE,
      '#title' => t('flatpickr datetimepicker'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
    ];

    $form['flatpickr_enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => !empty($settings['flatpickr_enabled']),
      '#fieldset' => 'widget_flatpickr',
      '#weight' => 0,
    ];

    // The PHP and flatpickr date format codes do not align directly. Attempt to
    // indicate to the user which formats can be converted and which will not
    // work.
    $now = date_example_date();

    foreach ($form['input_format']['#options'] as $format_php => $format_label) {
      if ($format_php == 'custom') {
        continue;
      }

      if (!flatpickr_can_convert_date_format_php_to_flatpickr($format_php)) {
        $form['input_format']['#options'][$format_php] = '(x) ' . $format_label;
      }
      else {
        $format_flatpickr_php = flatpickr_date_format_flatpickr_compatible($format_php);

        if ($format_php != $format_flatpickr_php) {
          $form['input_format']['#options'][$format_php] = '(*) ' . date_format_date($now, 'custom', $format_flatpickr_php);
        }
      }
    }

    $form['input_format']['#description'] .= '<br/>(x) Not compatible with flatpickr';
    $form['input_format']['#description'] .= '<br/>(*) Format modified to work with flatpickr';

    // Since we're altering the date_text instead or providing our own formatter
    // we want to make the year range and time increments available.
    $form['year_range'] = [
      '#type' => 'date_year_range',
      '#default_value' => $settings['year_range'],
      '#fieldset' => 'date_format',
      '#weight' => 6,
      '#states' => $states,
    ];
    $form['increment'] = [
      '#type' => 'select',
      '#title' => t('Time (minute) increments'),
      '#default_value' => $settings['increment'],
      '#options' => [
        1 => t('1 minute'),
        5 => t('5 minute'),
        10 => t('10 minute'),
        15 => t('15 minute'),
        30 => t('30 minute'),
      ],
      '#weight' => 7,
      '#fieldset' => 'date_format',
      '#states' => $states,
    ];
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function flatpickr_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id) {
  if ($form['#field']['type'] == 'timefield') {
    $field_name = $form['#field']['field_name'];
    $settings =& $form['#instance']['settings'];

    $form['instance']['settings']['flatpickr'] = [
      '#type' => 'fieldset',
      '#tree' => TRUE,
      '#title' => t('flatpickr datetimepicker'),
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#weight' => -1,
    ];

    $flatpickr_form =& $form['instance']['settings']['flatpickr'];

    $flatpickr_form['flatpickr_enabled'] = [
      '#type' => 'checkbox',
      '#title' => t('Enabled'),
      '#default_value' => isset($settings['flatpickr']['flatpickr_enabled']) ? $settings['flatpickr']['flatpickr_enabled'] : TRUE,
      '#weight' => 0,
    ];

    $formats = str_replace('i', 'i:s', array_keys(system_get_date_formats('short')));
    $formats = drupal_map_assoc($formats);
    $now = date_example_date();

    foreach ($formats as $f) {
      $options[$f] = date_format_date($now, 'custom', $f);
    }

    $flatpickr_form['increment'] = [
      '#type' => 'select',
      '#title' => t('Time (minutes) increments'),
      '#default_value' => !empty($settings['flatpickr']['increment']) ? $settings['flatpickr']['increment'] : 1,
      '#options' => [
        1 => t('1 minute'),
        5 => t('5 minute'),
        10 => t('10 minute'),
        15 => t('15 minute'),
        30 => t('30 minute'),
      ],
      '#weight' => 7,
    ];

    // Hide the jquery timepicker and settings.
    $form['instance']['settings']['disable_plugin']['#states'] = [
      'checked' => [
        'input[name="instance[settings][flatpickr][flatpickr_enabled]"]' => ['checked' => TRUE],
      ],
      'visible' => [
        'input[name="instance[settings][flatpickr][flatpickr_enabled]"]' => ['checked' => FALSE],
      ],
    ];

    $form['instance']['settings']['input_format']['#states'] = [
      'visible' => [
        'input[name="instance[settings][flatpickr][flatpickr_enabled]"]' => ['checked' => FALSE],
      ],
    ];

    // @todo is the timefield "default values" the actual year range
    // limits?  Using as such for the time being.
    $field_default_form =& $form['instance']['default_value_widget'][$field_name][LANGUAGE_NONE][0];

    foreach (['value', 'value2'] as $field_name) {
      if (isset($field_default_form[$field_name])) {
        unset($field_default_form[$field_name]['#attributes']['class'][0]);
        $field_default_form[$field_name]['#description'] = '';
        $field_default_form[$field_name]['#attributes']['class'][] = 'flatpickr';
        $field_default_form[$field_name]['#date_format'] = 'h:iA';
        $field_default_form[$field_name]['#flatpickr_date_format'] = flatpickr_date_format_flatpickr_compatible($field_default_form[$field_name]['#date_format']);
      }
    }

    unset($field_default_form['#attached']);
  }
}

/**
 * Implements hook_field_widget_form_alter().
 *
 * @todo do we need to alter the value prior to saving given the date formatting
 * issues?
 * @todo do we need any additional validation?
 */
function flatpickr_field_widget_form_alter(&$element, &$form_state, $context) {
  if ($context['field']['type'] == 'timefield') {
    $flatpickr_settings = $context['instance']['settings']['flatpickr'];

    if ($flatpickr_settings['flatpickr_enabled']) {
      foreach (['value', 'value2'] as $field_name) {
        if (isset($element[$field_name])) {
          unset($element[$field_name]['#attributes']['class'][0]);
          // @todo make configurable.
          $element[$field_name]['#flatpickr_date_format'] = flatpickr_date_format_flatpickr_compatible('h:iA');
        }
      }
    }
  }
}

/**
 * Helper function to attach flatpickr js settings to element.
 *
 * @param array $element
 *   The element/field to attach settings for.
 */
function _flatpickr_attach_field_js_settings(array &$element) {
  $js_settings = [];
  $prefix = '#flatpickr_';
  foreach (array_keys($element) as $key) {
    if (strpos($key, $prefix) === 0) {
      $name = str_replace($prefix, '', $key);
      $js_settings[$name] = $element[$key];
    }
  }

  if ($js_settings) {
    $element['#attached']['js'][] = [
      'type' => 'setting',
      'data' => [
        'flatpickr' => [
          'instances' => [
            $element['#id'] => $js_settings,
          ],
        ],
      ],
    ];
  }
}

/**
 * Implements hook_libraries_info().
 */
function flatpickr_libraries_info() {
  $libraries = [];

  $libraries['flatpickr'] = [
    'name' => 'flatpickr JavaScript Library',
    'vendor url' => 'https://chmln.github.io/flatpickr/',
    'download url' => 'https://github.com/chmln/flatpickr/releases',
    'version callback' => function_exists('libraries_get_package_json_version') ? 'libraries_get_package_json_version' : '_flatpickr_libraries_get_package_json_version',
    'files' => [
      'js' => [
        'dist/flatpickr.min.js',
        'dist/plugins/confirmDate/confirmDate.js',
      ],
      'css' => [
        'dist/flatpickr.min.css',
        'dist/plugins/confirmDate/confirmDate.css',
      ],
    ],
    'integration files' => [
      'flatpickr' => [
        'js' => ['js/flatpickr.js'],
      ],
    ],
  ];

  return $libraries;
}

/**
 * Gets the version information from a library's package.json file.
 *
 * The latest development version of the Libraries module includes a version
 * callback function to retrieve the library version from a package.json file.
 * This function simply mimics that logic until the function is committed to a
 * stable release and sites slowly upgrade to that version.
 *
 * @param array $library
 *   An associative array containing all information about the library.
 * @param array $options
 *   This callback expects no option.
 *
 * @return string
 *   A string containing the version of the library.
 *
 * @see libraries_get_path()
 * @see libraries_get_package_json_version()
 * @see https://www.drupal.org/node/2699799
 */
function _flatpickr_libraries_get_package_json_version(array $library, array $options) {
  $file = DRUPAL_ROOT . '/' . $library['library path'] . '/package.json';
  if (!file_exists($file)) {
    return '';
  }

  $content = file_get_contents($file);
  if (!$content) {
    return '';
  }

  $data = drupal_json_decode($content);
  if (isset($data['version'])) {
    return $data['version'];
  }
}

/**
 * Convert php date format codes to flatpickr format codes.
 *
 * The flatpickr library uses a specific set of date format codes that vary from
 * php date format codes. To avoid requiring the help of a module / library like
 * moment.js we attempt to map the php codes to flatpickr codes.
 *
 * @param string $format_php
 *   The data format using php format codes.
 *
 * @return string
 *   The date format using flatpickr format codes.
 *
 * @see https://chmln.github.io/flatpickr/#formatChars
 */
function flatpickr_date_format_php_to_flatpickr($format_php) {
  $format_map = _flatpickr_get_date_format_map();
  return str_replace(array_keys($format_map), array_values($format_map), $format_php);
}

/**
 * Convert flatpickr date format codes to php format codes.
 *
 * The flatpickr library uses a specific set of date format codes that vary from
 * php date format codes. To avoid requiring the help of a module / library like
 * moment.js we attempt to map the php codes to flatpickr codes.
 *
 * @param string $format_flatpickr
 *   The data format using flatpickr format codes.
 *
 * @return string
 *   The date format using php format codes.
 *
 * @see https://chmln.github.io/flatpickr/#formatChars
 */
function flatpickr_date_format_flatpickr_to_php($format_flatpickr, $original_format_php = '') {
  $format_map = _flatpickr_get_date_format_map();

  // If an original php format was provided we want to make sure to only use
  // those mappings. In some cases we have multiple php format codes mapping to
  // a single flatpickr format code. Attempt to format back to the original php
  // code.
  if ($original_format_php) {
    foreach (array_keys($format_map) as $format_code_php) {
      if (strpos($original_format_php, $format_code_php) === FALSE) {
        unset($format_map[$format_code_php]);
      }
    }
  }

  $format_map = array_flip($format_map);
  return str_replace(array_keys($format_map), array_values($format_map), $format_flatpickr);
}

/**
 * Convert php date format into flatpickr compatible php date format.
 *
 * @param string $format_php
 *   The data format using php format codes.
 *
 * @return string
 *   The flatpickr compatible php date format.
 *
 * @see https://chmln.github.io/flatpickr/#formatChars
 */
function flatpickr_date_format_flatpickr_compatible($format_php) {
  $format_flatpickr = flatpickr_date_format_php_to_flatpickr($format_php);
  $format_flatpickr_php = flatpickr_date_format_flatpickr_to_php($format_flatpickr);
  return $format_flatpickr_php;
}

/**
 * Can php date format be converted to flatpickr date format.
 *
 * @param string $format_php
 *   The data format using php format codes.
 *
 * @return bool
 *   TRUE if a conversion can be performed, FALSE otherwise.
 *
 * @see https://chmln.github.io/flatpickr/#formatChars
 */
function flatpickr_can_convert_date_format_php_to_flatpickr($format_php) {
  $format_map = _flatpickr_get_date_format_map();

  if ($format_php == 'site-wide') {
    $format_php = variable_get('date_format_short', '');
  }

  $test_format = str_replace(array_keys($format_map), '', $format_php);
  $test_format = preg_replace("/[^A-Za-z]/", '', $test_format);

  return strlen($test_format) === 0;
}

/**
 * Helper function: Get the php date format to flatpickr date format map.
 */
function _flatpickr_get_date_format_map() {
  // PHP => flatpickr.
  $format_map = [
    'jS' => 'J',
    'd' => 'd',
    'D' => 'D',
    'l' => 'l',
    'j' => 'j',
    'w' => 'w',
    'F' => 'F',
    'm' => 'm',
    'n' => 'n',
    'M' => 'M',
    'U' => 'U',
    'y' => 'y',
    'Y' => 'Y',
    'G' => 'H',
    'H' => 'H',
    'h' => 'h',
    'g' => 'h',
    'i' => 'i',
    's' => 'S',
    'a' => 'K',
    'A' => 'K',
  ];
  return $format_map;
}
