<?php

/**
 * @file
 * Admin page callbacks for DNA Datatables module.
 */

/**
 * Form: Basic Configuration.
 */
function dna_datatables_configuration_form($form, &$form_state) {
  $component_class = 'dna-datatables-configuration-form';
  $form['#component_class'] = $component_class;
  $form['#prefix'] = '<div id="' . $component_class . '-wrapper">';
  $form['#suffix'] = '</div>';
  $form['#attributes']['class'][] = $component_class;

  $form['submit_clear_cache'] = [
    '#value' => t('Rebuild Datatables Info Caches'),
    '#type' => 'submit',
    '#submit' => ['dna_datatables_configuration_form_clear_cache_submit'],
    '#ajax' => [
      'callback' => 'dna_datatables_configuration_form_ajax_callback',
      'wrapper' => $component_class . '-wrapper',
    ],
  ];

  // @todo change when there is configuration to alter.
  // return system_settings_form($form);
  return $form;
}

/**
 * Submit handler: Config form clear cache.
 */
function dna_datatables_configuration_form_clear_cache_submit($form, &$form_state) {
  $info = dna_datatables_table_info(NULL, TRUE);
  $info = dna_datatables_page_listing_info(NULL, TRUE);
  drupal_set_message('DNA Datatables caches have been cleared / rebuild.');
}

/**
 * AJAX handler: Config form clear cache.
 */
function dna_datatables_configuration_form_ajax_callback($form, &$form_state) {
  return $form;
}
