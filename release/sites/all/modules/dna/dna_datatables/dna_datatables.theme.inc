<?php

/**
 * @file
 * Theme functions for DNA Datatables.
 */

/**
 * Theme: Datatable.
 *
 * @see theme_table()
 */
function theme_dna_datatable($variables) {
  $component_class = 'dna-datatable';
  $dt_id = $variables['dt_id'];
  $attributes = &$variables['attributes'];

  if (empty($attributes['id'])) {
    $attributes['id'] = uniqid("{$component_class}--");
  }

  $attributes['class'][] = $component_class;
  $attributes['width'] = '100%';
  $attributes['cellspacing'] = 0;

  $table_info = dna_datatables_table_info($dt_id);

  // @todo work-around for bootstrap responsive tables.
  $variables['context']['responsive'] = 0;

  $output = '<div class="' . $component_class . '-wrapper">';

  // @todo allow for filters that do not render elements.
  if (!empty($table_info['filters']) || !empty($table_info['actions'])) {
    $output .= '<div class="' . $component_class . '-header">';

    if (!empty($table_info['meta']['label'])) {
      $output .= '<h3 class="' . $component_class . '__title">' . $table_info['meta']['label'] . '</h3>';
    }

    foreach (['filter', 'action'] as $section_type) {
      $section = "{$section_type}s";

      if (!empty($table_info[$section])) {
        $output .= '<div class="' . "{$component_class}-{$section}" . '" dna-datatable-id="' . $attributes['id'] . '">';

        foreach ($table_info[$section] as $item_id => $item) {
          if (!empty($item['element'])) {
            $element = $item['element'];

            if (empty($element['#name'])) {
              $element['#name'] = "{$dt_id}__{$item_id}";
            }
            // @todo rendering form element here does not go through the normal
            // hooks so dna_select2 is not picking it up. Manually calling
            // helper function.
            _dna_select2_prepare_element($element);
            $element['#attributes']['dna-datatable-id'] = $attributes['id'];
            $element['#attributes']["dna-datatable-{$section_type}-id"] = $item_id;

            if (empty($element['#attributes']['id'])) {
              $element['#attributes']['id'] = "dna-datatable__{$section_type}--" . drupal_clean_css_identifier($item_id);
            }

            $output .= render($element);
            $table_info[$section][$item_id]['element'] = "#{$element['#attributes']['id']}";
          }
        }

        $output .= '</div>';
        $output .= '<div class="' . "{$component_class}-{$section}__overlay" . '"></div>';
      }
    }

    $output .= '</div>';
  }

  $output .= theme('table', $variables);
  $output .= '</div>';

  drupal_add_js([
    'dna_datatables' => [
      'tables' => [
        $attributes['id'] => $table_info,
      ],
    ],
  ], 'setting');

  return $output;
}
