/**
 * @file
 * Javascript file for DNA Datatables.
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.theme.dna_datatables_themeFunctionExists = function (name) {
    return Drupal.theme.prototype.hasOwnProperty(name) || Drupal.theme.hasOwnProperty(name);
  }

  // @todo there may be a collision of hook suggestions with multiple tables on
  // the same page.
  Drupal.theme.dna_datatables_getThemeHookSuggestions = function (tableID, column) {
    var tableInfo = Drupal.settings.dna_datatables.tables[tableID];
    var renderSuggestions = tableInfo.meta.renderSuggestions[column] || [];
    var suggestions = [];

    Object.keys(renderSuggestions).forEach(function (key) {
      suggestions.push(renderSuggestions[key]);
    });

    return suggestions;
  }

  /**
   * Theme: Item Link.
   */
  Drupal.theme.prototype.actionLink = function(item, linkId, dtType, label) {
    dtType = dtType || 'display';
    var linkInfo = Drupal.dna_jsonapi.getLinkInfo(item, linkId, label);

    if (dtType == 'display') {
      if (linkInfo.path.length) {
        return `<a href="${linkInfo.path}">${linkInfo.label}</a>`;
      }
    }

    return linkInfo.label;
  }

  /**
   * Theme: Default Column Render.
   */
  Drupal.theme.prototype.dna_datatables_colRender__default = function(tableID, column, data, type, rowData, meta) {
    return data;
  }

  /**
   * Theme: Timestamp Column Render.
   */
  Drupal.theme.prototype.dna_datatables_colRender__timestamp = function(tableID, column, data, type, rowData, meta) {
    if (!['display', 'export'].includes(type)) {
      return data;
    }

    var ts = data * 1000;
    var ts = new Date(ts);
    return (type == 'display') ? ts.toLocaleString() : ts.toISOString();
  }

  /**
   * Theme: Relationship Column Render.
   */
  Drupal.theme.prototype.dna_datatables_colRender__relationship = function(tableID, column, data, type, rowData, meta) {
    if (data.data === null) {
      return rowData.attributes.hasOwnProperty(column) ? rowData.attributes[column] : '';
    }

    if (Array.isArray(data.data)) {
      return data.data.reduce(function (output, item) {
        return output.concat((output.length) ? ', ' : '', Drupal.theme('actionLink', item, 'view', type));
      }, '');
    }

    return Drupal.theme('actionLink', data.data, 'view', type);
  }

})(jQuery, Drupal, this, this.document);
