/**
 * @file
 * Javascript file for DNA Datatables.
 *
 * @todo can a class / prototype / etc. work better here to wrap logic together?
 * @todo break into separate js files.
 * @todo if column has no data then hide column.
 * @todo make into class/object for better code control? Allow for caching
 * instances?
 */

var DNADatatables = DNADatatables || {};

DNADatatables.buttons = function (func) {
  var args = Array.prototype.slice.apply(arguments, [1]);
  return (DNADatatables.buttons[func] || DNADatatables.buttons.prototype[func]).apply(this, args);
};

DNADatatables.buttons.prototype = {
  // @todo need a way for user to reset lastReset flag.
  reload: function (e, dt, $btn, config) {
    dt.ajax.reload(null, false);
  },

  autoReload: function (e, dt, $btn, config) {
    var intervalID = $btn.attr('dna-datatables-interval-id');

    if (intervalID) {
      clearInterval(intervalID);
      dt.buttons('.dna-datatables__button--autoReload').text('Enable Auto Reload');
    }
    else {
      intervalID = setInterval(function () {
        dt.ajax.reload(null, false);
      }, 15000);
      $btn.attr('dna-datatables-interval-id', intervalID);
      dt.buttons('.dna-datatables__button--autoReload').text('Disable Auto Reload');
    }
  },

  filters: function (e, dt, $btn, config) {
    $btn
      .closest('.dna-datatable-wrapper')
      .find('.dna-datatable-filters')
      .toggleClass('dna-datatable-filters--visible');
  }
};

// @see https://coderwall.com/p/tyy7lw/function-caching-in-javascript-caching-catching
// @todo move to dna_utilities.
DNADatatables.filterCacher = function (key, reset, func) {
  var args = Array.prototype.slice.apply(arguments, [3]);

  if (reset || !DNADatatables.filterCacher.cache[key]) {
    DNADatatables.filterCacher.cache[key] = func.apply(null, args);
  }

  return DNADatatables.filterCacher.cache[key];
}

DNADatatables.filterCacher.cache = {};

DNADatatables.filters = function (func) {
  var args = Array.prototype.slice.apply(arguments, [1]);
  return (DNADatatables.filters[func] || DNADatatables.filters.prototype[func]).apply(this, args);
}

DNADatatables.filters.prototype = {
  range: function (item, filterInfo) {
    var value = parseInt(Object.getByPath(item, filterInfo.valuePath));
    var range = filterInfo.criteria.split(':');
    var min = parseInt(range[0]);
    var max = parseInt(range[1]);

    if (isNaN(value)) {
      return false;
    }

    if (!isNaN(min) && !isNaN(max)) {
      if (value < min || value > max) {
        return false;
      }
    }

    return true;
  }
};


(function ($, Drupal, window, document) {

  'use strict';

  Drupal.dna_datatables = {
    // @todo was this here instead of DNADatatables to allow for access to
    // Drupal, $, etc. ?
    ajaxFunctions: function (func) {
      var args = Array.prototype.slice.apply(arguments, [1]);
      return Drupal.dna_datatables.ajaxFunctions[func].apply(this, args);
    },

    // @todo add to dt saveState?
    // @todo make user of DNADatatables.filterCacher?
    cacheSearchVars: function (tableID, reset) {
      var cacheID = 'dna_datatables__searchVars--' + tableID;
      var searchVars = sessionStorage.getItem(cacheID);
      var filterInfo = Drupal.settings.dna_datatables.tables[tableID].filters || {};

      reset = reset || false;

      if (reset || !searchVars) {
        searchVars = {};

        Object.keys(filterInfo).forEach(function (filterID) {
          searchVars[filterID] = filterInfo[filterID];
        });

        $('[dna-datatable-filter-id][dna-datatable-id="' + tableID + '"]')
          .each(function (i) {
            var filterID = $(this).attr('dna-datatable-filter-id');

            if (filterInfo.hasOwnProperty(filterID)) {
              searchVars[filterID].criteria = $(this).val();
            }
          });

        sessionStorage.setItem(cacheID, JSON.stringify(searchVars));
      }
      else {
        searchVars = JSON.parse(searchVars);
      }

      return searchVars;
    },

    initSearchVars: function (tableID) {
      $('[dna-datatable-filter-id][dna-datatable-id="' + tableID + '"]')
        .once('dna-datatable-search-init')
        // @todo may need to trigger on other events depending on filter
        // element.
        .on('change', function () {
          $('#' + tableID)
            .closest('.dna-datatable-wrapper')
            .find('.dna-datatable-filters')
            .removeClass('dna-datatable-filters--visible');
          $('#' + tableID).DataTable().draw();
        });
    },

    searchDone: function (tableID) {
      var $table = $('#' + tableID);
      var dt = $table.DataTable();
      var $filterBtn = $table
        .closest('.dna-datatable-wrapper')
        .find('.dna-datatable__button--filters');

      if (dt.rows().count() != dt.rows({search: 'applied'}).count()) {
        $filterBtn.addClass('dna-datatable__button--filters-applied');
      }
      else {
        $filterBtn.removeClass('dna-datatable__button--filters-applied');
      }
    }
  };

  function dna_datatables_buttonActionExists(name) {
    return DNADatatables.buttons.prototype.hasOwnProperty(name) || DNADatatables.buttons.hasOwnProperty(name);
  }

  function dna_datatables_buttonActions(buttons) {
    $.each(buttons, function (i, button) {
      if (button.hasOwnProperty('buttons')) {
        dna_datatables_buttonActions(button.buttons);
      }
      else if (button.hasOwnProperty('name') && dna_datatables_buttonActionExists(button.name)) {
        button.action = function (e, dt, $btn, config) {
          DNADatatables.buttons(button.name, e, dt, $btn, config);
        }
      }
    });
  }

  function dna_datatables_pager_showhide($table) {
    // @see https://github.com/DataTables/Plugins/blob/master/features/conditionalPaging/dataTables.conditionalPaging.js
    var dt = $table.DataTable();
    var $pager = $(dt.table().container()).find('.dataTables_paginate');
    var numPages = dt.page.info().pages;
    var visibility = (numPages > 1) ? '' : 'hidden';
    // @todo Do we want this? Can we make this easy to override?
    // $pager.css('visibility', visibility);
  }

  // @todo not working.
  function dna_datatables_pageLengthResize($table) {
    return;
    var dt = $table.DataTable();
    var offsetTop = $table.offset().top;
    var rows = $( 'tr', dt.table().body() );
    var rowHeight = rows.eq( rows.length > 1 ? 1 : 0  ).height();
    var parent = $(dt.table().container()).parent();
    var availableHeight = parent.height();
    var scrolling = dt.table().header().parentNode !== dt.table().body().parentNode;

    if ( parent.css('position') === 'static'  ) {
      parent.css( 'position', 'relative'  );
    }

    if (!scrolling) {
      if (dt.table().header()) {
        availableHeight -= $(dt.table().header()).height();
      }
      if (dt.table().footer()) {
        availableHeight -= $(dt.table().footer()).height();
      }
    }

    availableHeight -= offsetTop;
    availableHeight -= $(dt.table().container()).height() - ( offsetTop + $table.height() );

    var drawRows = Math.floor( availableHeight / rowHeight  );

    if ( drawRows !== Infinity && drawRows !== -Infinity && !isNaN( drawRows ) && drawRows > 0 && drawRows !== dt.page.len()) {
      // @todo make configurable.
      // dt.page.len( drawRows ).draw();
    }
  }

  // @todo move to separate functions and allow overriding / subscribing to
  // plugin?
  function dna_datatables_registerPlugins() {
    // @see https://datatables.net/plug-ins/api/processing()
    // @see https://github.com/DataTables/Plugins/blob/master/api/processing().js
    $.fn.dataTable.Api.register('processing()', function (show) {
      return this.iterator('table', function (ctx) {
        ctx.oApi._fnProcessingDisplay(ctx, show);
      })
    });

    // @see https://github.com/DataTables/Plugins/blob/master/api/row().show().js
    $.fn.dataTable.Api.register('row().show()', function() {
      var page_info = this.table().page.info();
      var new_row_index = this.index();
      var row_position = this.table().rows()[0].indexOf( new_row_index );
      if( row_position >= page_info.start && row_position < page_info.end ) {
        return this;
      }
      var page_to_display = Math.floor( row_position / this.table().page.len() );
      this.table().page( page_to_display );
      return this;
    });

    // @see https://datatables.net/extensions/buttons/examples/initialisation/plugins
    $.fn.dataTable.ext.buttons.actionLink = {
      className: 'dna-datatables__button--action-link',
      action: function (e, dt, $btn, config) {
        window.location.href = $btn.attr('href');
      }
    };

    $.fn.dataTable.ext.search.push(
      // if (settings.nTable.id === 'tabledata') {}
      function (settings, rowSearchData, rowIndex, rowRawData, delta) {
        var tableID = settings.sTableId;
        var cacheID = 'dna_datatables__searchVars--' + tableID;
        // @todo this resets on the first delta... do we want/need that?
        var searchVars = Drupal.dna_datatables.cacheSearchVars(tableID, !delta);
        var pass = true;

        Object.keys(searchVars).forEach(function (filterID) {
          pass = DNADatatables.filters(searchVars[filterID].type, rowRawData, searchVars[filterID], delta);

          if (!pass) {
            return pass;
          }
        });

        return pass;
      }
    );
  }

  // @todo do we want these as 'extend'?
  $.fn.extend({
    dtReload: function () {
      $(this).DataTable().ajax.reload(null, false);
    },
    // Add data to the table and redraw.
    dtAddData: function (data) {
      if (!this.length) {
        return;
      }

      if (!Array.isArray(data)) {
        data = [data];
      }

      var $table = $(this);
      var table = $table.DataTable();
      var rowNodes = table.rows.add(data).draw().nodes();
      // @todo make more generic so theme can style.
      $(rowNodes).css('opacity', .3).animate({ opacity: 1 }, 1000);
      Drupal.attachBehaviors($(rowNodes));
    },
    // Update data in the table and redraw.
    // @todo can row selector be derived from each data item?
    dtUpdateData: function (data, rowSelector) {
      if (!this.length) {
        return;
      }

      var $table = $(this);
      var table = $table.DataTable();
      var rowSelector = rowSelector || '';
      var rowNode;

      if (!Array.isArray(data)) {
        data = [data];
      }

      data.forEach(function (item) {
        var thisRowSelector = (!rowSelector.length) ? '#' + item._id : rowSelector;

        if (table.row(thisRowSelector).any()) {
          rowNode = table.row(thisRowSelector).data(item).invalidate().node();
          // @todo make more generic so theme can style.
          $(rowNode).css('opacity', .3).animate({ opacity: 1 }, 1000);
          Drupal.attachBehaviors($(rowNode));
        }
      });
    },
    dtUpsert: function (data) {
      if (!this.length) {
        return;
      }

      var $table = $(this);
      var table = $table.DataTable();

      if (!Array.isArray(data)) {
        data = [data];
      }

      data.forEach(function (item) {
        // @todo check dt settings for DT_RowId?
        var rowSelector = '#' + item._id;
        var isNew = true;

        try {
          if (table.rows(rowSelector).any()) {
            isNew = false;
          }
        }
        catch (err) {
          // Only using the catch since checking if a row exists by the row ID
          // seems to throw an exception if it is not found.
        }

        if (isNew) {
          $table.dtAddData(item);
        }
        else {
          $table.dtUpdateData(item, rowSelector);
        }
      })
    }
  });

  Drupal.behaviors.dna_datatables_init = {
    attach: function (context, settings) {

      $('.dna-datatable')
        .once('dna-datatables-init')
        .each(function (i) {
          var $table = $(this);
          var tableID = $table.attr('id');
          var tableSettings = Drupal.settings.dna_datatables.tables[tableID].config;

          tableSettings.columns.forEach(function(column) {
            var themeHookSuggestions = [];

            column.render = function (data, type, rowData, meta) {
              if (data === undefined) {
                return '';
              }

              var output = data;

              if (themeHookSuggestions = Drupal.theme.dna_datatables_getThemeHookSuggestions(tableID, column.name)) {
                themeHookSuggestions.some(function (suggestion) {
                  if (Drupal.theme.dna_datatables_themeFunctionExists(suggestion)) {
                    output = Drupal.theme(suggestion, tableID, column.name, data, type, rowData, meta);
                    return true;
                  }
                });
              }

              return output;
            }
          });

          if (tableSettings.hasOwnProperty('buttons')) {
            dna_datatables_buttonActions(tableSettings.buttons);
          }

          if (tableSettings.ajax.hasOwnProperty('function')) {
            var ajaxFunction = tableSettings.ajax.function;
            var params = tableSettings.ajax.params || {};

            if (Drupal.dna_datatables.ajaxFunctions.hasOwnProperty(ajaxFunction)) {
              tableSettings.ajax = function (data, callback, settings) {
                Drupal.dna_datatables.ajaxFunctions(ajaxFunction, tableID, params)
                  .then(function (res) {
                    callback(res);
                  })
                  .catch (function (e) {
                    console.warn(e);
                    console.warn(`Unable to load data for table ${settings.sTableId} with params: `, params);
                    callback({ data: [] });
                  });
              };
            }
          }

          // Note: this was moved out of the preInit.dt event because the button
          // defintion was required before initializing the datatable.
          dna_datatables_registerPlugins();

          var dt = $table
            .on('preInit.dt', function (e, settings) {
            })
            .on('init.dt', function (e, settings) {
              Drupal.dna_datatables.initSearchVars(tableID);
              dna_datatables_pageLengthResize($(this));
            })
            .on('draw.dt', function (e, settings) {
              Drupal.attachBehaviors($table);
              dna_datatables_pager_showhide($(this));
            })
            .on('search.dt', function (e, settings) {
              Drupal.dna_datatables.searchDone(tableID);
            })
            .DataTable(tableSettings);
        });

      $('.dna-datatable-filters__overlay')
        .once('dna-datatable-init')
        .on('click', function () {
          $(this)
            .closest('.dna-datatable-header')
            .find('.dna-datatable-filters--visible')
            .removeClass('dna-datatable-filters--visible');
        });

    }
  };

})(jQuery, Drupal, this, this.document);
