<?php

/**
 * @file
 * Code for the DNA Admin feature.
 *
 * @todo the module dependencies make this difficult to be a drop-in module on
 * existing sites without including other dna and contrib modules.
 */

include_once 'dna_admin.features.inc';

/**
 * Implements hook_menu().
 */
function dna_admin_menu() {
  $items = [];

  $items['admin/config/dna'] = [
    'title' => 'DNA',
    'description' => 'Configuration options for the DNA Suite of modules.',
    'position' => 'left',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => ['access administration pages'],
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'management',
  ];

  return $items;
}

/**
 * Implements hook_mail().
 */
function dna_admin_mail($key, &$message, $params) {
  if ($key == 'watchdog') {
    $message['subject'] = $params['subject'];
    $message['body'] = $params['body'];
  }
}

/**
 * Implements hook_watchdog().
 *
 * @todo make configurable.
 * @todo move to dna_watchdog.
 */
function dna_admin_watchdog(array $log_entry) {
  $send_email = FALSE;

  // Based on the severity of the watchdog log, generate an email notification
  // that an issue has occurred.
  $severities = variable_get('dna_admin_watchdog_severities', [
    WATCHDOG_EMERGENCY,
    WATCHDOG_ALERT,
    WATCHDOG_CRITICAL,
  ]);
  if (array_key_exists($log_entry['severity'], $severities)) {
    $send_email = TRUE;
  }

  // Also send an email for php errors.
  if ($log_entry['type'] == 'php' && $log_entry['severity'] == WATCHDOG_ERROR) {
    $send_email = TRUE;
  }

  if (!$send_email) {
    return;
  }

  $subject = t('@site_name: A !type watchdog log has been created.', [
    '@site_name' => variable_get('site_name', t('DNA Admin')),
    '!type' => $log_entry['type'],
  ]);

  $type = t('Type: !type', array('!type' => $log_entry['type']));
  $link = !empty($log_entry['link']) ? t('Link: !link', array('!link' => $log_entry['link'])) : t('Link: None');
  $uid = t('UID: !uid', array('!uid' => $log_entry['uid']));
  $request_uri = t('Request URI: !request_uri', array('!request_uri' => $log_entry['request_uri']));
  $referer = t('Referrer: !referrer', array('!referrer' => $log_entry['referer']));
  $created = t('Created: !created', array('!created' => format_date($log_entry['timestamp'], 'short')));

  $body = array(
    'type' => $type,
    'link' => $link,
    'uid' => $uid,
    'request_uri' => $request_uri,
    'referrer' => $referer,
    'created' => $created,
    'message' => format_string($log_entry['message'], $log_entry['variables']),
  );

  $params = array(
    'subject' => $subject,
    'body' => $body,
  );

  if ($recipients = variable_get('dna_admin_watchdog_recipients', [
    'craig.aschbrenner@portaldna.com',
  ])) {
    drupal_mail('dna_admin', 'watchdog', implode(',', $recipients), language_default(), $params);
  }
}
