<?php

/**
 * @file
 * Install, update and uninstall functions for DNA Security.
 */

/**
 * Implements hook_schema().
 */
function dna_security_schema() {
  $schema['dna_security_ip_ban'] = [
    'description' => 'Logs potential IP threats.',
    'fields' => [
      'id' => [
        'description' => 'Unique ID.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'ip' => [
        'description' => 'IP address of logged in users.',
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
      ],
      'rule_id' => [
        'description' => 'The unique rule identifier that triggered the log entry.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => '',
      ],
      'message' => [
        'description' => 'The message associated with the log entry.',
        'type' => 'varchar',
        'length' => 2048,
        'not null' => TRUE,
        'default' => '',
      ],
      'uid' => [
        'description' => 'The {users}.uid, if any, associated with the log entry.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
      'action' => [
        'description' => 'The action taken on the threat.',
        'type' => 'varchar',
        'length' => 40,
        'not null' => TRUE,
        'default' => '',
      ],
      'created' => [
        'description' => 'The unix timestamp when the entry was created.',
        'type' => 'int',
        'not null' => TRUE,
      ],
    ],
    'indexes' => [
      'ip' => ['ip'],
      'rule_id' => ['rule_id'],
      'rule_ip_count' => ['ip', 'rule_id', 'created'],
      'created' => ['created'],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}
