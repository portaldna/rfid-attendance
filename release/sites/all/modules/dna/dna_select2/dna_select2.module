<?php

/**
 * @file
 * Main file for DNA Select2 module.
 */

/**
 * Implements hook_field_widget_WIDGET_TYPE_form_alter().
 *
 * This hook captures options select fields added to entities.
 */
function dna_select2_field_widget_options_select_form_alter(&$element, &$form_state, $context) {
  _dna_select2_prepare_element($element);
}

/**
 * Implements hook_element_info_alter().
 *
 * This hook captures select fields added through the Form API.
 */
function dna_select2_element_info_alter(&$type) {
  $type['select']['#after_build'][] = '_dna_select2_element_after_build';
}

/**
 * Implements hook_dna_core_libraries().
 */
function dna_select2_dna_core_libraries() {
  return [
    'select2' => [
      'url' => 'https://github.com/select2/select2',
      'version' => '4.0.0',
      'info' => [
        'path' => 'dist',
        'version arguments' => [
          'file' => 'dist/js/select2.js',
          'pattern' => '@Select2\s+([0-9\.-]+)@',
          'lines' => 5,
        ],
        'files' => [
          'js' => [
            'js/select2.min.js',
          ],
          'css' => [
            'css/select2.min.css',
          ],
        ],
        'integration files' => [
          'dna_select2' => [
            'js' => [
              'js/dna_select2.js',
            ],
          ],
        ],
      ],
    ],
  ];
}

/**
 * Generate select2 settings.
 */
function _dna_select2_select2_settings($element) {
  $id = _dna_select2_select2_id($element);
  $settings = [
    'debug' => FALSE,
    'dropdownParent' => '.form-type-select',
  ];

  if ($id) {
    drupal_alter(['dna_select2_instance_settings', "dna_select2_instance_{$id}_settings"], $settings);
  }

  return $settings;
}

/**
 * Global select2 settings.
 */
function _dna_select2_select2_global_settings() {
  $settings = [
    'debug' => FALSE,
    'width' => '100%',
    'minimumResultsForSearch' => 10,
  ];
  drupal_alter('dna_select2_global_settings', $settings);
  return $settings;
}

/**
 * Field element #after_build.
 *
 * @see dna_select2_element_info_alter()
 */
function _dna_select2_element_after_build($element, $form_state) {
  _dna_select2_prepare_element($element);
  return $element;
}

/**
 * Prepare element for select2.
 */
function _dna_select2_prepare_element(&$element) {
  $id = _dna_select2_select2_id($element);

  if (!$id) {
    return;
  }

  if (path_is_admin(current_path()) && !variable_get('dna_select2_admin_paths_enabled', FALSE)) {
    return;
  }

  $element['#attributes']['dna-select2-id'] = $id;

  $element['#attached']['js'][] = [
    'data' => [
      'dna_select2' => [
        'settings' => [
          '_global' => _dna_select2_select2_global_settings(),
          $id => _dna_select2_select2_settings($element),
        ],
      ],
    ],
    'type' => 'setting',
  ];
  $element['#attached']['libraries_load'][] = ['select2'];
}

/**
 * Get element select2 id.
 */
function _dna_select2_select2_id($element) {
  foreach (['#field_name', '#name', '#id'] as $prop) {
    if (!empty($element[$prop])) {
      return $element[$prop];
    }
  }
  return FALSE;
}
