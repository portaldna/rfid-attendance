/**
 * @file
 * Javascript file for DNA Select2.
 */

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.behaviors.dna_select2_init = {
    attach: function (context, settings) {

      Object.keys(Drupal.settings.dna_select2.settings._global).forEach(function (settingID) {
        var value = Drupal.settings.dna_select2.settings._global[settingID];
        $.fn.select2.defaults.set(settingID, value);
      });

      var select2s = $('[dna-select2-id]');

      select2s
        .once('dna-select2-init')
        .each(function (i) {
          var id = $(this).attr('dna-select2-id');
          var settings = Drupal.settings.dna_select2.settings.hasOwnProperty(id) ? Drupal.settings.dna_select2.settings[id] : {};

          if (settings.hasOwnProperty('dropdownParent')) {
            settings.dropdownParent = $(this).parent(settings.dropdownParent);
          }

          // @todo do we want to do this for non-required fields?
          if (false && $(this)[0].hasAttribute('placeholder')) {
            var option = $(this).is(':selected') ? '<option value="_placeholder"></option>' : '<option value="_placeholder" selected="selected"></option>';
            $(this).prepend(option);

            settings.placeholder = {
              id: '_placeholder',
              text: $(this).attr('placeholder'),
            };
          }
          else if ($(this)[0].hasAttribute('dna-select2-placeholder-value')) {
            var optionValue = $(this).attr('dna-select2-placeholder-value');
            var optionText = $(this).find('option[value="' + $(this).attr('dna-select2-placeholder-value') + '"]').text();

            settings.placeholder = {
              id: optionValue,
              text: optionText
            };
          }

          $(this)
            .promise()
            .done(function () {
              $(this).trigger('dna-select2-init');
            });

          $(this)
            .select2(settings)
            .closest('.form-wrapper')
            .addClass('dna-select2-form-wrapper');
        });

    }
  };

})(jQuery, Drupal, this, this.document);
