<?php

/**
 * @file
 * Theme functions for DNA JSON API.
 */

/**
 * Theme: Config Form.
 */
function theme_dna_jsonapi_configuration_form($variables) {
  $form = $variables['form'];
  $rows = [];

  foreach (element_children($form['clear_cache_wrapper']['json_array_caches']) as $type) {
    $row = [];

    foreach (element_children($form['clear_cache_wrapper']['json_array_caches'][$type]) as $column) {
      $row[] = ['data' => drupal_render($form['clear_cache_wrapper']['json_array_caches'][$type][$column])];
    }

    $rows[] = ['data' => $row];
  }

  $form['clear_cache_wrapper']['json_array_caches']['#markup'] = theme('table', [
    'header' => $form['clear_cache_wrapper']['json_array_caches']['#header'],
    'rows' => $rows,
    'sticky' => TRUE,
    'empty' => t('There are no defined JSON API types at this time.'),
  ]);

  $output = drupal_render_children($form);
  return $output;
}
