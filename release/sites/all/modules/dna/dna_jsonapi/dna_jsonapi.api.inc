<?php

/**
 * @file
 * API Callbacks for DNA JSON API module.
 */

/**
 * API Endpoint.
 *
 * @todo static errors and add to response/watchdog here.
 * @todo add access contol responses.
 */
function dna_jsonapi_api($type, $entity = NULL) {
  drupal_page_is_cacheable(FALSE);
  $info = dna_jsonapi_api_info($type);
  $op = _dna_jsonapi_get_request_op($entity);
  $function = "dna_jsonapi_api__{$op}";
  $response = [];

  if ($info && function_exists($function)) {
    $response = $function($type, $entity);
  }
  else {
    $response['errors'] = [
      'status' => 400,
      // @todo more detail & watchdog?
      'title' => t('An error occurred. Please contact an administrator for assistance.'),
    ];
  }

  $context = [
    'type' => $type,
    'entity' => $entity,
  ];
  drupal_alter('dna_json_api_response', $response, $context);

  $response['meta']['created'] = REQUEST_TIME;

  drupal_json_output($response);
  drupal_exit();
}

/**
 * JSON API (GET/view).
 *
 * @todo error handling (e.g. missing entity).
 */
function dna_jsonapi_api__view($type, $entity) {
  $function = "dna_jsonapi__{$type}__view";

  if (function_exists($function)) {
    $response = $function($type, $entity);
  }
  else {
    $response = [
      'data' => dna_jsonapi_get_entity_json_array($entity, $type),
    ];
  }

  return $response;
}

/**
 * JSON API (GET/list).
 *
 * @todo add check for dna_jsonapi__TYPE__list().
 * @todo move query portion to separate function. Add hooks.
 * @todo allow query to be overwritten altered. In info hook?
 * @todo make this query only from cache tables since EntityFieldQuery only
 * works with entities.
 */
function dna_jsonapi_api__list($type) {
  $function = "dna_jsonapi__{$type}__view";

  if (function_exists($function)) {
    $response = $function($type);
  }
  // @todo any of the logic below re-usable by the $function above?
  else {
    $info = dna_jsonapi_api_info($type);
    $query = drupal_get_query_parameters();
    $last_requested = !empty($query['last_requested']) ? $query['last_requested'] : 0;
    // @todo the logic for this was removed while other refactoring was taking
    // place. Look into adding it back in.
    $include_level = !empty($query['include_level']) ? $query['include_level'] : 0;
    $response = ['data' => []];
    $ids = dna_jsonapi_get_deleted_item_ids($type, $last_requested);

    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $type);

    if ($last_requested) {
      // @todo Entities (e.g. user) that do not have a changed property means
      // they are sent every time. Need to add a way to track when something
      // changes. Verify a patch to drupal core adds this to the user.
      // @todo this should also check the cache tables created timestamp as a
      // new json array could have been cached w/o a change to the entity.
      if (!empty($info['attributes']['changed'])) {
        $query->propertyCondition('changed', $last_requested, '>=');
      }
    }

    $results = $query->execute();

    if (!empty($results[$type])) {
      $ids = array_merge($ids, array_keys($results[$type]));
    }

    if ($ids) {
      $data = dna_jsonapi_get_entities_json_array($ids, $type);
      $response = [
        'data' => array_values($data),
      ];
    }
  }

  return $response;
}

/**
 * JSON API (POST/add).
 *
 * @todo can use ajax loaded forms for now.
 * @todo need to add try/catch.
 * @todo allow this to add multiple (array) of items?
 * @todo permissions.
 */
function dna_jsonapi_api__add($type) {
  $data = dna_jsonapi_input_data($type, 'add');
  $response = [];
  $errors = [];

  if (!$data) {
    $data = [];
  }

  dna_jsonapi_validate_data_against_schema($data, 'add', $errors);

  if ($errors) {
    $response['errors'] = [
      'status' => 400,
      'title' => implode(' ', $errors),
    ];
  }

  else {
    try {
      $entity = dna_jsonapi_create_entity($data);
      $json_array = dna_jsonapi_get_entity_json_array($entity, $type);
      $response['data'] = [$json_array];
    }
    catch (Exception $e) {
      watchdog(__FUNCTION__, 'An error occurred generating the new entity. Exception: @e', [
        '@e' => $e->getMessage(),
      ], WATCHDOG_ERROR);
      $response['errors'] = [
        'status' => 400,
        'title' => t('An error occurred attempting to create the log entry. Please contact an administrator for assistance.'),
      ];
    }
  }

  if (!empty($response['errors'])) {
    $dblog_data = [
      'Request' => $data,
      'Errors' => $response['errors'],
    ];
    watchdog(__FUNCTION__, "!dblog_data", [
      '!dblog_data' => '<pre>' . print_r($dblog_data, TRUE) . '</pre>',
    ], WATCHDOG_ERROR);
  }
  else {
    // @todo should header go into dna_jsonapi_api()?
    drupal_add_http_header('status', 201);
  }

  return $response;
}
