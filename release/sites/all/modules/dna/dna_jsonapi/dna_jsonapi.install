<?php

/**
 * @file
 * Install, update, and uninstall hooks for DNA JSON API.
 */

/**
 * Add the PouchDB deleted items table.
 */
function dna_jsonapi_update_7101() {
  $table = 'dna_jsonapi_deleted_items';

  if (!db_table_exists($table)) {
    $schema = drupal_get_schema_unprocessed('dna_jsonapi', $table);
    db_create_table($table, $schema);
  }
}

/**
 * Implements hook_uninstall().
 */
function dna_jsonapi_uninstall() {
  // @todo move to dna_core/utils.
  $sql = "
    SELECT
      name
    FROM {variable}
    WHERE name LIKE :module
  ";
  $sql_args = [
    ':module' => 'dna_jsonapi',
  ];
  $vars = db_query($sql, $sql_args)->fetchCol();

  foreach ($vars as $var) {
    variable_del($var);
  }
}

/**
 * Implements hook_schema().
 */
function dna_jsonapi_schema() {
  return [
    'dna_jsonapi_deleted_items' => [
      'description' => 'Tracks entities / items that are deleted.',
      'fields' => [
        'type' => [
          'description' => 'The type of item that was deleted.',
          'type' => 'varchar',
          'length' => 128,
          'not null' => TRUE,
        ],
        'id' => [
          'description' => 'The item ID that was deleted.',
          'type' => 'varchar',
          'length' => 256,
          'not null' => TRUE,
        ],
        'created' => [
          'description' => 'The unix timestamp when the entry was created.',
          'type' => 'int',
          'not null' => TRUE,
        ],
      ],
      'indexes' => [
        'created' => ['created'],
      ],
    ],
  ];
}
