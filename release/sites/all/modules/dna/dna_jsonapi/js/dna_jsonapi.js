/**
 * @file
 * Javascript file for DNA JSON API.
 */

(function ($, Drupal, window, document) {

  'use strict';

  /**
   * Get item link.
   */
  Drupal.dna_jsonapi = {
    getLinkInfo: function (item, linkId, label) {
      var meta = item.meta || {};
      var links = meta.links || {};
      var response = {
        label: label || meta.label || item.id,
        path: '',
      };

      if (links.hasOwnProperty(linkId)) {
        response.path = links[linkId];
      }
      else if (Drupal.settings.dna_jsonapi.api_info.hasOwnProperty(item.type)) {
        links = Drupal.settings.dna_jsonapi.api_info[item.type].meta.links || {};
        var bundle = Object.getByPath(item, 'attributes.bundle', item.type);

        if (links.hasOwnProperty(linkId)) {
          if (typeof links[linkId] === 'string') {
            response.path = '/' + links[linkId].replace('%entity_object', item.id);
          }
          else if (links[linkId].hasOwnProperty(bundle)) {
            response.path = '/' + links[linkId][bundle].replace('%entity_object', item.id);
          }
        }
      }

      return response;
    }

  }
})(jQuery, Drupal, this, this.document);
