<?php

/**
 * @file
 * Admin page callbacks for DNA JSON API module.
 */

/**
 * Form: Basic Configuration.
 */
function dna_jsonapi_configuration_form($form, &$form_state) {
  $component_class = 'dna-jsonapi-configuration-form';
  $form['#component_class'] = $component_class;
  $form['#prefix'] = '<div id="' . $component_class . '-wrapper">';
  $form['#suffix'] = '</div>';
  $form['#attributes']['class'][] = $component_class;

  $api_info = dna_jsonapi_api_info();

  $form['clear_cache_wrapper'] = [
    '#type' => 'container',
    'json_array_caches' => [
      '#header' => [
        'label' => t('Type'),
        'total' => t('Total Items'),
        'cached' => t('Server Cached Items'),
        'cached_actions' => '',
      ],
    ],
    'submit_clear_info' => [
      '#value' => t('Rebuild JSON API Info Cache'),
      '#type' => 'submit',
      '#submit' => ['dna_jsonapi_configuration_form_clear_info_cache_submit'],
      '#ajax' => [
        'callback' => 'dna_jsonapi_configuration_form_ajax_callback',
        'wrapper' => $component_class . '-wrapper',
      ],
    ],
  ];

  foreach ($api_info as $type => $info) {
    $total = t('Unknown');
    $cached = 0;

    if (!empty($info['meta']['entityType'])) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', $info['meta']['entityType'])
        ->entityCondition('bundles', array_keys($info['meta']['bundles']));
      $results = $query->execute();

      $total = count(array_keys($results[$info['meta']['entityType']]));
    }

    $bin = "cache_jsonapi_{$type}";
    $cached = db_query("SELECT cid FROM {{$bin}}")->rowCount();

    $actions = [
      'submit_clear_cache' => [
        '#value' => t('Clear Cache'),
        '#type' => 'submit',
        '#name' => "clear_cache_{$type}",
        '#submit' => ['dna_jsonapi_configuration_form_clear_json_cache_submit'],
        '#ajax' => [
          'callback' => 'dna_jsonapi_configuration_form_ajax_callback',
          'wrapper' => $component_class . '-wrapper',
        ],
        // @todo do we need clear and rebuild?
        '#access' => FALSE,
      ],
      'remove_button' => [
        '#value' => t('Rebuild Cache'),
        '#type' => 'submit',
        '#name' => "rebuild_cache_{$type}",
        '#submit' => ['dna_jsonapi_configuration_form_rebuild_json_cache_submit'],
      ],
    ];

    $row = [
      'label' => ['#markup' => !empty($info['label_plural']) ? $info['label_plural'] : $info['label']],
      'total' => ['#markup' => $total],
      'cached' => ['#markup' => $cached],
      'cached_actions' => $actions,
    ];

    $form['clear_cache_wrapper']['json_array_caches'][$type] = $row;
  }

  // @todo change when there is configuration to alter.
  // return system_settings_form($form);
  return $form;
}

/**
 * Submit handler: Config form clear info cache.
 */
function dna_jsonapi_configuration_form_clear_info_cache_submit($form, &$form_state) {
  $jsonapi_info = dna_jsonapi_api_info(NULL, TRUE);
  drupal_set_message(t('DNA JSON API info cache has been cleared / rebuild.'));
}

/**
 * Submit handler: Config form clear json cache.
 */
function dna_jsonapi_configuration_form_clear_json_cache_submit($form, &$form_state) {
  $trigger = $form_state['triggering_element']['#name'];
  $type = str_replace('clear_cache_', '', $trigger);
  $bin = "cache_jsonapi_{$type}";
  cache_clear_all('*', $bin, TRUE);
  drupal_set_message(t('DNA JSON API @type item cache cleared.', [
    '@type' => $type,
  ]));
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler: Config form rebuild json cache.
 */
function dna_jsonapi_configuration_form_rebuild_json_cache_submit($form, &$form_state) {
  $trigger = $form_state['triggering_element']['#name'];
  $type = str_replace('rebuild_cache_', '', $trigger);
  $bin = "cache_jsonapi_{$type}";
  cache_clear_all('*', $bin, TRUE);

  $batch = [
    'operations' => [
      ['dna_jsonapi_configuration_form_rebuild_json_cache_batch', [$type]],
    ],
    'finished' => 'dna_jsonapi_configuration_form_rebuild_json_cache_batch_finished',
    'title' => t('Rebuilding JSON API item cache for @type', ['@type' => $type]),
    'init_message' => t('Starting process of rebuilding JSON API item cache for @type', [
      '@type' => $type,
    ]),
    'progress_message' => t('Rebuilding JSON API item cache (@percentage% complete).'),
    'error_message' => t('Encountered an error rebuilding JSON API item cache.'),
    'file' => drupal_get_path('module', 'dna_jsonapi') . '/dna_jsonapi.admin.inc',
  ];

  batch_set($batch);
}

/**
 * Batch Operation: Rebuild JSON API item cache.
 *
 * @todo need callback load non-entity types.
 */
function dna_jsonapi_configuration_form_rebuild_json_cache_batch($type, &$context) {
  if (!isset($context['sandbox']['progress'])) {
    $api_info = dna_jsonapi_api_info($type);
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', $api_info['meta']['entityType'])
      ->entityCondition('bundles', array_keys($api_info['meta']['bundles']));
    $results = $query->execute();

    $context['sandbox']['api_info'] = $api_info;
    $context['sandbox']['items'] = array_keys($results[$api_info['meta']['entityType']]);
    $context['sandbox']['current'] = 0;
    $context['sandbox']['progress'] = 0;
    $context['sandbox']['max'] = count($context['sandbox']['items']);
  }

  $item = array_shift($context['sandbox']['items']);
  $json = dna_jsonapi_get_entity_json_array($item, $context['sandbox']['api_info']['meta']['entityType'], TRUE);

  $context['results'][] = $item;
  $context['sandbox']['progress']++;
  $context['sandbox']['current'] = $item;
  $context['message'] = t('Now processing item !id (!progress of !max).', [
    '!id' => is_numeric($item) ? $item : $context['sandbox']['progress'],
    '!progress' => $context['sandbox']['progress'],
    '!max' => $context['sandbox']['max'],
  ]);

  $context['finished'] = $context['sandbox']['progress'] / $context['sandbox']['max'];
}

/**
 * Batch Finished: Rebuild JSON API item cache.
 */
function dna_jsonapi_configuration_form_rebuild_json_cache_batch_finished($success, $results, $operations) {
  if ($success) {
    $message = t('@count items successfully processed:', ['@count' => count($results)]);
    drupal_set_message($message);
  }
  else {
    $error_operation = reset($operations);
    $message = t('An error occurred while processing %error_operation with arguments: @arguments', [
      '%error_operation' => $error_operation[0],
      '@arguments' => print_r($error_operation[1], TRUE),
    ]);
    drupal_set_message($message, 'error');
  }
}

/**
 * AJAX handler: Config form clear json cache.
 */
function dna_jsonapi_configuration_form_ajax_callback($form, &$form_state) {
  return $form;
}
