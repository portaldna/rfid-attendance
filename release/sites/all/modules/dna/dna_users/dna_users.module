<?php

/**
 * @file
 * Code for the DNA Users feature.
 */

include_once 'dna_users.features.inc';

/**
 * Implements hook_entity_property_info_alter().
 */
function dna_users_entity_property_info_alter(&$info) {
  // Add the user changed property to go along with the patch from
  // https://www.drupal.org/project/drupal/issues/1835754.
  $info['user']['properties']['changed'] = [
    'label' => t("Changed"),
    'description' => t("The date the user account was changed."),
    'type' => 'date',
    'schema field' => 'changed',
  ];
}

/**
 * Implements hook_admin_paths_alter().
 */
function dna_users_admin_paths_alter(&$paths) {
  $paths['user/*'] = FALSE;
}

/**
 * Implements template_preprocess_user_profile().
 */
function dna_users_preprocess_user_profile(&$variables) {
  if ($variables['elements']['#view_mode'] != 'full') {
    $variables['theme_hook_suggestions'][] = 'user_profile__' . $variables['elements']['#view_mode'];
  }
}

/**
 * Implements hook_field_widget_form_alter().
 */
function dna_users_field_widget_form_alter(&$element, &$form_state, $context) {
  // @todo removed display name field from this module however keeping this
  // logic here to refer back to if / when field added back.
  if (FALSE && !empty($element['#field_name']) && $element['#field_name'] == 'field_user_display_name') {
    $element['value']['#element_validate'][] = 'dna_users_display_name_validate';
  }
}

/**
 * Validate handler: Display Name field.
 *
 * @todo removed display name field from this module however keeping this
 * logic here to refer back to if / when field added back.
 */
function dna_users_display_name_validate($element, &$form_state) {
  // Only validate the display name field for uniqueness if it was changed from
  // the original value or if this is a new user.
  $requires_validation = FALSE;

  if (!empty($element['#entity_type']) && $element['#entity_type'] == 'user') {
    $account = $element['#entity'];
    $requires_validation = !empty($account->is_new) || (isset($account->field_user_display_name) && $account->field_user_display_name[LANGUAGE_NONE][0]['value'] != $form_state['values']['field_user_display_name'][LANGUAGE_NONE][0]['value']);
  }

  if ($requires_validation) {
    $sql = 'SELECT field_user_display_name_value FROM field_data_field_user_display_name';
    $sql .= ' WHERE field_user_display_name_value = :display_name';
    $sql_args = array(':display_name' => $form_state['values']['field_user_display_name'][LANGUAGE_NONE][0]['value']);
    $results = db_query($sql, $sql_args)->fetchField();

    if (!empty($results)) {
      form_error($element, t('The @field_name @display_name is currently in use. Please choose a different @field_name.', array(
        '@field_name' => $element['#title'],
        '@display_name' => $form_state['values']['field_user_display_name'][LANGUAGE_NONE][0]['value'],
      )));
    }
  }
}
