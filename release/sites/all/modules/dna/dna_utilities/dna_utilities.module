<?php

/**
 * @file
 * Drupal needs this blank file.
 */

/**
 * Implements hook_page_alter().
 */
function dna_utilities_page_alter(&$page) {
  $page['page_bottom']['dna_utilities']['#attached']['js'][] = [
    'data' => drupal_get_path('module', 'dna_utilities') . '/js/dna_utilities.js',
    'scope' => 'footer',
  ];
}

/**
 * Load Entities.
 */
function dna_utilities_ensure_entities($entities, $entity_type) {
  if (!is_array($entities)) {
    $entities = [$entities];
  }

  $first_entity = reset($entities);

  if (is_numeric($first_entity)) {
    $entities = entity_load($entity_type, $entities);
  }

  return $entities;
}

/**
 * Load Entity.
 */
function dna_utilities_ensure_entity($entity, $entity_type) {
  $entities = dna_utilities_ensure_entities([$entity], $entity_type);
  return reset($entities);
}

/**
 * Create Entity.
 */
function dna_utilities_create_entity($entity_type, $bundle, array $fields = [], $clone_from = NULL, $save = TRUE) {
  $entity = entity_create($entity_type, ['type' => $bundle]);

  $no_clone = [
    'id',
    'uuid',
    'created',
    'changed',
    'status',
  ];

  if ($clone_from) {
    $clone_from = dna_utilities_ensure_entity($clone_from, $entity_type);

    if ($clone_from->bundle() != $bundle) {
      $clone_from = NULL;
    }
  }

  if ($clone_from && $clone_from->bundle() == $bundle) {
    $clone_wrapper = entity_metadata_wrapper($entity_type, $clone_from);
    $props = array_keys($clone_wrapper->getPropertyInfo());

    $context = [
      'entity_type' => $entity_type,
      'bundle' => $bundle,
    ];
    drupal_alter('dna_utilities_create_entity_no_clone', $no_clone, $context);

    foreach ($props as $prop) {
      if (in_array($prop, $no_clone)) {
        continue;
      }

      $value = $clone_wrapper->{$prop}->value();

      if (isset($value) && !empty($value)) {
        $fields[$prop] = $value;
      }
    }
  }

  dna_utilities_update_entity($entity_type, $entity, $fields, $save);
  return $entity;
}

/**
 * Update Entity.
 */
function dna_utilities_update_entity($entity_type, &$entity, $fields, $save = TRUE) {
  $entity_wrapper = entity_metadata_wrapper($entity_type, $entity);

  $context = [
    'entity_type' => $entity_type,
    'bundle' => $entity_wrapper->getBundle(),
  ];
  drupal_alter('dna_utilities_update_entity_fields', $fields, $context);

  foreach ($fields as $field => $value) {
    $entity_wrapper->{$field} = $value;
  }

  if ($save) {
    $entity_wrapper->save();
  }

  $entity = $entity_wrapper->value();
}

/**
 * Account timezone.
 */
function dna_utilities_account_timezone($account = NULL) {
  $account = dna_utilities_ensure_entity($account, 'user');

  if (!$account) {
    $account = $GLOBALS['user'];
  }

  $timezone = !empty($account->timezone) ? $account->timezone : date_default_timezone(FALSE);
  return $timezone;
}

/**
 * Format Date.
 */
function dna_utilities_format_date($timestamp, $type = 'medium', $format = '', $account = NULL) {
  $timezone = $account ? dna_utilities_account_timezone($account) : NULL;
  return format_date($timestamp, $type, $format, $timezone);
}

/**
 * Send Email.
 */
function dna_utilities_send_mail($module, $key, $params = []) {
  if (empty($params['recipients'])) {
    watchdog(__FUNCTION__, 'Unable to send email !module !key without any recipients. Params: !params', [
      '!module' => $module,
      '!key' => $key,
      '!params' => '<pre>' . print_r($params, TRUE) . '</pre>',
    ], WATCHDOG_ERROR);
    return FALSE;
  }

  $params['module'] = $module;
  $params['mail_key'] = $key;

  if (!is_array($params['recipients'])) {
    $params['recipients'] = [$params['recipients']];
  }

  $first_recipient = reset($params['recipients']);
  $to = [];

  if (is_numeric($first_recipient)) {
    $sql = "
      SELECT DISTINCT
        mail
      FROM {users}
      WHERE uids IN (:uids)
    ";
    $sql_args = [':uids' => $params['recipients']];
    $to = db_query($sql, $sql_args)->fetchCol();
  }
  elseif (is_object($first_recipient) && !empty($first_recipient->mail)) {
    foreach ($params['recipients'] as $delta => $recipient) {
      $to[] = $recipient->mail;
    }
  }
  elseif (is_string($first_recipient)) {
    $to = $params['recipients'];
  }

  if (is_array($to)) {
    $to = array_values($to);
    $to = array_unique($to);
    $to = implode(',', $to);
  }

  if (!$to) {
    watchdog(__FUNCTION__, 'Unable to send email !module !key. Unable to determine email addresses. Params: !params', [
      '!module' => $module,
      '!key' => $key,
      '!params' => '<pre>' . print_r($params, TRUE) . '</pre>',
    ], WATCHDOG_ERROR);
    return FALSE;
  }

  if (empty($params['language'])) {
    $params['language'] = $GLOBALS['language'];
  }

  return drupal_mail($module, $key, $to, $params['language'], $params);
}

/**
 * AJAX Command: Drupal Goto.
 */
function dna_utilities_ajax_command_drupal_goto($path = '', array $options = []) {
  $url = url($path, $options);
  $command = [
    'command' => 'dna_utilities_drupalGoto',
    'data' => [
      'path' => $path,
      'options' => $options,
      'url' => $url,
    ],
  ];
  return $command;
}

/**
 * AJAX Command: Display status messages on page.
 */
function dna_utilities_ajax_command_status_messages_target($type = NULL, $target_selector = '') {
  $content = theme('status_messages', ['display' => $type]);
  $command = _dna_utilities_ajax_command_status_message_target_helper($content, $type, $target_selector);
  $command['dna_utilities_ajax_command_status_messages'] = TRUE;
  return $command;
}

/**
 * AJAX Command: Display status messages in dialog.
 */
function dna_utilities_ajax_command_status_messages_dialog($type = NULL) {
  $content = theme('status_messages', ['display' => $type]);
  $command = module_exists('a11ydialog') ? a11ydialog_ajax_command_open('a11ydialog-shared', $content, '') : [];
  drupal_alter('dna_utilities_ajax_command_status_messages_dialog', $command);

  // If a11ydialog does not exist and no other module modified the command then
  // initiate the dna_utilities_ajax_command_status_messages_target() command.
  if (!$command) {
    $command = _dna_utilities_ajax_command_status_message_target_helper($content, $type, $target_selector);
  }

  $command['dna_utilities_ajax_command_status_messages'] = TRUE;
  return $command;
}

/**
 * AJAX Command: Display status messages.
 */
function dna_utilities_ajax_command_status_messages($type = NULL, $dialog = TRUE) {
  if ($dialog) {
    $command = dna_utilities_ajax_command_status_messages_dialog($type);
  }
  else {
    $command = dna_utilities_ajax_command_status_messages_target($type);
  }

  drupal_alter('dna_utilities_ajax_command_status_messages', $command);
  return $command;
}

/**
 * Get 'path'.
 *
 * When ajax calls are made the [current/request]_path is system/ajax which can
 * cause logic issues based on path. So this is a wrapper to work around that.
 *
 * @param string $path
 *   A path from current_path() or request_path().
 * @param array $ajax_paths
 *   List of ajax paths to work around.
 *
 * @return string
 *   A path that attempts to work around system/ajax paths.
 */
function dna_utilities_get_path($path = '', array $ajax_paths = []) {
  if (!$path) {
    $path = current_path();
  }

  $ajax_paths[] = 'system/ajax';

  if (in_array(current_path(), $ajax_paths) && !empty($_SERVER['HTTP_REFERER'])) {
    $parts = parse_url($_SERVER['HTTP_REFERER']);
    $path = trim($parts['path'], '/');
  }

  return $path;
}

/**
 * Get difference between two items.
 */
function dna_utilities_compute_differences($new, $original = NULL, array $skip_keys = []) {
  if ($new && !is_array($new)) {
    $new = (array) $new;
  }

  if (!$original && !empty($new['original'])) {
    $original = $new['original'];
  }

  if (!empty($new['original'])) {
    unset($new['original']);
  }

  if ($original && !is_array($original)) {
    $original = (array) $original;
  }

  if (!empty($original['original'])) {
    unset($original['original']);
  }

  if ($new && !$original) {
    return $new;
  }

  if ($original && !$new) {
    return $original;
  }

  if (!$new && !$original) {
    return [];
  }

  $diff = [];

  // Some properties don't need to be compared or otherwise get added
  // inconsistently. So attempt to rectify those here.
  _dna_utilities_compute_differences_cleaner($new);
  _dna_utilities_compute_differences_cleaner($original);

  $skip_keys = array_merge($skip_keys, [
    // Various date field elements.
    'show_todate',
    'offset',
    'offset2',
  ]);

  foreach ($new as $k => $v) {
    if (array_key_exists($k, $original)) {
      if (is_array($v)) {
        $rdiff = dna_utilities_compute_differences($v, $original[$k]);
        if (count($rdiff)) {
          $diff[$k] = $rdiff;
        }
      }
      else {
        if ($v != $original[$k]) {
          $diff[$k] = $v;
        }
      }
    }
    elseif (!in_array($k, $skip_keys)) {
      $diff[$k] = $v;
    }
  }

  return $diff;
}

/**
 * Helper to load account.
 */
function dna_utilities_load_account($account = NULL, $check_path = FALSE) {
  if (!$account) {
    if ($check_path) {
      $path = dna_utilities_get_path();

      if (strpos($path, 'user/') === 0) {
        $path_parts = explode('/', $path);
        $account = $path[1];
      }
    }

    if (!$account) {
      $account = $GLOBALS['user']->uid;
    }
  }

  $account = dna_utilities_ensure_entity($account, 'user');

  return $account;
}

/**
 * Add ajax command alter capabilities with context.
 *
 * @todo hook_ajax_render_alter() exists but does not provide any context.
 */
function dna_utilities_ajax_command_alter(&$commands, array $context = [], $key = '', $display_status_messages = FALSE) {
  drupal_alter('dna_utilities_ajax_commands', $commands, $context, $key);

  if ($display_status_messages) {
    foreach ($commands as $delta => $command) {
      if (!empty($command['dna_utilities_ajax_command_status_messages'])) {
        $display_status_messages = FALSE;
        break;
      }
    }
  }

  $commands = array_values($commands);

  if ($display_status_messages) {
    $command = dna_utilities_ajax_command_status_messages();
    unset($command['dna_utilities_ajax_command_status_messages']);
    $commands[] = $command;
  }
}

/**
 * Get form(_state) trigger.
 */
function dna_utilities_get_form_trigger($form_state) {
  return !empty($form_state['triggering_element']['#name']) ? $form_state['triggering_element']['#name'] : '';
}

/**
 * Load include file.
 */
function dna_utilities_module_load_include($filename, $module = '') {
  list($module, $filename, $ext) = _dna_utilities_load_include_helper($filename, $module);

  if (!$filename) {
    return;
  }

  return module_load_include($ext, $module, $filename);
}

/**
 * Form load include file.
 */
function dna_utilities_form_load_include(&$form_state, $filename, $module = '') {
  list($module, $filename, $ext) = _dna_utilities_load_include_helper($filename, $module);

  if (!$filename) {
    return;
  }

  return form_load_include($form_state, $ext, $module, $filename);
}

/**
 * Quick module feature revert.
 */
function dna_utilities_revert_feature($modules) {
  if (!is_array($modules)) {
    $modules = [$modules];
  }

  foreach ($modules as $module) {
    features_revert_module($module);
  }
}

/**
 * Insert into array.
 *
 * @todo search_by value doesn't work if array values are themselves arrays.
 */
function dna_utilities_array_insert(array &$haystack, $needle, $value, $where = 'after', $search_by = 'key') {
  $searchable = ($search_by == 'key') ? array_keys($haystack) : array_values($haystack);
  $index = array_search($needle, $searchable);

  if ($index === FALSE) {
    return FALSE;
  }

  if ($where == 'after') {
    $index++;
  }

  $first = array_slice($haystack, 0, $index, TRUE);
  $last = array_slice($haystack, $index, count($haystack) - $index, TRUE);
  $haystack = $first + $value + $last;

  $last_weight = NULL;

  foreach ($haystack as $key => &$value) {
    if (isset($value['#weight'])) {
      $weight_key = '#weight';
    }
    elseif (isset($value['weight'])) {
      $weight_key = 'weight';
    }
    else {
      continue;
    }

    $weight = (int) $value[$weight_key];

    if (is_null($last_weight)) {
      $last_weight = $weight;
      continue;
    }

    if ($weight <= $last_weight) {
      $value[$weight_key] = $last_weight + 1;
      $last_weight = $value[$weight_key];
      continue;
    }

    $last_weight = $weight;
  }

  return TRUE;
}

/**
 * Helper: Clean items for comparison.
 */
function _dna_utilities_compute_differences_cleaner(&$item) {
  unset($item['changed']);
  $item['is_new'] = !empty($item['is_new']);
}

/**
 * Helper: Get module, filename, ext for load includes.
 */
function _dna_utilities_load_include_helper($filename, $module = '') {
  $output = [
    'module' => $module,
    'filename' => '',
    'ext' => '',
  ];

  if ($filename) {
    $paths = explode('/', $filename);
    $filename = array_pop($paths);

    if (!$module) {
      $parts = explode('.', $filename);
      $output['module'] = $parts[0];
    }

    $output['ext'] = pathinfo($filename, PATHINFO_EXTENSION);
    $output['filename'] = pathinfo($filename, PATHINFO_FILENAME);

    if ($paths) {
      $output['filename'] = implode('/', $paths) . '/' . $output['filename'];
    }
  }

  return array_values($output);
}

/**
 * Helper: Status Messages AJAX Command.
 */
function _dna_utilities_ajax_command_status_message_target_helper($content, $type = NULL, $target_selector = '') {
  $selector = $target_selector ?: variable_get('dna_utilities_ajax_command_status_messages_target', '#content');
  $command = ajax_command_prepend($selector, $content);
  return $command;
}
