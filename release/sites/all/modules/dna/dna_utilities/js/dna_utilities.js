/**
 * @file
 * Javascript file for DNA Utilities.
 */

'use strict';

// @see https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/assign
if (typeof Object.assign != 'function') {
  // Must be writable: true, enumerable: false, configurable: true
  Object.defineProperty(Object, "assign", {
    value: function assign(target, varArgs) { // .length of function is 2
      'use strict';
      if (target == null) { // TypeError if undefined or null
        throw new TypeError('Cannot convert undefined or null to object');
      }

      var to = Object(target);

      for (var index = 1; index < arguments.length; index++) {
        var nextSource = arguments[index];

        if (nextSource != null) { // Skip over if undefined or null
          for (var nextKey in nextSource) {
            // Avoid bugs when hasOwnProperty is shadowed
            if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
              to[nextKey] = nextSource[nextKey];
            }
          }
        }
      }
      return to;
    },
    writable: true,
    configurable: true
  });
}

// @see https://stackoverflow.com/questions/6491463/accessing-nested-javascript-objects-with-string-key
Object.getByPath = function (obj, path, defaultValue) {
  path = path.replace(/\[(\w+)\]/g, '.$1');
  path = path.replace(/^\./, '');
  var pathParts = path.split('.');

  for (var i = 0, n = pathParts.length; i < n; ++i) {
    var k = pathParts[i];

    if (typeof obj == 'object' && obj != null && obj.hasOwnProperty(k)) {
      obj = obj[k];
    }
    else {
      return defaultValue;
    }
  }

  return obj;
}

if (typeof Array.unique != 'function') {
  Array.unique = function (arr) {
    return arr.filter(function (item, index) {
      return arr.indexOf(item) >= index;
    });
  };
}

// @see https://gomakethings.com/custom-events-with-vanilla-javascript/
(function () {
  if ( typeof window.CustomEvent === "function" ) return false;

  function CustomEvent ( event, params ) {
    params = params || { bubbles: false, cancelable: false, detail: undefined };
    var evt = document.createEvent( 'CustomEvent' );
    evt.initCustomEvent( event, params.bubbles, params.cancelable, params.detail );
    return evt;
   }

  CustomEvent.prototype = window.Event.prototype;

  window.CustomEvent = CustomEvent;
})();


/**
 * @todo
 */
var DNAUtils = (function (me) {

  var config = {
    // @todo make this configurable.
    debug: true,
    canLog: (typeof console !== 'undefined' && typeof console.log !== 'undefined'),
  };

  // @todo how to use console.warn, etc.?
  var log = function log() {
    if (config.debug && config.canLog) {
      console.log.apply(console, arguments);
    }
  };

  var timestamp = function timestamp() {
    return Math.floor(new Date().getTime() / 1000);
  };

  var simpleUniqueID = function simpleUniqueID() {
    return (Date.now().toString(36) + Math.random().toString(36).substr(2, 5)).toUpperCase();
  };

  var pause = function pause(duration) {
    return new Promise(function (res) {
      return setTimeout(res, duration);
    });
  };

  // @see https://tech.mybuilder.com/handling-retries-and-back-off-attempts-with-javascript-promises/
  // @see https://itnext.io/blog-md-3f9801f99454
  var retry = function retry(fn, args, retries, maxDelay, delay, startTs) {
    args = args || [];
    retries = (retries !== undefined) ? retries : 5;
    delay = delay || 500;
    maxDelay = maxDelay || (retries * delay);
    startTs = startTs || Date.now();

    return fn.apply(null, args)
      .catch(function (err) {
        return (retries > 1 && maxDelay > 0) ? DNAUtils.pause(delay).then(function () {
          return DNAUtils.retry(fn, args, retries - 1, maxDelay - delay, delay * 2, startTs);
        }) : Promise.reject(err);
      });
  };

  // @see https://coderwall.com/p/miqu3g/easily-extend-javascript-prototypes
  // @see https://hackernoon.com/understanding-javascript-prototype-and-inheritance-d55a9a23bde2
  function extend (base, constructor) {
    var prototype = new Function();
    prototype.prototype = base.prototype;
    constructor.prototype = new prototype();
    constructor.prototype.constructor = constructor;
  }

  me = {
    log: log,
    timestamp: timestamp,
    pause: pause,
    retry: retry,
    extend: extend,
  };

  return me;

}(DNAUtils || {}));

(function ($, Drupal, window, document) {

  'use strict';

  Drupal.dna_utilities = {
    drupalGoto: function (ajax, response, status) {
      window.location.href = response.url;
    }
  };

  Drupal.ajax.prototype.commands.dna_utilities_drupalGoto = Drupal.dna_utilities.drupalGoto;

})(jQuery, Drupal, this, this.document);
