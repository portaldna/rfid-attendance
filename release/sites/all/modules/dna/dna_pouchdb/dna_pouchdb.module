<?php

/**
 * @file
 * Main file for DNA PouchDB.
 *
 * @todo how to track / update deleted entities/records?
 */

/**
 * Implements hook_page_build().
 */
function dna_pouchdb_page_build(&$page) {
  $settings = [
    'dna_pouchdb' => _dna_pouchdb_generate_js_settings(),
  ];

  $page['page_bottom']['dna_pouchdb']['#attached']['js'][] = [
    'data' => $settings,
    'type' => 'setting',
  ];
  $page['page_bottom']['dna_pouchdb']['#attached']['libraries_load'][] = ['pouchdb'];
}

/**
 * Implements hook_dna_datatables_table_info_alter().
 */
function dna_pouchdb_dna_datatables_table_info_alter(&$info) {
  foreach ($info as $id => &$table_info) {
    if (!empty($table_info['meta']['jsonapi_type'])) {
      $urls = !empty($table_info['config']['ajax']['url']) ? [$table_info['config']['ajax']['url']] : [];
      $relatedTypes = array_unique(array_values($table_info['meta']['relationships']));

      foreach ($relatedTypes as $relatedType) {
        if ($api_info = dna_jsonapi_api_info($relatedType)) {
          $urls[] = $api_info['apiPath'];
        }
      }

      $table_info['config']['ajax']['function'] = 'dna_pouchdb';
      $table_info['config']['ajax']['params'] = [
        'database' => 'default',
        'type' => $table_info['meta']['jsonapi_type'],
        'urls' => $urls,
      ];
    }
  }
}

/**
 * Implements hook_dna_ecker_entity_form_submit_ajax_commands_alter().
 *
 * @todo might be easier to just use hook_ajax_render_alter().
 */
function dna_pouchdb_dna_ecker_entity_form_submit_ajax_commands_alter(&$commands, $form, $form_state) {
  if (form_get_errors()) {
    return;
  }

  $json_arrays = dna_jsonapi_static_json_build();
  $commands[] = dna_pouchdb_ajax_command_load_data($json_arrays);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function dna_pouchdb_form_dna_jsonapi_configuration_form_alter(&$form, &$form_state) {
  $reset_timestamps = variable_get('dna_pouchdb_cache_reset_timestamps', []);
  $api_info = dna_jsonapi_api_info();

  $columns = [
    'reset_ts' => t('Client Reset'),
    'reset_ts_actions' => '',
  ];
  dna_utilities_array_insert($form['clear_cache_wrapper']['json_array_caches']['#header'], 'cached_actions', $columns);

  foreach ($api_info as $type => $info) {
    if (!empty($form['clear_cache_wrapper']['json_array_caches'][$type])) {
      $last_reset = !empty($reset_timestamps[$type]) ? format_date($reset_timestamps[$type]['timestamp'], 'short') : t('Never');
      $column = [
        'reset_ts' => ['#markup' => $last_reset],
      ];
      dna_utilities_array_insert($form['clear_cache_wrapper']['json_array_caches'][$type], 'cached_actions', $column);
      $form['clear_cache_wrapper']['json_array_caches'][$type]['actions']['remove_button'] = [
        '#value' => t('Reset Client'),
        '#type' => 'submit',
        '#name' => "reset_client_{$type}",
        '#submit' => ['dna_pouchdb_form_dna_jsonapi_configuration_form_reset_client_submit'],
        '#ajax' => $form['clear_cache_wrapper']['json_array_caches'][$type]['cached_actions']['submit_clear_cache']['#ajax'],
      ];
    }
  }
}

/**
 * Submit handler: DNA JSON API config form reset client.
 */
function dna_pouchdb_form_dna_jsonapi_configuration_form_reset_client_submit($form, &$form_state) {
  $trigger = $form_state['triggering_element']['#name'];
  $type = str_replace('reset_client_', '', $trigger);
  $api_info = dna_jsonapi_api_info($type);
  $reset_timestamps = variable_get('dna_pouchdb_cache_reset_timestamps', []);
  $reset_timestamps[$type] = [
    'apiPath' => $api_info['apiPath'],
    'timestamp' => REQUEST_TIME,
  ];
  variable_set('dna_pouchdb_cache_reset_timestamps', $reset_timestamps);
  drupal_set_message(t('Client reset for type @type at @time. The next API request by each user will trigger a full rebuild of the @type cache.', [
    '@type' => $type,
    '@time' => format_date($reset_timestamps[$type]['timestamp'], 'short'),
  ]));
  $form_state['rebuild'] = TRUE;
}

/**
 * Implements hook_dna_core_libraries().
 */
function dna_pouchdb_dna_core_libraries() {
  return [
    // pouchdb.
    'pouchdb' => [
      'url' => 'https://github.com/pouchdb/pouchdb',
      'version' => '7.0.0',
      'info' => [
        'version arguments' => [
          'file' => 'pouchdb.min.js',
          'pattern' => '@PouchDB\s+([0-9\.-]+)@',
          'lines' => 3,
        ],
        'files' => [
          'js' => [
            'pouchdb.min.js',
            'pouchdb.find.min.js',
            'pouchdb.upsert.min.js',
            'pouchdb.crypto-pouch.min.js',
          ],
        ],
        'dependencies' => [
          'promise-polyfill',
          'fetch',
        ],
        'integration files' => [
          'dna_pouchdb' => [
            'js' => [
              'js/dna_pouchdb.pouchdb.js',
              'js/dna_pouchdb.js',
            ],
          ],
        ],
      ],
    ],
  ];
}

/**
 * AJAX command to add data to database.
 */
function dna_pouchdb_ajax_command_load_data($json_arrays, $db_name = '') {
  return [
    'command' => 'dna_pouchdb_loadData',
    'data' => [
      'dbName' => $db_name ?: 'default',
      'docs' => $json_arrays,
    ],
  ];
}

/**
 * Get pouchdb database info.
 */
function dna_pouchdb_database_info() {
  $dbs_info = &drupal_static(__FUNCTION__);

  if (!isset($dbs_info)) {
    $modules = module_implements('dna_pouchdb_database_info');

    foreach ($modules as $module) {
      $dbs = module_invoke($module, 'dna_pouchdb_database_info');

      foreach ($dbs as $db_name => $db_info) {
        $options = !empty($db_info['options']) ? $db_info['options'] : [];
        $preload_urls = !empty($db_info['preload_urls']) ? $db_info['preload_urls'] : [];
        $indexes = !empty($db_info['indexes']) ? $db_info['indexes'] : [];

        $dbs_info[$db_name] = [
          'name' => $db_name,
          'options' => $options + [
            // @see https://stackoverflow.com/questions/36148675/how-to-limit-pouchdb-revisions-or-permanently-delete-revisions
            'revs_limit' => 1,
            'auto_compaction' => TRUE,
          ],
          'per_user' => FALSE,
          'preloadURLs' => $preload_urls,
          'indexes' => array_merge($indexes, [
            'type' => [
              'fields' => ['type'],
            ],
            'cacheKeys' => [
              'fields' => ['meta.cacheKeys'],
            ],
          ]),
        ];
      }
    }

    drupal_alter('dna_pouchdb_database_info', $dbs_info);
  }

  return $dbs_info;
}

/**
 * Get pouch crypto password.
 */
function dna_pouchdb_crypto_password() {
  global $user;

  $data = $user->init . $user->created;
  $key = drupal_get_hash_salt() . $user->init;
  return drupal_hmac_base64($data, $key);
}

/**
 * Generate javascript settings.
 *
 * @todo add default pouchdb settings (e.g. used when creating db).
 */
function _dna_pouchdb_generate_js_settings() {
  $databases = dna_pouchdb_database_info();
  // @todo make db specific.
  $lastReset = variable_get('dna_pouchdb_cache_reset_timestamps', []);

  foreach ($databases as $db_id => &$database) {
    $database['lastReset'] = $lastReset;
  }

  $default = reset($databases);
  $default = !empty($default['name']) ? $default['name'] : '';
  $settings = [
    'default' => $default,
    'databases' => $databases,
    // @todo
    // 'crypto' => dna_pouchdb_crypto_password(),
  ];
  return $settings;
}
