/**
 * @file
 * Javascript file for DNA PouchDB.
 *
 * @todo avoid multiple calls to same API when multiple tables exist on a page.
 */

var DNAPouchDB = (function (me) {

  'use strict';

  var _dbs = {};
  var _db = null;

  var _DEFAULT_CONFIG = {
    options: {
      revs_limit: 1,
      // deterministic_revs: true,
      auto_compaction: true
    },
    indexes: {
      type: {
        fields: ['type'],
      },
      cacheKeys: {
        fields: ['meta.cacheKeys'],
      }
    },
    per_user: false,
    encrypted: false,
    crypto: false,
    lastReset: {}
  };

  var _DEFAULT_GET_ALL_OPTIONS = {
    include_docs: true
  };

  var _DEFAULT_CHANGES_OPTIONS = {
    since: 'now',
    live: true,
    include_docs: true
  };

  var dbExists = function (name) {
    var db = new PouchDB(name, { skip_setup: true });
    return db.info()
      .then(function (info) {
        return true;
      })
      .catch(function (err) {
        return false;
      })
  };

  var initializing = function (name) {
    return _dbs.hasOwnProperty(name) ? _dbs[name].initializing : false;
  };

  var initialized = function (name) {
    return _dbs.hasOwnProperty(name) ? _dbs[name].initialized : false;
  };

  var localKey = function (prefix, key) {
    return `_local/${prefix}__${key}`;
  };

  var setTracker = function (trackerType, key, value) {
    return upsert({
      _id: localKey(trackerType, key),
      value: value
    });
  };

  var getTracker = function (trackerType, key, defaultValue) {
    return get(localKey(trackerType, key))
      .then(function (doc) {
        return doc.value;
      })
      .catch(function (e) {
        return defaultValue;
      });
  };

  var getItemId = function (item) {
    if (item === null || item === undefined) {
      return null;
    }

    return item.hasOwnProperty('_id') ? item._id : `${item.type}/${item.id}`;
  };

  var createIndex = function (indexName, index) {
    index = Object.assign({
      name: indexName,
      ddoc: indexName
    }, index);

    return _db.createIndex({index: index});
  };

  var createIndexes = function (indexes) {
    var indexPromises = [];

    Object.keys(indexes).forEach(function (indexName) {
      indexPromises.push(createIndex(indexName, indexes[indexName]));
    });

    return indexPromises.length ? Promise.all(indexPromises) : Promise.resolve(true);
  };

  var info = function (key) {
    return _db.info()
      .then(function (info) {
        if (key === undefined) {
          return info;
        }
        else {
          return info.hasOwnProperty(key) ? info[key] : '';
        }
      });
  };

  var add = function (doc) {
    return _db.put(doc);
  };

  var get = function (id) {
    return _db.get(id);
  };

  var update = function (doc) {
    return _db.get(doc._id)
      .then(function (res) {
        return add(Object.assign(res, doc));
      });
  };

  // @see https://github.com/pouchdb/upsert
  var upsert = function (doc) {
    return _db.upsert(doc._id, function (existingDoc) {
      return Object.assign(existingDoc, doc);
    });
  };

  var remove = function (id) {
    return _db.get(id)
      .then(function (doc) {
        return _db.remove(doc);
      });
  };

  var getAll = function (options) {
    options = options || {};
    options = Object.assign(_DEFAULT_GET_ALL_OPTIONS, options);

    return _db.allDocs(options)
      .then(function (res) {
        return res.rows.map(function (row) {
          return row.doc;
        });
      });
  };

  var find = function () {
    // @todo
  };

  var getRelatedIDs = function (docs) {
    return new Promise(function (resolve, reject) {
      var ids = [];

      docs.forEach(function (doc) {
        if (doc.hasOwnProperty('relationships')) {
          Object.keys(doc.relationships).forEach(function (attr) {
            var data = doc.relationships[attr].data;

            if (!Array.isArray(data)) {
              data = [data];
            }

            data.forEach(function (attrData) {
              var id = getItemId(attrData);

              if (id !== null && !ids.includes(id)) {
                ids.push(id);
              }
            });
          });
        }
      });

      resolve(ids);
    });
  };

  var getRelationships = function (ids) {
    return getAll({ include_docs: true, keys: ids })
      .then(function (relatedDocs) {
        var relatedDocsKeyed = {};

        relatedDocs.forEach(function (relatedDoc) {
          var id = getItemId(relatedDoc);

          if (id !== null) {
            relatedDocsKeyed[id] = relatedDoc;
          }
        });

        return relatedDocsKeyed;
      });
  };

  var attachDocs = function (docs, relatedDocs) {
    return new Promise(function (resolve, reject) {
      docs.forEach(function (doc) {
        if (doc.hasOwnProperty('relationships')) {
          Object.keys(doc.relationships).forEach(function (attr) {
            var data = doc.relationships[attr].data;
            var newData = [];

            if (!Array.isArray(data)) {
              data = [data];
            }

            data.forEach(function (attrData) {
              var id = getItemId(attrData);

              if (id !== null && relatedDocs.hasOwnProperty(id)) {
                newData.push(relatedDocs[id]);
              }
            })

            if (newData.length) {
              doc.relationships[attr].data = (newData.length > 1) ? newData : newData[0];
            }
          });
        }
      });

      resolve(docs);
    });
  };

  var getAllByType = function (type, includeRelationshipsLevel) {
    includeRelationshipsLevel = includeRelationshipsLevel || 0;

    return _db.find({
      selector: {
        type: type
      },
      use_index: 'type'
    })
      .then(function (res) {
        return (includeRelationshipsLevel <= 0) ? res.docs : attachRelatedDocs(res.docs);
      });
  };

  var attachRelatedDocs = function (docs) {
    if (!Array.isArray(docs)) {
      docs = [docs];
    }

    return getRelatedIDs(docs)
      .then(function (relatedIDs) {
        return getRelationships(relatedIDs);
      })
      .then(function (relatedDocs) {
        return attachDocs(docs, relatedDocs);
      });
  }

  var bulkAdd = function (docs) {
    return _db.bulkDocs(docs);
  };

  var bulkUpdate = function (docs) {
    // @todo
  };

  // @see https://github.com/tlvince/pouchdb-upsert-bulk
  var bulkUpsert = function (docs, options) {
    options = options || {};

    if (!Array.isArray(docs)) {
      docs = [docs];
    }

    var allDocsOpts = {
      keys: docs.map(function (doc) { return doc._id; }),
    };

    if (!options.replace) {
      allDocsOpts.include_docs = true;
    }

    return _db.allDocs(allDocsOpts)
      .then(function (res) {
        return docs.map(function (doc) {
          var row = res.rows.find(function (row) {
            return row.id === doc._id;
          });

          if (!row || row.error) {
            return doc;
          }

          if (!options.replace) {
            return Object.assign({}, row.doc, doc);
          }

          return Object.assign({}, doc, {
            _rev: row.value.rev,
          });
        });
      })
      .then(function (docs) {
        return _db.bulkDocs(docs);
      });
  };

  var bulkRemove = function (docs) {
    // @todo
  };

  // @see https://stackoverflow.com/questions/29877607/pouchdb-delete-alldocs-javascript
  var removeAllByType = function (type) {
    return getAllByType(type, 0)
      .then(function (docs) {
        return docs.map(function (doc) {
          doc._deleted = true;
          return doc;
        });
      })
      .then(function (docsToDelete) {
        return _db.bulkDocs(docsToDelete);
      });
  };

  var resetType = function (type, resetInfo) {
    var typePromises = [];

    return getTracker('lastReset', type, 0)
      .then(function (lastResetTimestamp) {
        if (lastResetTimestamp >= resetInfo.timestamp) {
          typePromises.push(Promise.resolve(true));
        }
        else {
          typePromises.push(setTracker('lastRefresh', resetInfo.apiPath, 0));
          typePromises.push(removeAllByType(type));
          typePromises.push(setTracker('lastReset', type, resetInfo.timestamp));
        }

        return typePromises.length ? Promise.all(typePromises) : Promise.resolve(true);
      });
  };

  var handleResets = function (resets) {
    var resetPromises = [];

    Object.keys(resets).forEach(function (type) {
      resetPromises.push(resetType(type, resets[type]));
    });

    return resetPromises.length ? Promise.all(resetPromises) : Promise.resolve(true);
  };

  // @todo handle non-success / errors to avoid settings lastRefresh flag, etc.
  var getJSON = function (url, params) {
    params = params || {};

    return fetch(url, params)
      .then(function (res) {
        return res.json();
      });
  };

  var loadByURL = function (urls, params) {
    params = params || {};

    if (!Array.isArray(urls)) {
      urls = [urls];
    }

    urls = Array.unique(urls);
    var urlPromises = [];

    urls.forEach(function (url) {
      var promise = getTracker('lastRefresh', url, 0)
        .then(function (lastTimestamp) {
          var searchParams = new URLSearchParams({ last_requested: lastTimestamp });
          var fetchUrl = `${url}?${searchParams.toString()}`;
          return getJSON(fetchUrl, params);
        })
        .then(function (res) {
          return setTracker('lastRefresh', url, Object.getByPath(res, 'meta.created', DNAUtils.timestamp()))
            .then(function () {
              return bulkUpsert(res.data);
            });
        });

      urlPromises.push(promise);
    });

    return urlPromises.length ? Promise.all(urlPromises) : Promise.resolve(true);
  };

  var preloadURLs = function (urls, params) {
    return loadByURL(urls, params);
  };

  var changes = function (enable, options) {
    return (enable === false) ? cancelChanges() : enableChanges(options);
  };

  var enableChanges = function (options) {
    options = options || {};
    options = Object.assign(_DEFAULT_CHANGES_OPTIONS, options);

    var dbChanges =_db.changes(options);

    info('db_name')
      .then(function (dbName) {
        _dbs[dbName].changes = dbChanges;
      });

    return dbChanges;
  };

  var cancelChanges = function () {
    _db.changes.cancel();

    info('db_name')
      .then(function (dbName) {
        _dbs[dbName].changes = false;
      });

    return false;
  }

  var encrypt = function (password) {
    _db.crypto(password);
  };

  var decrypt = function () {
    _db.removeCrypto();
  };

  var init = function (name, config) {
    // @todo how to alter maxDelay parameter for first time loads?
    // @todo make these into configuration?
    return DNAUtils.retry(initHandler, [name, config], 10, 60 * 1000, 500);
  };

  var initHandler = function initHandler(name, config) {
    return new Promise(function (resolve, reject) {
      if (initializing(name)) {
        reject('Initialization already in progress.');
      }
      else if (initialized(name)) {
        _db = _dbs[name].db;
        resolve(true);
      }
      else {
        config = Object.assign(_DEFAULT_CONFIG, config || {});
        _dbs[name] = {
          initializing: true,
          initialized: false,
          config: config,
          db: new PouchDB(name, config.options),
          changes: false,
        };
        _db = _dbs[name].db;

        return createIndexes(_dbs[name].config.indexes)
          .then(function () {
            return handleResets(_dbs[name].config.lastReset)
          })
          .then(function () {
            _dbs[name].initializing = false;
            _dbs[name].initialized = true;
            resolve(true);
          });
      }
    });
  };

  var destroy = function (name, really) {
    if (really !== true) {
      return false;
    }

    var db = new PouchDB(name);
    db.destroy()
      .then(function (res) {
        if (_dbs.hasOwnProperty[name]) {
          delete _dbs[name];
        }
        return true;
      });
  };

  var me = {
    init: init,
    info: info,
    add: add,
    get: get,
    update: update,
    upsert: upsert,
    remove: remove,
    getAll: getAll,
    getAllByType: getAllByType,
    attachRelatedDocs: attachRelatedDocs,
    bulkUpsert: bulkUpsert,
    loadByURL: loadByURL,
    removeAllByType: removeAllByType,
    resetType: resetType,
    changes: changes,
    destroy: destroy
  };

  return me;

}(DNAPouchDB || {}));
