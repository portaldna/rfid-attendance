/**
 * @file
 * Javascript file for DNA PouchDB.
 */


(function ($, Drupal, window, document) {

  'use strict';

  Drupal.dna_pouchdb = {
    init: function (params, dbName) {
      params = params || {};
      dbName = dbName || params.database || Drupal.settings.dna_pouchdb.default;

      if (dbName == 'default') {
        dbName = Drupal.settings.dna_pouchdb.default;
      }

      return DNAPouchDB.init(dbName, Drupal.settings.dna_pouchdb.databases[dbName]);
    },

    loadData: function (ajax, response, status) {
      var dbName = response.data.dbName || Drupal.settings.dna_pouchdb.default;
      var docs = response.data.docs;

      Drupal.dna_pouchdb.init({}, dbName)
        .then(function () {
          DNAPouchDB.bulkUpsert(docs);
        });
    },

    // @todo make this easily overridable and possibly where each table could
    // have their own handler. dna_datatables.js may need more logic.
    addChangeHandler: function (tableID, type) {
      DNAPouchDB
        .changes(true)
        .on('change', function (changeDoc) {
          if (changeDoc.doc.type == type) {
            // @todo break out into separate function(s).
            DNAPouchDB
              // @todo does this break with _deleted docs?
              .attachRelatedDocs(changeDoc.doc)
              .then(function (docs) {
                docs.forEach(function (doc) {
                  $('#' + tableID).dtUpsert(doc);
                });
              });
          }
        });
    }
  };

  Drupal.ajax.prototype.commands.dna_pouchdb_loadData = Drupal.dna_pouchdb.loadData;

  if (Drupal.hasOwnProperty('dna_datatables')) {
    Drupal.dna_datatables.ajaxFunctions.dna_pouchdb = function (tableID, params) {
      return Drupal.dna_pouchdb.init(params)
        .then(function () {
          // @todo turning this off causes it to be off for all tables?
          // Similarly turning it on might cause conflict with tables still
          // loading?
          // DNAPouchDB.changes(false);
          return DNAPouchDB.loadByURL(params.urls || []);
        })
        .then(function () {
          return DNAPouchDB.getAllByType(params.type, 1)
            .then(function (docs) {
              Drupal.dna_pouchdb.addChangeHandler(tableID, params.type);
              return { data: docs };
            });
        });

    }
  }

})(jQuery, Drupal, this, this.document);
