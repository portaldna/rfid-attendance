@todo flesh this out.

The purpose of this module is as follows:

Provide a .make file to download core components (contrib modules) that are not
specific to any feature (or other DNA suite module).  This includes modules
like ctools, views, pathauto, etc.

Provide configuration exports (defaultconfig & strongarm) for system level
configuration or configuration that does not easily fall into the other DNA
suite of modules.

Similar to the configuration, any functionality / hooks may be included here.
