<?php

/**
 * @file
 * Token hooks for DNA Core.
 *
 * @todo add entity uri/url tokens.
 * @see https://www.drupal.org/project/token/issues/2938681
 */

/**
 * Implements hook_token_info().
 */
function dna_core_token_info() {
  $info['types']['dna'] = [
    'name' => t('Utility Tokens'),
    'description' => t('Helper / utility tokens.'),
  ];

  $info['types']['email'] = [
    'name' => t('Email'),
    'description' => t('Email message related tokens.'),
    'needs-data' => 'email',
  ];

  $path = 'user/login';
  $normal_path = drupal_get_normal_path($path);
  $item = menu_get_item($normal_path);
  $title = t('User Login Override');

  $info['tokens']['dna']['link'] = [
    'name' => t('Link'),
    'description' => t('Render a full link given a path. E.g. [dna:link:!path] turns into %link. Optionally override the page title with [dna:link:!path:!title]', [
      '!path' => $path,
      '%link' => l($item['title'], $item['href'], [
        'absolute' => TRUE,
      ]),
      '!title' => $title,
    ]),
    'dynamic' => TRUE,
  ];

  $info['tokens']['email'] = [
    'subject' => [
      'name' => t('Email Subject'),
      'description' => t('The subject of the email.'),
    ],
    'body' => [
      'name' => t('Email Body'),
      'description' => t('The body of the email.'),
    ],
    'author' => [
      'name' => t('The Author User Account'),
      'description' => t('The Drupal user account of the email author, if it exists.'),
      'type' => 'user',
    ],
    'recipient' => [
      'name' => t('A Recipient User Account'),
      'description' => t('The Drupal user account of an email recipient, if it exists. NOTE: If there are multiple recipients then the first recipient is used.'),
      'type' => 'user',
    ],
  ];

  if (module_exists('variable')) {
    variable_include();

    foreach (module_implements('dna_core_variable_group_tokens') as $module) {
      $groups_info = module_invoke($module, 'dna_core_variable_group_tokens');

      foreach ($groups_info as $group => $group_info) {
        $vars = variable_list_group($group);
        $label = ucwords(str_replace('_', ' ', $group));

        if (!$vars) {
          continue;
        }

        $info['types'][$group] = [
          'name' => $label,
          'description' => t('@label related tokens.', ['@label' => $label]),
        ];

        foreach ($vars as $var_name => $var) {
          if (empty($var['token'])) {
            continue;
          }

          $part = str_replace("{$module}_", '', $var_name);
          $info['tokens'][$group][$part] = [
            'name' => $var['title'],
            'description' => !empty($var['description']) ? $var['description'] : '',
          ];
        }
      }
    }
  }

  return $info;
}

/**
 * Implements hook_tokens().
 */
function dna_core_tokens($type, $tokens, array $data = [], array $options = []) {
  $replacements = [];
  $sanitize = !empty($options['sanitize']);
  $lang = !empty($options['language']) ? $options['language'] : language_default();
  $url_options = [
    'absolute' => TRUE,
    'language' => $lang,
  ];

  if ($type == 'email' && !empty($data['email'])) {
    $email = $data['email'];
    $from = trim($email['from']);
    $recipients = explode(',', $email['to']);

    if (!is_array($recipients)) {
      $recipients = [$recipients];
    }

    foreach ($recipients as &$recipient) {
      $recipient = trim($recipient);
    }

    foreach ($tokens as $name => $original) {
      if ($name == 'subject') {
        $replacements[$original] = $email['subject'];
      }
      elseif ($name == 'body') {
        $body = implode("\n\n", $email['body']);
        $body = drupal_html_to_text($body);
        $body = drupal_wrap_mail($body);
        $replacements[$original] = $body;
      }
      elseif ($name == 'author' && valid_email_address($from)) {
        $replacements[$original] = $from;
      }
      elseif ($name == 'recipient' && !empty($recipients) && valid_email_address($recipients[0])) {
        $replacements[$original] = $recipients[0];
      }
    }

    if ($author_tokens = token_find_with_prefix($tokens, 'author')) {
      if ($author = user_load_by_mail($from)) {
        $author_replacements = token_generate('user', $author_tokens, ['user' => $author], $options);
        $replacements += $author_replacements;
      }
    }

    if ($recipient_tokens = token_find_with_prefix($tokens, 'recipient')) {
      if ($recipient = user_load_by_mail($recipients[0])) {
        $recipient_replacements = token_generate('user', $recipient_tokens, ['user' => $recipient], $options);
        $replacements += $recipient_replacements;
      }
    }
  }

  if ($type == 'dna') {
    foreach ($tokens as $name => $original) {
      if (strpos($name, 'link:') === 0) {
        $parts = explode(':', $name);
        $path = $parts[1];
        $normal_path = drupal_get_normal_path($path);
        $item = menu_get_item($normal_path);
        $title = isset($parts[2]) ? $parts[2] : $item['title'];
        $title = $title ?: $item['href'];

        if (!empty($item['title']) && !empty($item['href'])) {
          $replacements[$original] = l($title, $item['href'], $url_options);
        }
      }
    }
  }

  if (module_exists('variable')) {
    variable_include();

    foreach (module_implements('dna_core_variable_group_tokens') as $module) {
      $groups_info = module_invoke($module, 'dna_core_variable_group_tokens');

      if (!array_key_exists($type, $groups_info)) {
        continue;
      }

      foreach ($groups_info as $group => $group_info) {
        $vars = variable_list_group($group);

        foreach ($tokens as $name => $original) {
          $var_name = "{$module}_{$name}";

          if (!array_key_exists($var_name, $vars) || empty($vars[$var_name]['token'])) {
            continue;
          }

          $value = variable_format_value($var_name);

          $replacements[$original] = token_replace($value, [], [
            'language' => $lang,
            'sanitize' => FALSE,
            'clear' => FALSE,
          ]);
        }
      }
    }
  }

  return $replacements;
}
