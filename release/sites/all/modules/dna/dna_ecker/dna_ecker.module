<?php

/**
 * @file
 * Main file for DNA ECKer.
 */

/**
 * Implements hook_menu_alter().
 */
function dna_ecker_menu_alter(&$items) {
  $ecker_info = dna_ecker_eck_entity_info();

  foreach ($ecker_info as $entity_type => $info) {
    foreach ($info['actions'] as $action => $path) {
      if (is_array($path)) {
        continue;
      }

      if (!isset($items[$path])) {
        continue;
      }

      if (strpos($path, '%entity_object') === FALSE) {
        continue;
      }

      $page_callback = "dna_ecker__entity__{$action}";

      if (!function_exists($page_callback)) {
        continue;
      }

      $path_entity_id = _dna_ecker_entity_object_replacement_position($path);

      $items[$path]['page callback'] = $page_callback;
      $items[$path]['page arguments'] = [$entity_type, $path_entity_id];
    }

    if (!empty($info['meta']['entity_module']) && $info['meta']['entity_module'] == 'eck') {
      $view_path = $info['actions']['view'];

      if (isset($items[$view_path])) {
        $items[$view_path]['title callback'] = 'dna_ecker_entity_view_title_callback';
        $items[$view_path]['title arguments'] = [$path_entity_id];
      }
    }
  }
}

/**
 * Implements hook_menu().
 */
function dna_ecker_menu() {
  $items = [];

  $items['admin/config/dna/ecker'] = [
    'title' => 'ECKer',
    'description' => 'Configure DNA ECKer related settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => ['dna_ecker_configuration_form'],
    'access arguments' => ['administer site configuration'],
    'file' => 'dna_ecker.admin.inc',
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'management',
  ];

  return $items;
}

/**
 * Title callback: ECK Entity View.
 */
function dna_ecker_entity_view_title_callback($entity) {
  return $entity->label;
}

/**
 * Implements hook_dna_ecker_eck_entity_info().
 */
function dna_ecker_dna_ecker_eck_entity_info() {
  return [
    'user' => [],
  ];
}

/**
 * Implements hook_dna_ecker_eck_entity_info_alter().
 */
function dna_ecker_dna_ecker_eck_entity_info_alter(&$info) {
  if (isset($info['user'])) {
    $props = [
      'url',
      'edit_url',
      'theme',
    ];

    foreach ($info['user']['properties'] as $property => $prop_info) {
      if (in_array($property, $props)) {
        unset($info['user']['properties'][$property]);
      }
    }

    unset($info['user']['base_path']);
    $info['user']['meta']['entity_module'] = 'user';
  }
}

/**
 * Implements hook_entity_info_alter().
 */
function dna_ecker_entity_info_alter(&$entity_info) {
  $ecker_info = dna_ecker_eck_entity_info();

  foreach ($ecker_info as $entity_type => $info) {
    if (empty($entity_info[$entity_type]) || $info['meta']['entity_module'] != 'eck') {
      continue;
    }

    foreach ($entity_info[$entity_type]['bundles'] as $bundle => &$bundle_info) {
      if (!in_array($bundle, array_keys($info['bundles']))) {
        continue;
      }
      $bundle_info['crud']['add']['path'] = $info['actions']['add'][$bundle];
      $bundle_info['crud']['view']['path'] = $info['actions']['view'];
      $bundle_info['crud']['view']['entity_id'] = _dna_ecker_entity_object_replacement_position($info['actions']['view']);
      $bundle_info['crud']['edit']['path'] = $info['actions']['edit'];
      $bundle_info['crud']['edit']['entity_id'] = _dna_ecker_entity_object_replacement_position($info['actions']['edit']);
      $bundle_info['crud']['delete']['path'] = $info['actions']['delete'];
      $bundle_info['crud']['delete']['entity_id'] = _dna_ecker_entity_object_replacement_position($info['actions']['delete']);
    }

    $entity_info[$entity_type]['uri callback'] = 'dna_ecker__entity__uri';
  }
}

/**
 * Implements hook_eck_entity_save_message_alter().
 */
function dna_ecker_eck_entity_save_message_alter(&$msg, $args, $context) {
  $entity_type = $context['entity']->entityType();

  if ($ecker_info = dna_ecker_eck_entity_info($entity_type)) {
    $msg = '@entity_label has been saved.';
  }
}

/**
 * Implements hook_entity_presave().
 */
function dna_ecker_entity_presave($entity, $entity_type) {
  $ecker_info = dna_ecker_eck_entity_info($entity_type);

  if (!$ecker_info) {
    return;
  }

  if (!empty($ecker_info['properties']['status'])) {
    if (!isset($entity->status)) {
      $entity->status = 1;
    }
  }
}

/**
 * Implements hook_entity_load().
 */
function dna_ecker_entity_load($entities, $entity_type) {
  $ecker_info = dna_ecker_eck_entity_info($entity_type);

  if (!$ecker_info) {
    return;
  }

  if (!empty($ecker_info['meta']['entity_module']) && $ecker_info['meta']['entity_module'] == 'eck') {
    foreach ($entities as $entity_id => $entity) {
      if ($label = eck__entity__label($entity)) {
        $entity->label = $label;
      }

      if (!empty($ecker_info['properties']['status'])) {
        if (!isset($entity->status)) {
          $entity->status = 1;
        }
      }
    }
  }
}

/**
 * Implements hook_page_build().
 */
function dna_ecker_page_build(&$page) {
  $page['page_bottom']['dna_ecker']['#attached']['js'][] = [
    'data' => [
      'dna_ecker' => [
        'entities' => dna_ecker_eck_entity_info(),
      ],
    ],
    'type' => 'setting',
  ];
}

/**
 * Implements hook_entity_tr_alter().
 *
 * This is a near copy of entity_table() to allow for changes like
 * %entity_object, etc.
 *
 * @todo Override admin listings then deprecate this.
 *
 * @see entity_table()
 */
function dna_ecker_entity_tr_alter(&$row, $info) {
  $entity_type = $info['entity_type'];
  $ecker_info = dna_ecker_eck_entity_info($entity_type);

  if (!$ecker_info) {
    return;
  }

  $entity = $info['entity'];
  $bundle = $info['bundle'];

  if (!in_array($bundle, array_keys($ecker_info['bundles']))) {
    return;
  }

  $id = entity_id($entity_type, $entity);

  $actions = $ecker_info['actions'];
  $allowed_operations = '';
  $destination = drupal_get_destination();
  // Check that the user has permissions to edit.
  if (eck__entity_menu_access('edit', $entity_type, $bundle, $id)) {
    $edit_path = str_replace('%entity_object', $id, $actions['edit']);
    $allowed_operations = l(t('edit'), $edit_path, array('query' => $destination));
  }

  // Check that the user has permissions to delete.
  if (eck__entity_menu_access('delete', $entity_type, $bundle, $id)) {
    $delete_path = str_replace('%entity_object', $id, $actions['delete']);
    $allowed_operations .= (($allowed_operations) ? '<br />' : '') . l(t('delete'), $delete_path, array('query' => $destination));
  }

  // Check that the user has permissions to view.
  if (eck__entity_menu_access('view', $entity_type, $bundle, $id)) {
    $uri = entity_uri($entity_type, $entity);
    $row = array(
      l(entity_label($entity_type, $entity), $uri['path'], $uri['options']),
    );
  }
  else {
    $row = array(
      entity_label($entity_type, $entity),
    );
  }

  $row[] = array('data' => $allowed_operations);
}

/**
 * Implements hook_form_alter().
 */
function dna_ecker_form_alter(&$form, &$form_state, $form_id) {
  if (strpos($form_id, 'eck__entity__form_') === 0) {
    $entity_type = $form['#entity_type'];
    $ecker_info = dna_ecker_eck_entity_info($entity_type);

    if (!$ecker_info) {
      return;
    }

    $bundle = $form['#bundle'];

    if (!in_array($bundle, array_keys($ecker_info['bundles']))) {
      return;
    }

    if (isset($form['redirect'])) {
      unset($form['redirect']);
    }

    $component_class = drupal_clean_css_identifier("{$entity_type}-form");
    $form['#component_class'] = $component_class;
    $form['#prefix'] = '<div id="' . $component_class . '-wrapper">';
    $form['#suffix'] = '</div>';
    $form['#attributes']['class'][] = $component_class;

    $current_path = current_path();
    $is_new = (strpos($form_id, 'eck__entity__form_add_') === 0);
    $entity = !empty($form_state['values']['entity']) ? $form_state['values']['entity'] : $form_state[$entity_type];

    $title = t('@action @label', [
      '@action' => $is_new ? t('Add') : t('Edit'),
      '@label' => ($is_new || !isset($entity->label)) ? $ecker_info['label'] : $entity->label,
    ]);

    drupal_set_title($title);

    $form['is_new'] = [
      '#type' => 'value',
      '#value' => $is_new,
    ];

    $form['actions']['#weight'] = 100;
    $form['actions']['submit']['#ajax'] = [
      'callback' => 'dna_ecker_entity_form_submit_ajax_callback',
      'wrapper' => $component_class . '-wrapper',
    ];

    drupal_alter('dna_ecker_entity_form', $form, $form_state, $form_id);
  }
}

/**
 * AJAX callback: ECK Entity Forms.
 */
function dna_ecker_entity_form_submit_ajax_callback($form, &$form_state) {
  if ($errors = form_get_errors()) {
    return $form;
  }

  $commands = [];

  if ($messages = theme('status_messages')) {
    // @todo configure selector.
    $commands['status_messages'] = ajax_command_invoke('.alert-wrapper', 'append', [$messages]);
  }

  // @todo change to use dna_utilities_ajax_command_alter().
  drupal_alter('dna_ecker_entity_form_submit_ajax_commands', $commands, $form, $form_state);

  return ['#type' => 'ajax', '#commands' => array_values($commands)];
}

/**
 * ECK URI Callback.
 *
 * @see eck__entity__uri()
 */
function dna_ecker__entity__uri($entity) {
  $entity_type = $entity->entityType();
  $ecker_info = dna_ecker_eck_entity_info($entity_type);
  $ids = entity_extract_ids($entity_type, $entity);

  module_load_include('inc', 'eck', 'eck.entity');
  $view_path = $ecker_info['actions']['view'];
  $view_path = str_replace('%entity_object', $ids[0], $view_path);

  return ['path' => $view_path];
}

/**
 * Page callback: ECK View.
 */
function dna_ecker__entity__view($entity_type, $entity) {
  $bundle = $entity->bundle();
  return eck__entity__view($entity_type, $bundle, $entity);
}

/**
 * Page callback: ECK Edit.
 */
function dna_ecker__entity__edit($entity_type, $entity) {
  $bundle = $entity->bundle();
  return eck__entity__edit($entity_type, $bundle, $entity);
}

/**
 * Page callback: ECK Delete.
 */
function dna_ecker__entity__delete($entity_type, $entity) {
  $bundle = $entity->bundle();
  return eck__entity__delete($entity_type, $bundle, $entity);
}

/**
 * Get eck entity info.
 *
 * Hook info:
 * - entity_type
 * - bundles
 * - path
 * - label
 * - label_plural
 * //
 *
 * Output:
 * - entity_type
 * - label
 * - label_plural
 * - base_path
 * - actions:
 *   - view
 *   - edit
 *   - delete
 *   - add:
 *     - [bundle]
 * - bundles:
 *   - [bundle]
 *     - label
 *     - fields
 * - properties[]
 *   - [property]
 *     - type
 *     - label
 *     - description
 *     - required
 * - meta
 *   - module
 *   - config
 *     - ajax
 *       - url
 *     - columns[]
 *       - title
 *       - name
 *   - menu (hook_menu)
 *     - type
 *     - menu_name
 *     - weight
 * //
 */
function dna_ecker_eck_entity_info($return_entity_type = NULL, $reset = FALSE) {
  $ecker_info = &drupal_static(__FUNCTION__);

  if (!isset($ecker_info) || $reset) {
    if (!$reset && $cache = cache_get('dna_ecker_eck_entity_info')) {
      $ecker_info = $cache->data;
    }
    else {
      $modules = module_implements('dna_ecker_eck_entity_info');

      foreach ($modules as $module) {
        $mod_info = module_invoke($module, 'dna_ecker_eck_entity_info');

        foreach ($mod_info as $entity_type => $info) {
          if (!empty($ecker_info[$entity_type])) {
            continue;
          }

          $entity_info = entity_get_info($entity_type);
          $bundles = !empty($info['bundles']) ? $info['bundles'] : array_keys($entity_info['bundles']);
          $ecker_info[$entity_type] = _dna_ecker_build_entity_info($entity_type, $reset);
          $e_info = &$ecker_info[$entity_type];

          $base_path = !empty($info['path']) ? $info['path'] : ($e_info['base_path'] ? $e_info['base_path'] : drupal_clean_css_identifier($entity_type));

          $e_info['meta']['module'] = $module;
          $e_info['base_path'] = $base_path;
          $e_info['label'] = !empty($info['label']) ? $info['label'] : $e_info['label'];
          $e_info['label_plural'] = !empty($info['label_plural']) ? $info['label_plural'] : $e_info['label'];

          if ($e_info['meta']['entity_module'] == 'eck') {
            $e_info['actions'] = [
              'add' => [],
              'view' => "{$base_path}/%entity_object",
              'edit' => "{$base_path}/%entity_object/edit",
              'delete' => "{$base_path}/%entity_object/delete",
            ];

            foreach ($e_info['bundles'] as $bundle => $bundle_info) {
              if (!in_array($bundle, $bundles)) {
                continue;
              }

              $bundle_clean = drupal_clean_css_identifier($bundle);
              $e_info['actions']['add'][$bundle] = "{$base_path}/{$bundle_clean}/add";
            }
          }

          // @todo override admin listing.
        }
      }

      drupal_alter('dna_ecker_eck_entity_info', $ecker_info);

      foreach ($ecker_info as $entity_type => &$info) {
        drupal_alter("dna_ecker_eck_entity_info_{$entity_type}", $info);
        $info['meta']['created'] = REQUEST_TIME;
      }

      cache_set('dna_ecker_eck_entity_info', $ecker_info, 'cache', CACHE_TEMPORARY);
    }
  }

  if ($return_entity_type) {
    return isset($ecker_info[$return_entity_type]) ? $ecker_info[$return_entity_type] : [];
  }
  else {
    return $ecker_info;
  }
}

/**
 * Build entity info basics.
 *
 * @todo handle user, node, term, etc. better.
 * @todo look into using $wrapper->getPropertyInfo().
 */
function _dna_ecker_build_entity_info($entity_type, $reset = FALSE) {
  $ecker_info = &drupal_static(__FUNCTION__, []);

  if (!isset($ecker_info[$entity_type]) || $reset) {
    $entity_info = entity_get_info($entity_type);
    $entity_keys = array_flip(array_filter($entity_info['entity keys']));
    $base_path = !empty($entity_info['default path']) ? $entity_info['default path'] : '';
    $fields_info = field_info_instances($entity_type);
    $properties_info = entity_get_property_info($entity_type);

    $ecker_info[$entity_type] = [
      'label' => $entity_info['label'],
      'label_plural' => !empty($entity_info['plural label']) ? $entity_info['plural label'] : '',
      'base_path' => $base_path ?: '',
      'bundles' => [],
      'actions' => [],
      'meta' => [
        'entity_module' => isset($entity_info['module']) ? $entity_info['module'] : '',
        'relationships' => [],
      ],
    ];

    if ($base_path && $ecker_info[$entity_type]['meta']['entity_module'] == 'eck') {
      $ecker_info[$entity_type] += [
        'actions' => [
          'add' => [],
          'view' => $base_path,
          'edit' => "{$base_path}/edit",
          'delete' => "{$base_path}/delete",
        ],
      ];
    }

    foreach ($entity_info['bundles'] as $bundle => $bundle_info) {
      $ecker_info[$entity_type]['bundles'][$bundle] = [
        'label' => $bundle_info['label'],
        'fields' => [],
      ];

      if ($base_path && $ecker_info[$entity_type]['meta']['entity_module'] == 'eck') {
        $bundle_clean = drupal_clean_css_identifier($bundle);
        $ecker_info[$entity_type]['actions']['add'][$bundle] = "{$base_path}/{$bundle_clean}/add";
      }
    }

    foreach ($fields_info as $bundle => $bundle_fields) {
      foreach ($bundle_fields as $field_name => $field_info) {
        $field_info_field = field_info_field($field_name);

        $ecker_info[$entity_type]['bundles'][$bundle]['fields'][$field_name] = [
          'field_name' => $field_name,
          'label' => $field_info['label'],
          'description' => strip_tags($field_info['description']),
          'required' => (int) $field_info['required'],
          'cardinality' => $field_info_field['cardinality'],
          'weight' => $field_info['display']['default']['weight'],
          'type' => $field_info_field['type'],
        ];

        if ($field_info_field['type'] == 'entityreference') {
          $ecker_info[$entity_type]['meta']['relationships'][$field_name] = $field_info_field['settings']['target_type'];
        }
      }
    }

    foreach ($properties_info['properties'] as $property => $property_info) {
      if (!empty($entity_keys[$property])) {
        $property = $entity_keys[$property];
      }

      if ($property == 'id') {
        continue;
      }

      if (empty($property_info['type'])) {
        $property_info['type'] = 'text';
      }

      if ($property_info['type'] == 'date') {
        $property_info['type'] = 'timestamp';
      }

      $ecker_info[$entity_type]['properties'][$property] = [
        'type' => ($property_info['type'] == 'token') ? 'text' : $property_info['type'],
        'label' => $property_info['label'],
        'description' => strip_tags($property_info['description']),
        'required' => (int) !empty($property_info['required']),
        'cardinality' => 1,
        'weight' => 100,
      ];

      if ($property == 'uid') {
        $ecker_info[$entity_type]['meta']['relationships']['uid'] = 'user';
        $ecker_info[$entity_type]['properties']['uid']['type'] = 'relationship';
      }

      if ($property == 'bundle') {
        $ecker_info[$entity_type]['properties'][$property]['label'] = t('@entity_label Type', [
          '@entity_label' => $ecker_info[$entity_type]['label'],
        ]);
      }
    }
  }

  return $ecker_info[$entity_type];
}

/**
 * Get entity_object replacement position.
 */
function _dna_ecker_entity_object_replacement_position($path) {
  $path_parts = explode('/', $path);
  $path_entity_id = array_search('%entity_object', $path_parts);
  return $path_entity_id;
}
