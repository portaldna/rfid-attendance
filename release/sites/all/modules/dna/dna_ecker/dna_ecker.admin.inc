<?php

/**
 * @file
 * Admin page callbacks for DNA ECKer module.
 */

/**
 * Form: Basic Configuration.
 */
function dna_ecker_configuration_form($form, &$form_state) {
  $component_class = 'dna-ecker-configuration-form';
  $form['#component_class'] = $component_class;
  $form['#prefix'] = '<div id="' . $component_class . '-wrapper">';
  $form['#suffix'] = '</div>';
  $form['#attributes']['class'][] = $component_class;

  $form['submit_clear_cache'] = [
    '#value' => t('Rebuild ECKer Info Cache'),
    '#type' => 'submit',
    '#submit' => ['dna_ecker_configuration_form_clear_cache_submit'],
    '#ajax' => [
      'callback' => 'dna_ecker_configuration_form_ajax_callback',
      'wrapper' => $component_class . '-wrapper',
    ],
  ];

  // @todo change when there is configuration to alter.
  // return system_settings_form($form);
  return $form;
}

/**
 * Submit handler: Config form clear cache.
 */
function dna_ecker_configuration_form_clear_cache_submit($form, &$form_state) {
  $ecker_info = dna_ecker_eck_entity_info(NULL, TRUE);
  drupal_set_message('DNA ECKer cache has been cleared / rebuild.');
}

/**
 * AJAX handler: Config form clear cache.
 */
function dna_ecker_configuration_form_ajax_callback($form, &$form_state) {
  return $form;
}
