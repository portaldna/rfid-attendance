<?php
/**
 * @file
 * dna_develop.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function dna_develop_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'access devel information'.
  $permissions['access devel information'] = array(
    'name' => 'access devel information',
    'roles' => array(
      'administrator' => 'administrator',
      'anonymous user' => 'anonymous user',
      'authenticated user' => 'authenticated user',
    ),
    'module' => 'devel',
  );

  // Exported permission: 'administer maillog'.
  $permissions['administer maillog'] = array(
    'name' => 'administer maillog',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'maillog',
  );

  // Exported permission: 'delete maillog'.
  $permissions['delete maillog'] = array(
    'name' => 'delete maillog',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'maillog',
  );

  // Exported permission: 'execute php code'.
  $permissions['execute php code'] = array(
    'name' => 'execute php code',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'devel',
  );

  // Exported permission: 'switch users'.
  $permissions['switch users'] = array(
    'name' => 'switch users',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'devel',
  );

  // Exported permission: 'view maillog'.
  $permissions['view maillog'] = array(
    'name' => 'view maillog',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'maillog',
  );

  return $permissions;
}
