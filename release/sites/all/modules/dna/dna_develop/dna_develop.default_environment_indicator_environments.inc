<?php
/**
 * @file
 * dna_develop.default_environment_indicator_environments.inc
 */

/**
 * Implements hook_default_environment_indicator_environment().
 */
function dna_develop_default_environment_indicator_environment() {
  $export = array();

  $environment = new stdClass();
  $environment->disabled = FALSE; /* Edit this to true to make a default environment disabled initially */
  $environment->api_version = 1;
  $environment->machine = 'dna_local_development';
  $environment->name = 'DNA Local Development';
  $environment->regexurl = 'dna.loc';
  $environment->settings = array(
    'color' => '#008de7',
    'text_color' => '#ffffff',
    'weight' => '-99',
    'position' => 'top',
    'fixed' => 1,
  );
  $export['dna_local_development'] = $environment;

  return $export;
}
