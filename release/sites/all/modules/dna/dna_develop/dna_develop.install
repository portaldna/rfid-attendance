<?php
/**
 * Install, update, and uninstall hooks for DNA Develop module.
 */

/**
 * Implements hook_install().
 */
function dna_develop_install() {
  // Only set initial/default modules if not already defined.
  $modules = variable_get('dna_develop_dev_modules', array());

  if (!$modules) {
    $modules = array(
      'dblog',
      'defaultconfig',
      'devel',
      'diff',
      'features',
      'field_ui',
      'link_css',
      'maillog',
      'stage_file_proxy',
      'tota11y',
      'views_ui',
    );

    if (module_exists('eck')) {
      $modules[] = 'eck_devel';
    }

    if (module_exists('commerce')) {
      $modules[] = 'commerce_devel';
    }

    variable_set('dna_develop_dev_modules', $modules);
  }
}

/**
 * Implements hook_uninstall().
 */
function dna_develop_uninstall() {
  variable_del('dna_develop_dev_modules');
  variable_del('dna_develop_dev_modules_enabled');
}

/**
 * Implements hook_enable().
 */
function dna_develop_enable() {
  // Enable useful development modules (if they exist) when this module is
  // enabled. We pull from a variable that can be defined in a settings file to
  // allow for site specific modules.
  if ($modules = variable_get('dna_develop_dev_modules', array())) {
    foreach ($modules as $module_name) {
      if (!module_exists($module_name)) {
        unset($modules[$module_name]);
      }
    }

    if ($modules) {
      module_enable($modules);
      drupal_set_message(t('The following modules have been enabled. These modules should automatically be disabled when DNA Develop is disabled. !modules', array(
        '!modules' => implode(', ', $modules),
      )));

      // Set a variable to track specifically what was enabled. We only want to
      // disable the modules we enabled in dna_develop_disable.
      variable_set('dna_develop_dev_modules_enabled', $modules);
    }
  }
}

/**
 * Implements hook_disable().
 */
function dna_develop_disable() {
  // Disables the modules that were enabled in dna_develop_enable.
  if ($modules = variable_get('dna_develop_dev_modules_enabled', array())) {
    foreach ($modules as $module_name) {
      if (!module_exists($module_name)) {
        unset($modules[$module_name]);
      }
    }

    if ($modules) {
      // Note: We do not disable dependencies. It's possible another module has
      // a dependency on a module that is about to be disabled. There could be a
      // bad chain-reaction. This isn't perfect as a module could be disabled
      // that is depended on, however these should mostly be development or
      // admin UI type modules that won't break site functionality if disabled.
      module_disable($modules, FALSE);
      drupal_set_message(t('The following modules have been disabled. !modules', array(
        '!modules' => implode(', ', $modules),
      )));
    }
  }
}
