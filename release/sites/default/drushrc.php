<?php

/**
 * @file
 * Drush runtime config (drushrc) file.
 *
 * @see https://raw.githubusercontent.com/drush-ops/drush/8.x/examples/example.drushrc.php
 */

/**
 * Result File.
 *
 * Specify the filename and path where 'sql-dump' should store backups of
 * database dumps.  The default is to dump to STDOUT, however if this option is
 * set in a drushrc.php file, the default behaviour can be achieved by
 * specifying a value of FALSE ("--result-file=0" on the command line).  Two
 * substitution tokens are available: @DATABASE is replaced with the name of the
 * database being dumped, and @DATE is replaced with the current time and date
 * of the dump of the form: YYYYMMDD_HHMMSS.  A value of TRUE ("--result-file=1"
 * on the command line) will cause 'sql-dump' to use the same temporary backup
 * location as 'pm-updatecode'.
 */
$options['result-file'] = '../db_backups/@DATABASE-@DATE.sql';

/**
 * Structure Tables.
 *
 * List of tables whose *data* is skipped by the 'sql-dump' and 'sql-sync'
 * commands when the "--structure-tables-key=common" option is provided.
 * You may add specific tables to the existing array or add a new element.
 */
$options['structure-tables']['common'] = [
  'cache',
  'cache_*',
  'captcha_sessions',
  'history',
  'search_*',
  'sessions',
];

$command_specific['site-install'] = ['account-name' => 'admin', 'account-pass' => 'dontbetardy'];
