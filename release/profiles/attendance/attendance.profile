<?php

/**
 * @file
 * Attendance Install profile functions.
 */

/**
 * Implements hook_install_tasks_alter().
 */
function attendance_install_tasks_alter(&$tasks, $install_state) {
  // Attempt to resolve dependency issues.
  // Taken from panopoly.profile.
  require_once drupal_get_path('module', 'dna_core') . '/dna_core.profile.inc';
  $tasks['install_load_profile']['function'] = 'dna_core_install_load_profile';

  // If we only offer one language, define a callback to set this.
  if (!(count(install_find_locales($install_state['parameters']['profile'])) > 1)) {
    $tasks['install_select_locale']['function'] = 'dna_core_install_locale_selection';
  }

  // Override "install_finished" task.
  $tasks['install_finished']['function'] = 'attendance_install_finished';
}

/**
 * Override of install_finished().
 */
function attendance_install_finished(&$install_state) {
  // Flush all caches to ensure that any full bootstraps during the installer
  // do not leave stale cached data, and that any content types or other items
  // registered by the installation profile are registered correctly.
  drupal_flush_all_caches();

  // Remember the profile which was used.
  variable_set('install_profile', drupal_get_profile());

  // Installation profiles are always loaded last.
  db_update('system')
    ->fields(array('weight' => 1000))
    ->condition('type', 'module')
    ->condition('name', drupal_get_profile())
    ->execute();

  // Cache a fully-built schema.
  drupal_get_schema(NULL, TRUE);

  // Run cron to populate update status tables (if available) so that users
  // will be warned if they've installed an out of date Drupal version.
  // Will also trigger indexing of profile-supplied content or feeds.
  drupal_cron_run();

  // Clear & rebuild Features key caches.
  if (module_exists('features')) {
    features_get_info(NULL, NULL, TRUE);
    features_rebuild();

    // Rebuild all Default Config (cleanup install).
    if (module_exists('defaultconfig')) {
      defaultconfig_rebuild_all();
    }

    // Revert all Features (cleanup install).
    features_revert();
  }

  // Remove install messages.
  $messages = array('completed', 'status', 'warning', 'error');
  foreach ($messages as $message) {
    drupal_get_messages($message, TRUE);
  }

  // Finish installation.
  if (!drupal_is_cli()) {
    // Redirect UI-based install to the site homepage.
    drupal_goto(variable_get('site_frontpage', ''));
  }
  else {
    // Display a success message for Drush-based install.
    drupal_set_message("RFID Attendance install complete.");
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function attendance_form_install_configure_form_alter(&$form, $form_state) {
  // Hide some messages from various modules that are just too chatty.
  drupal_get_messages('status');
  drupal_get_messages('warning');

  // Set reasonable defaults for site configuration form.
  $form['site_information']['site_name']['#default_value'] = 'RFID Attendance';
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['server_settings']['site_default_country']['#default_value'] = 'US';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'America/New_York';

  // Define a default email address if we can guess a valid one.
  if (valid_email_address('admin@' . $_SERVER['HTTP_HOST'])) {
    $form['site_information']['site_mail']['#default_value'] = 'admin@' . $_SERVER['HTTP_HOST'];
    $form['admin_account']['account']['mail']['#default_value'] = 'admin@' . $_SERVER['HTTP_HOST'];
  }
}
