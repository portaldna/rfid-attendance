const SerialPort = require('serialport');
const request = require('request');
const Readline = SerialPort.parsers.Readline;
const yaml = require('js-yaml');
const fs = require('fs');

var portName = 'COM1';
var configFile = fs.existsSync('../../vm/local.config.yml') ? 'local.config.yml' : 'config.yml';

try {
  config = yaml.safeLoad(fs.readFileSync('../../vm/' + configFile, 'utf8'));
} catch (e) {
  console.log(e);
}

const hostname = config.vagrant_hostname;

const port = new SerialPort(portName, {
	baudRate: 9600,
	dataBits: 8,
	parity: 'none',
	stopBits: 1,
	flowControl: false
});

const parser = port.pipe(new Readline({ delimiter: '\r' }));

parser.on('data', function(data) {
	var bits = data;
	console.log('bits', bits);

  // Use the following curl command to test from the command line. Change the
  // url hostname based on the local configuration.
  // curl -i -X POST -H "Content-Type:application/json" http://loc.rfid.com/dna-jsonapi/attendance-log/ -d '{"tag_id":"enter a tag id","reader_id":"enter a reader id"}'
  var requestOptions = {
    url: 'http://' + hostname + '/dna-jsonapi/attendance-log',
    method: 'POST',
    json: {
      tag_id: bits,
      reader_id: '00003421',
    },
  };

  // @todo do we want any error logging here?
  // @todo what about also writing to the file system as a poorman's backup?
  try {
    request(requestOptions);
  }
  catch (err) {
    console.log('error', err);
  }
});
