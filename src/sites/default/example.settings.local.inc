<?php

/**
 * @file
 * Drupal site-specific configuration file.
 *
 * See settings.php to understand what options are available and may be
 * overridden in this file.
 */

$conf['site_name'] = 'RFID Attendance';

# File system settings (admin/config/media/file-system)
$conf['file_temporary_path'] = '/tmp';
